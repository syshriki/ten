CREATE DATABASE  IF NOT EXISTS `tendb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `tendb`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tendb
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `archivednotifications`
--

DROP TABLE IF EXISTS `archivednotifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivednotifications` (
  `idarchivednotifications` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `affectinguser` int(11) DEFAULT NULL,
  `affecteduser` int(11) DEFAULT NULL,
  `idpost` int(11) DEFAULT NULL,
  `idunreadnotification` int(11) DEFAULT NULL,
  `leftimage` varchar(55) DEFAULT NULL,
  `rightimage` varchar(55) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `archiveddatestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idarchivednotifications`),
  KEY `effectinguser2_idx` (`affectinguser`),
  KEY `effecteduser2_idx` (`affecteduser`),
  KEY `postid2_idx` (`idpost`),
  CONSTRAINT `effecteduser2` FOREIGN KEY (`affecteduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `effectinguser2` FOREIGN KEY (`affectinguser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `postid2` FOREIGN KEY (`idpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `idfollowers` int(11) NOT NULL AUTO_INCREMENT,
  `idrequestee` int(11) DEFAULT NULL,
  `idrecipient` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idfollowers`),
  UNIQUE KEY `gsc` (`idrequestee`,`idrecipient`),
  KEY `idrequestee_idx` (`idrequestee`),
  KEY `idrecipient_idx` (`idrecipient`),
  CONSTRAINT `idrecipient` FOREIGN KEY (`idrecipient`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idrequestee` FOREIGN KEY (`idrequestee`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`followers_AFTER_INSERT` AFTER INSERT ON `followers` FOR EACH ROW
BEGIN
CALL insertUnreadFriendRequestNotification(NEW.`idrequestee`,NEW.`idrecipient`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `hashtags`
--

DROP TABLE IF EXISTS `hashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtags` (
  `idhashtags` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(100) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idhashtags`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unreadnotifications`
--

DROP TABLE IF EXISTS `unreadnotifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unreadnotifications` (
  `idunreadnotifications` int(11) NOT NULL AUTO_INCREMENT,
  `notificationtype` varchar(45) DEFAULT NULL,
  `affectinguser` int(11) DEFAULT NULL,
  `affecteduser` int(11) DEFAULT NULL,
  `idpost` int(11) DEFAULT NULL,
  `isread` tinyint(1) DEFAULT '0',
  `leftimagesrc` varchar(55) DEFAULT NULL,
  `rightimagesrc` varchar(55) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idunreadnotifications`),
  KEY `effectinguser_idx` (`affectinguser`),
  KEY `effecteduser_idx` (`affecteduser`),
  KEY `postid_idx` (`idpost`),
  CONSTRAINT `effecteduser` FOREIGN KEY (`affecteduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `effectinguser` FOREIGN KEY (`affectinguser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `postid` FOREIGN KEY (`idpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` blob NOT NULL,
  `datestamp` datetime DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `imagesrc` varchar(45) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `postsprivate` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=814 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userblocks`
--

DROP TABLE IF EXISTS `userblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userblocks` (
  `iduserblocks` int(11) NOT NULL AUTO_INCREMENT,
  `iduserblocked` int(11) DEFAULT NULL COMMENT 'Id of the user blocked',
  `iduserblocker` int(11) DEFAULT NULL COMMENT 'if of the users who is blocking the user',
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserblocks`),
  KEY `blocked_idx` (`iduserblocked`),
  KEY `blocker_idx` (`iduserblocker`),
  CONSTRAINT `blocked` FOREIGN KEY (`iduserblocked`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blocker` FOREIGN KEY (`iduserblocker`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='users can block other users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usercomment`
--

DROP TABLE IF EXISTS `usercomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usercomment` (
  `idusercomment` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`idusercomment`),
  KEY `iduser5_idx` (`iduser`),
  KEY `idpost_idx` (`iduserpost`),
  CONSTRAINT `idpost2` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser5` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='user comment on other user posts';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`usercomment_AFTER_INSERT` AFTER INSERT ON `usercomment` FOR EACH ROW
BEGIN
     CALL insertCommentNotification(NEW.idusercomment,NEW.iduser);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `userfavorites`
--

DROP TABLE IF EXISTS `userfavorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userfavorites` (
  `iduserfavorites` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserfavorites`),
  KEY `iduser4_idx` (`iduser`),
  KEY `idpost3_idx` (`iduserpost`),
  CONSTRAINT `idpost3` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser6` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpost`
--

DROP TABLE IF EXISTS `userpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpost` (
  `iduserpost` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `datestamp` timestamp NULL DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `iduserpostrepost` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduserpost`),
  KEY `iduser_idx` (`iduser`),
  CONSTRAINT `iduser3` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1128 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`userpost_AFTER_INSERT` AFTER INSERT ON `userpost` FOR EACH ROW
BEGIN
	IF NEW.iduserpostrepost IS NOT null AND NEW.iduserpostrepost <> 0 THEN
		CALL insertUnreadRepostNotification(NEW.iduserpostrepost,NEW.iduser);
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `userposthashtags`
--

DROP TABLE IF EXISTS `userposthashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userposthashtags` (
  `iduserposthashtags` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `idhashtag` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduserposthashtags`),
  KEY `userpost_idx` (`iduserpost`),
  KEY `hashtag_idx` (`idhashtag`),
  CONSTRAINT `hashtag` FOREIGN KEY (`idhashtag`) REFERENCES `hashtags` (`idhashtags`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpostimage`
--

DROP TABLE IF EXISTS `userpostimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostimage` (
  `iduserpostimage` int(11) NOT NULL AUTO_INCREMENT,
  `imagesrc` varchar(55) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpostimage`),
  KEY `iduserpost_idx` (`iduserpost`),
  CONSTRAINT `usepost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpostlikes`
--

DROP TABLE IF EXISTS `userpostlikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostlikes` (
  `iduserpostlikes` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL COMMENT 'The user who liked the post',
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpostlikes`),
  KEY `iduser4_idx` (`iduser`),
  KEY `idpost_idx` (`iduserpost`),
  CONSTRAINT `idpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser4` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`userpostlikes_AFTER_INSERT` AFTER INSERT ON `userpostlikes` FOR EACH ROW
BEGIN
    CALL insertUnreadLikeNotification(NEW.iduserpostlikes,NEW.iduser);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'tendb'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `archiveReadNotifications` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `archiveReadNotifications` ON SCHEDULE EVERY 1 DAY STARTS '2014-11-26 19:36:53' ON COMPLETION NOT PRESERVE ENABLE DO CALL archiveReadNotifications() */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'tendb'
--
/*!50003 DROP PROCEDURE IF EXISTS `archiveReadNotifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `archiveReadNotifications`()
BEGIN
	START TRANSACTION;
		INSERT INTO archivednotifications SELECT unreadnotifications.*,NOW() FROM unreadnotifications WHERE isread = 1;
		DELETE FROM unreadnotifications WHERE isread = 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertCommentNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCommentNotification`(IN idcomment INT(11),IN idaffectinguser INT(11))
BEGIN
	DECLARE iduserpost  INT(11);
	DECLARE idaffecteduser  INT(11);
	DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
	SELECT user.iduser,userpost.iduserpost INTO idaffecteduser,iduserpost FROM userpost INNER JOIN usercomment ON usercomment.iduserpost = userpost.iduserpost INNER JOIN user ON userpost.iduser = user.iduser WHERE usercomment.idusercomment = idcomment LIMIT 1;
	SELECT userpostimage.imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = iduserpost;
    SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
    INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('comment',idaffectinguser,idaffecteduser,iduserpost,leftimgsrc,rightimgsrc,NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertPostExpiredNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertPostExpiredNotification`(IN idrepost INT(11),IN idaffectinguser INT(11),IN idaffecteduser INT(11))
BEGIN
 DECLARE leftimgsrc VARCHAR(55);
 SELECT userpostimage.imagesrc INTO leftimgsrc FROM userpostimage WHERE idrepost = userpostimage.iduserpost;
 INSERT INTO unreadnotifications(notificationtype,affectinguser,idpost,leftimagesrc,datestamp) VALUES ('postexpired',idaffectinguser,idrepost,leftimgsrc,NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadFriendRequestNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadFriendRequestNotification`(IN idaffectinguser INT(11),IN idaffecteduser INT(11))
BEGIN
 DECLARE leftimgsrc VARCHAR(55);
 SELECT user.imagesrc INTO leftimgsrc FROM user WHERE idaffectinguser = user.iduser; 
 INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,leftimagesrc,datestamp) VALUES ('friendrequest',idaffectinguser,idaffecteduser,leftimgsrc,NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadLikeNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadLikeNotification`(IN idlike INT(11),IN idaffectinguser INT(11))
BEGIN
	DECLARE iduserpost  INT(11);
	DECLARE idaffecteduser  INT(11);
	DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
	SELECT user.iduser,userpost.iduserpost INTO idaffecteduser,iduserpost FROM userpost INNER JOIN userpostlikes ON userpostlikes.iduserpost = userpost.iduserpost INNER JOIN user ON userpost.iduser = user.iduser WHERE userpostlikes.iduserpostlikes = idlike LIMIT 1;
    SELECT imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = iduserpost;
    SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
 INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('like',idaffectinguser,idaffecteduser,iduserpost,leftimgsrc,rightimgsrc,NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadRepostNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadRepostNotification`(IN idrepost INT(11),IN idaffectinguser INT(11))
BEGIN
	DECLARE idaffecteduser  INT(11);
    DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
    SELECT imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = idrepost;
    SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
	SELECT userpost.iduser INTO idaffecteduser FROM userpost WHERE userpost.iduserpost = idrepost LIMIT 1;
	INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('repost',idaffectinguser,idaffecteduser,idrepost,leftimgsrc,rightimgsrc,NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-15  1:33:19
