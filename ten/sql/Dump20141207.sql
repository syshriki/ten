CREATE DATABASE  IF NOT EXISTS `slinkdb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `slinkdb`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: slinkdb
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appinfo`
--

DROP TABLE IF EXISTS `appinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appinfo` (
  `idappinfo` int(11) NOT NULL,
  PRIMARY KEY (`idappinfo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code`
--

DROP TABLE IF EXISTS `code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code` (
  `idcode` int(11) NOT NULL AUTO_INCREMENT,
  `c_created` datetime DEFAULT NULL,
  `c_code` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcode`),
  UNIQUE KEY `c_code_UNIQUE` (`c_code`),
  CONSTRAINT `c_iduser` FOREIGN KEY (`idcode`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `code_gateway`
--

DROP TABLE IF EXISTS `code_gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code_gateway` (
  `idgatway_code` int(11) NOT NULL AUTO_INCREMENT,
  `cg_idcode` int(11) DEFAULT NULL,
  `cg_idgateway` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgatway_code`),
  KEY `gc_idcode_idx` (`cg_idcode`),
  KEY `gc_gateway_idx` (`cg_idgateway`),
  CONSTRAINT `cg_gateway` FOREIGN KEY (`cg_idgateway`) REFERENCES `gateway` (`idgateway`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cg_idcode` FOREIGN KEY (`cg_idcode`) REFERENCES `code` (`idcode`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `default_gateway`
--

DROP TABLE IF EXISTS `default_gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_gateway` (
  `iddefault_gateway` int(11) NOT NULL AUTO_INCREMENT,
  `dg_idgateway` int(11) DEFAULT NULL,
  `dg_iduser_default` int(11) DEFAULT NULL,
  PRIMARY KEY (`iddefault_gateway`),
  KEY `d_idgateway_idx` (`dg_idgateway`),
  KEY `dg_iduser_default_idx` (`dg_iduser_default`),
  CONSTRAINT `dg_idgateway` FOREIGN KEY (`dg_idgateway`) REFERENCES `gateway` (`idgateway`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `dg_iduser_default` FOREIGN KEY (`dg_iduser_default`) REFERENCES `user_default` (`iduser_default`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gateway`
--

DROP TABLE IF EXISTS `gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gateway` (
  `idgateway` int(11) NOT NULL AUTO_INCREMENT,
  `g_idgatewaytype` int(11) DEFAULT NULL,
  `g_iduser` int(11) DEFAULT NULL,
  `g_gatewayuser` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgateway`),
  KEY `ug_idgatewaytype_idx` (`g_idgatewaytype`),
  KEY `g_iduser_idx` (`g_iduser`),
  CONSTRAINT `g_iduser` FOREIGN KEY (`g_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gatewaytype`
--

DROP TABLE IF EXISTS `gatewaytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gatewaytype` (
  `idgatewaytype` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgatewaytype`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `u_email` varchar(100) DEFAULT NULL,
  `u_imgsrc` varchar(45) DEFAULT NULL,
  `u_lastactive` datetime DEFAULT NULL,
  `u_private` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_code`
--

DROP TABLE IF EXISTS `user_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_code` (
  `iduser_code` int(11) NOT NULL AUTO_INCREMENT,
  `uc_iduser` int(11) DEFAULT NULL,
  `uc_idcode` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduser_code`),
  KEY `uc_iduser_idx` (`uc_iduser`),
  KEY `uc_idcode_idx` (`uc_idcode`),
  CONSTRAINT `uc_idcode` FOREIGN KEY (`uc_idcode`) REFERENCES `code` (`idcode`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `uc_iduser` FOREIGN KEY (`uc_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_default`
--

DROP TABLE IF EXISTS `user_default`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_default` (
  `iduser_default` int(11) NOT NULL AUTO_INCREMENT,
  `ud_iduser` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduser_default`),
  KEY `ud_iduser_idx` (`ud_iduser`),
  CONSTRAINT `ud_iduser` FOREIGN KEY (`ud_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userauthentication`
--

DROP TABLE IF EXISTS `userauthentication`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userauthentication` (
  `iduserauthentication` int(11) NOT NULL AUTO_INCREMENT,
  `ua_iduser` int(11) DEFAULT NULL,
  `ua_salt` varchar(45) NOT NULL,
  `ua_username` varchar(45) NOT NULL,
  `ua_password` varchar(45) NOT NULL,
  PRIMARY KEY (`iduserauthentication`),
  UNIQUE KEY `ua_username_UNIQUE` (`ua_username`),
  UNIQUE KEY `iduser_UNIQUE` (`ua_iduser`),
  KEY `ua_iduser_idx` (`ua_iduser`),
  KEY `ua_username_idx` (`ua_username`),
  CONSTRAINT `ua_iduser` FOREIGN KEY (`ua_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userauthrecover`
--

DROP TABLE IF EXISTS `userauthrecover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userauthrecover` (
  `iduserauthrecover` int(11) NOT NULL,
  `uar_iduser` int(11) DEFAULT NULL,
  `uar_link` varchar(45) DEFAULT NULL,
  `uar_date` varchar(45) DEFAULT NULL,
  `uar_success` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`iduserauthrecover`),
  KEY `uar_iduser_idx` (`uar_iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping events for database 'slinkdb'
--

--
-- Dumping routines for database 'slinkdb'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-07 22:08:10
