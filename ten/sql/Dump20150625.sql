-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tensalt
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `tensalt`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tensalt` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tensalt`;

--
-- Table structure for table `usersalt`
--

DROP TABLE IF EXISTS `usersalt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersalt` (
  `iduser` int(11) NOT NULL,
  `salt` blob NOT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usersalt`
--

LOCK TABLES `usersalt` WRITE;
/*!40000 ALTER TABLE `usersalt` DISABLE KEYS */;
INSERT INTO `usersalt` VALUES (1,' !~SNbX�>J&�.�C]\n��+','2015-06-22 21:48:20'),(2,'x�-�#T��vD�A)^�3W/','2015-06-22 21:54:03'),(3,'���<yJ�\"�г�;���K{X�','2015-06-22 22:55:55');
/*!40000 ALTER TABLE `usersalt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tensalt'
--

--
-- Dumping routines for database 'tensalt'
--

--
-- Current Database: `tendb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tendb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tendb`;

--
-- Table structure for table `archivednotifications`
--

DROP TABLE IF EXISTS `archivednotifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `archivednotifications` (
  `idarchivednotifications` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `affectinguser` int(11) DEFAULT NULL,
  `affecteduser` int(11) DEFAULT NULL,
  `idpost` int(11) DEFAULT NULL,
  `idunreadnotification` int(11) DEFAULT NULL,
  `leftimage` varchar(55) DEFAULT NULL,
  `rightimage` varchar(55) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `archiveddatestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idarchivednotifications`),
  KEY `effectinguser2_idx` (`affectinguser`),
  KEY `effecteduser2_idx` (`affecteduser`),
  KEY `postid2_idx` (`idpost`),
  CONSTRAINT `effecteduser2` FOREIGN KEY (`affecteduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `effectinguser2` FOREIGN KEY (`affectinguser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `postid2` FOREIGN KEY (`idpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `archivednotifications`
--

LOCK TABLES `archivednotifications` WRITE;
/*!40000 ALTER TABLE `archivednotifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `archivednotifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `idfollowers` int(11) NOT NULL AUTO_INCREMENT,
  `idrequestee` int(11) DEFAULT NULL,
  `idrecipient` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idfollowers`),
  UNIQUE KEY `gsc` (`idrequestee`,`idrecipient`),
  KEY `idrequestee_idx` (`idrequestee`),
  KEY `idrecipient_idx` (`idrecipient`),
  CONSTRAINT `idrecipient` FOREIGN KEY (`idrecipient`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idrequestee` FOREIGN KEY (`idrequestee`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `followers`
--

LOCK TABLES `followers` WRITE;
/*!40000 ALTER TABLE `followers` DISABLE KEYS */;
INSERT INTO `followers` VALUES (1,2,1,'2015-06-22 22:19:11'),(2,3,2,'2015-06-22 22:56:06'),(3,3,1,'2015-06-22 23:47:38'),(4,1,2,'2015-06-22 23:58:29');
/*!40000 ALTER TABLE `followers` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`followers_AFTER_INSERT` AFTER INSERT ON `followers` FOR EACH ROW
BEGIN
CALL insertUnreadFriendRequestNotification(NEW.`idrequestee`,NEW.`idrecipient`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `hashtags`
--

DROP TABLE IF EXISTS `hashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtags` (
  `idhashtags` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(100) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idhashtags`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hashtags`
--

LOCK TABLES `hashtags` WRITE;
/*!40000 ALTER TABLE `hashtags` DISABLE KEYS */;
INSERT INTO `hashtags` VALUES (1,'#water','2015-06-22 23:36:10');
/*!40000 ALTER TABLE `hashtags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unreadnotifications`
--

DROP TABLE IF EXISTS `unreadnotifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unreadnotifications` (
  `idunreadnotifications` int(11) NOT NULL AUTO_INCREMENT,
  `notificationtype` varchar(45) DEFAULT NULL,
  `affectinguser` int(11) DEFAULT NULL,
  `affecteduser` int(11) DEFAULT NULL,
  `idpost` int(11) DEFAULT NULL,
  `isread` tinyint(1) DEFAULT '0',
  `leftimagesrc` varchar(55) DEFAULT NULL,
  `rightimagesrc` varchar(55) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idunreadnotifications`),
  KEY `effectinguser_idx` (`affectinguser`),
  KEY `effecteduser_idx` (`affecteduser`),
  KEY `postid_idx` (`idpost`),
  CONSTRAINT `effecteduser` FOREIGN KEY (`affecteduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `effectinguser` FOREIGN KEY (`affectinguser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `postid` FOREIGN KEY (`idpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unreadnotifications`
--

LOCK TABLES `unreadnotifications` WRITE;
/*!40000 ALTER TABLE `unreadnotifications` DISABLE KEYS */;
INSERT INTO `unreadnotifications` VALUES (1,'friendrequest',2,1,NULL,0,'',NULL,'2015-06-22 22:19:11'),(2,'repost',2,1,2,0,'',NULL,'2015-06-22 22:19:20'),(3,'repost',2,1,5,0,'',NULL,'2015-06-22 22:55:34'),(4,'friendrequest',3,2,NULL,0,'',NULL,'2015-06-22 22:56:06'),(5,'like',2,1,3,0,'',NULL,'2015-06-22 23:46:30'),(6,'comment',2,1,3,0,'',NULL,'2015-06-22 23:46:58'),(7,'friendrequest',3,1,NULL,0,'',NULL,'2015-06-22 23:47:37'),(8,'comment',3,1,3,0,'',NULL,'2015-06-22 23:47:52'),(9,'like',3,1,3,0,'',NULL,'2015-06-22 23:48:07'),(10,'like',3,2,4,0,'',NULL,'2015-06-22 23:57:15'),(11,'friendrequest',1,2,NULL,0,'',NULL,'2015-06-22 23:58:29'),(12,'like',1,2,4,0,'',NULL,'2015-06-22 23:58:39'),(13,'comment',3,2,4,0,'',NULL,'2015-06-23 00:27:42'),(14,'mention',1,2,8,0,'',NULL,'2015-06-24 21:43:12'),(33,'comment',2,1,8,0,'',NULL,'2015-06-24 22:18:05'),(34,'comment',2,1,3,0,'',NULL,'2015-06-24 22:21:23'),(35,'comment',2,1,3,0,'',NULL,'2015-06-24 22:22:26'),(36,'comment',2,1,3,0,'',NULL,'2015-06-24 22:23:09'),(37,'comment',2,1,3,0,'',NULL,'2015-06-24 22:23:47'),(38,'comment',2,1,3,0,'',NULL,'2015-06-24 22:26:40'),(39,'comment',2,1,3,0,'',NULL,'2015-06-24 22:28:18'),(44,'comment',2,1,3,0,'',NULL,'2015-06-24 22:31:58'),(45,'comment',2,1,3,0,'',NULL,'2015-06-24 22:37:29'),(64,'comment',2,1,3,0,'',NULL,'2015-06-24 23:10:02'),(65,'postexpired',2,2,11,0,'',NULL,'2015-06-24 23:14:52'),(66,'postexpired',1,1,5,0,'',NULL,'2015-06-24 23:22:52'),(67,'postexpired',3,3,7,0,'',NULL,'2015-06-24 23:22:52'),(68,'postexpired',1,1,8,0,'',NULL,'2015-06-24 23:22:52'),(69,'postexpired',2,2,9,0,'',NULL,'2015-06-24 23:22:52'),(70,'postexpired',2,2,10,0,'',NULL,'2015-06-24 23:22:52'),(71,'postexpired',2,2,12,0,'',NULL,'2015-06-24 23:38:52');
/*!40000 ALTER TABLE `unreadnotifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` blob NOT NULL,
  `datestamp` datetime DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `imagesrc` varchar(45) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `postsprivate` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'asdasd','ln����w�/��^PB/&����\Z�H�\0��]','2015-06-22 21:48:20','','asdasd','','',0),(2,'qweqwe','���&��5�t!��S����Y,�5]j-���i�','2015-06-22 21:54:03','sdfsdf','qweqwe','profile20150624232444567.jpg','',1),(3,'zxczxc','����C�j]y�χ�ƔJ]�e�H����ix��[','2015-06-22 22:55:55','','zxczxc','','',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userblocks`
--

DROP TABLE IF EXISTS `userblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userblocks` (
  `iduserblocks` int(11) NOT NULL AUTO_INCREMENT,
  `iduserblocked` int(11) DEFAULT NULL COMMENT 'Id of the user blocked',
  `iduserblocker` int(11) DEFAULT NULL COMMENT 'if of the users who is blocking the user',
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserblocks`),
  KEY `blocked_idx` (`iduserblocked`),
  KEY `blocker_idx` (`iduserblocker`),
  CONSTRAINT `blocked` FOREIGN KEY (`iduserblocked`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blocker` FOREIGN KEY (`iduserblocker`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='users can block other users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userblocks`
--

LOCK TABLES `userblocks` WRITE;
/*!40000 ALTER TABLE `userblocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `userblocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usercomment`
--

DROP TABLE IF EXISTS `usercomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usercomment` (
  `idusercomment` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`idusercomment`),
  KEY `iduser5_idx` (`iduser`),
  KEY `idpost_idx` (`iduserpost`),
  CONSTRAINT `idpost2` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser5` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COMMENT='user comment on other user posts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usercomment`
--

LOCK TABLES `usercomment` WRITE;
/*!40000 ALTER TABLE `usercomment` DISABLE KEYS */;
INSERT INTO `usercomment` VALUES (1,2,3,'2015-06-22 23:46:58','Commenting'),(2,3,3,'2015-06-22 23:47:52','2'),(3,3,4,'2015-06-23 00:27:42','Vhh'),(4,2,8,'2015-06-24 22:18:05','sfsdf'),(5,2,3,'2015-06-24 22:21:23','asd'),(6,2,3,'2015-06-24 22:22:26','123'),(7,2,3,'2015-06-24 22:23:09','asdasdasd'),(8,2,3,'2015-06-24 22:23:48','qweqwe'),(9,2,3,'2015-06-24 22:26:41','asdasd'),(10,2,3,'2015-06-24 22:28:19','asdasd'),(11,2,3,'2015-06-24 22:31:58','aSasas'),(12,2,3,'2015-06-24 22:37:30','543'),(13,2,3,'2015-06-24 23:10:02','sdadsausdlk ajslkd jaslkdj alsdkja slkdj aslkdj alksdj alksdj alskdj alskdj alksdj alksdj alksdj aklsjd ');
/*!40000 ALTER TABLE `usercomment` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`usercomment_AFTER_INSERT` AFTER INSERT ON `usercomment` FOR EACH ROW
BEGIN
     CALL insertCommentNotification(NEW.idusercomment,NEW.iduser);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `userfavorites`
--

DROP TABLE IF EXISTS `userfavorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userfavorites` (
  `iduserfavorites` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserfavorites`),
  KEY `iduser4_idx` (`iduser`),
  KEY `idpost3_idx` (`iduserpost`),
  CONSTRAINT `idpost3` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser6` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userfavorites`
--

LOCK TABLES `userfavorites` WRITE;
/*!40000 ALTER TABLE `userfavorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `userfavorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpost`
--

DROP TABLE IF EXISTS `userpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpost` (
  `iduserpost` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `datestamp` timestamp NULL DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `iduserpostrepost` int(11) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpost`),
  KEY `iduser_idx` (`iduser`),
  CONSTRAINT `iduser3` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpost`
--

LOCK TABLES `userpost` WRITE;
/*!40000 ALTER TABLE `userpost` DISABLE KEYS */;
INSERT INTO `userpost` VALUES (1,'asdasd','2016-06-23 01:48:47',1,0,'2016-06-22 21:58:47'),(2,'aSasasaSas','2016-06-23 01:49:59',1,0,'2016-06-22 22:01:59'),(3,'sqwe','2016-06-23 02:18:56',1,0,'2016-06-22 22:30:56'),(4,'aSasasaSas','2016-06-23 02:19:20',2,2,'2016-06-22 21:59:59'),(5,'test1','2015-06-23 02:55:26',1,0,'2015-06-22 23:05:26'),(6,'test1','2015-06-23 02:55:34',2,5,'2015-06-22 23:05:26'),(7,'#water','2015-06-23 03:36:10',3,0,'2015-06-22 23:36:10'),(8,'@qweqwe hey','2015-06-25 01:43:12',1,0,'2015-06-24 21:53:12'),(9,'asdasd','2015-06-25 01:53:43',2,0,'2015-06-24 22:03:43'),(10,'test','2015-06-25 01:53:53',2,0,'2015-06-24 22:03:53'),(11,'post expired notification','2015-06-25 03:04:26',2,0,'2015-06-24 23:14:26'),(12,'asasd','2015-06-25 03:28:36',2,0,'2015-06-24 23:38:36');
/*!40000 ALTER TABLE `userpost` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`userpost_AFTER_INSERT` AFTER INSERT ON `userpost` FOR EACH ROW
BEGIN
	IF NEW.iduserpostrepost IS NOT null AND NEW.iduserpostrepost <> 0 THEN
		CALL insertUnreadRepostNotification(NEW.iduserpostrepost,NEW.iduser);
	END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `userposthashtags`
--

DROP TABLE IF EXISTS `userposthashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userposthashtags` (
  `iduserposthashtags` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `idhashtag` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduserposthashtags`),
  KEY `userpost_idx` (`iduserpost`),
  KEY `hashtag_idx` (`idhashtag`),
  CONSTRAINT `hashtag` FOREIGN KEY (`idhashtag`) REFERENCES `hashtags` (`idhashtags`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userposthashtags`
--

LOCK TABLES `userposthashtags` WRITE;
/*!40000 ALTER TABLE `userposthashtags` DISABLE KEYS */;
INSERT INTO `userposthashtags` VALUES (1,7,'2015-06-22 23:36:10',1);
/*!40000 ALTER TABLE `userposthashtags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpostimage`
--

DROP TABLE IF EXISTS `userpostimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostimage` (
  `iduserpostimage` int(11) NOT NULL AUTO_INCREMENT,
  `imagesrc` varchar(55) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpostimage`),
  KEY `iduserpost_idx` (`iduserpost`),
  CONSTRAINT `usepost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpostimage`
--

LOCK TABLES `userpostimage` WRITE;
/*!40000 ALTER TABLE `userpostimage` DISABLE KEYS */;
/*!40000 ALTER TABLE `userpostimage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userpostlikes`
--

DROP TABLE IF EXISTS `userpostlikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostlikes` (
  `iduserpostlikes` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL COMMENT 'The user who liked the post',
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpostlikes`),
  KEY `iduser4_idx` (`iduser`),
  KEY `idpost_idx` (`iduserpost`),
  CONSTRAINT `idpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser4` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userpostlikes`
--

LOCK TABLES `userpostlikes` WRITE;
/*!40000 ALTER TABLE `userpostlikes` DISABLE KEYS */;
INSERT INTO `userpostlikes` VALUES (1,3,2,'2015-06-22 23:46:31'),(2,3,3,'2015-06-22 23:48:07'),(3,4,3,'2015-06-22 23:57:15'),(4,4,1,'2015-06-22 23:58:40');
/*!40000 ALTER TABLE `userpostlikes` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `tendb`.`userpostlikes_AFTER_INSERT` AFTER INSERT ON `userpostlikes` FOR EACH ROW
BEGIN
    CALL insertUnreadLikeNotification(NEW.iduserpostlikes,NEW.iduser);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping events for database 'tendb'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `postExpired` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8 */ ;;
/*!50003 SET character_set_results = utf8 */ ;;
/*!50003 SET collation_connection  = utf8_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`root`@`localhost`*/ /*!50106 EVENT `postExpired` ON SCHEDULE EVERY 2 MINUTE STARTS '2015-06-24 23:02:52' ON COMPLETION NOT PRESERVE ENABLE DO CALL notifyExpired() */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'tendb'
--
/*!50003 DROP PROCEDURE IF EXISTS `archiveReadNotifications` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `archiveReadNotifications`()
BEGIN
	START TRANSACTION;
		INSERT INTO archivednotifications SELECT unreadnotifications.*,NOW() FROM unreadnotifications WHERE isread = 1;
		DELETE FROM unreadnotifications WHERE isread = 1;
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertCommentNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertCommentNotification`(IN idcomment INT(11),IN idaffectinguser INT(11))
BEGIN
	DECLARE iduserpost  INT(11);
	DECLARE idaffecteduser  INT(11);
	DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
	SELECT user.iduser,userpost.iduserpost INTO idaffecteduser,iduserpost FROM userpost INNER JOIN usercomment ON usercomment.iduserpost = userpost.iduserpost INNER JOIN user ON userpost.iduser = user.iduser WHERE usercomment.idusercomment = idcomment LIMIT 1;
	IF idaffectinguser != idaffecteduser THEN #For efficiency no need to continue if condition met
		SELECT userpostimage.imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = iduserpost;
		SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
		INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('comment',idaffectinguser,idaffecteduser,iduserpost,leftimgsrc,rightimgsrc,NOW());
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertPostExpiredNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertPostExpiredNotification`(IN idrepost INT(11),IN idaffecteduser INT(11))
BEGIN
 INSERT INTO unreadnotifications(notificationtype,affecteduser,affectinguser,idpost,leftimagesrc,datestamp) VALUES ('postexpired',idaffecteduser,idaffecteduser,idrepost,"",NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadFriendRequestNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadFriendRequestNotification`(IN idaffectinguser INT(11),IN idaffecteduser INT(11))
BEGIN
 DECLARE leftimgsrc VARCHAR(55);
 	IF idaffectinguser != idaffecteduser THEN #For efficiency no need to continue if condition met
	 SELECT user.imagesrc INTO leftimgsrc FROM user WHERE idaffectinguser = user.iduser; 
	 INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,leftimagesrc,datestamp) VALUES ('friendrequest',idaffectinguser,idaffecteduser,leftimgsrc,NOW());
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadLikeNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadLikeNotification`(IN idlike INT(11),IN idaffectinguser INT(11))
BEGIN
	DECLARE iduserpost  INT(11);
	DECLARE idaffecteduser  INT(11);
	DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
	SELECT user.iduser,userpost.iduserpost INTO idaffecteduser,iduserpost FROM userpost INNER JOIN userpostlikes ON userpostlikes.iduserpost = userpost.iduserpost INNER JOIN user ON userpost.iduser = user.iduser WHERE userpostlikes.iduserpostlikes = idlike LIMIT 1;
	IF idaffectinguser != idaffecteduser THEN #For efficiency no need to continue if condition met
		SELECT imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = iduserpost;
		SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
		INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('like',idaffectinguser,idaffecteduser,iduserpost,leftimgsrc,rightimgsrc,NOW());
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadMentionNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadMentionNotification`(IN idaffectinguser INT(11),IN idaffecteduser INT(11),IN iduserpost INT(11))
BEGIN
	DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
	IF idaffectinguser != idaffecteduser THEN #For efficiency no need to continue if condition met
		SELECT imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = iduserpost;
		SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
		INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('mention',idaffectinguser,idaffecteduser,iduserpost,leftimgsrc,rightimgsrc,NOW());
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUnreadRepostNotification` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUnreadRepostNotification`(IN idrepost INT(11),IN idaffectinguser INT(11))
BEGIN
	DECLARE idaffecteduser  INT(11);
    DECLARE leftimgsrc VARCHAR(55);
    DECLARE rightimgsrc VARCHAR(55);
    SELECT imagesrc INTO rightimgsrc FROM userpostimage WHERE userpostimage.iduserpost = idrepost;
    SELECT user.imagesrc INTO leftimgsrc FROM user WHERE user.iduser = idaffectinguser;
	SELECT userpost.iduser INTO idaffecteduser FROM userpost WHERE userpost.iduserpost = idrepost LIMIT 1;
	INSERT INTO unreadnotifications(notificationtype,affectinguser,affecteduser,idpost,leftimagesrc,rightimagesrc,datestamp) VALUES ('repost',idaffectinguser,idaffecteduser,idrepost,leftimgsrc,rightimgsrc,NOW());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `notifyExpired` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `notifyExpired`()
BEGIN
	DECLARE postid INT(11);
    DECLARE userid INT(11);
    DECLARE done INT DEFAULT 0;
	DECLARE expired CURSOR FOR SELECT up.iduser,up.iduserpost FROM tendb.userpost AS up WHERE up.expiration <= NOW() AND up.iduserpostrepost=0 AND up.iduserpost NOT IN (SELECT idpost FROM tendb.unreadnotifications WHERE idpost = up.iduserpost AND notificationtype='postexpired');
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    OPEN expired;
    the_loop: LOOP
		FETCH expired INTO userid,postid;
        SELECT postid;
        IF done =1 THEN
            LEAVE the_loop;
        END IF;
		CALL insertPostExpiredNotification(postid,userid);
    END LOOP the_loop;
    CLOSE expired;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-25 18:30:11
