-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: tensalt
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `tensalt`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tensalt` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tensalt`;

--
-- Table structure for table `usersalt`
--

DROP TABLE IF EXISTS `usersalt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usersalt` (
  `iduser` int(11) NOT NULL,
  `salt` blob NOT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Current Database: `tendb`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tendb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tendb`;

--
-- Table structure for table `followers`
--

DROP TABLE IF EXISTS `followers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followers` (
  `idfollowers` int(11) NOT NULL AUTO_INCREMENT,
  `idrequestee` int(11) DEFAULT NULL,
  `idrecipient` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idfollowers`),
  KEY `idrequestee_idx` (`idrequestee`),
  KEY `idrecipient_idx` (`idrecipient`),
  CONSTRAINT `idrecipient` FOREIGN KEY (`idrecipient`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idrequestee` FOREIGN KEY (`idrequestee`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `hashtags`
--

DROP TABLE IF EXISTS `hashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtags` (
  `idhashtags` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(100) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`idhashtags`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notificationmention`
--

DROP TABLE IF EXISTS `notificationmention`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationmention` (
  `idnotificationmention` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `idusermentioned` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idnotificationmention`),
  KEY `iduserpost3_idx` (`iduserpost`),
  KEY `idusermentioned_idx` (`idusermentioned`),
  CONSTRAINT `idusermentioned` FOREIGN KEY (`idusermentioned`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduserpost3` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notificationrepost`
--

DROP TABLE IF EXISTS `notificationrepost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationrepost` (
  `idnotificationrepost` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `read` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idnotificationrepost`),
  KEY `iduser_idx` (`iduser`),
  KEY `iduserpost_idx` (`iduserpost`),
  CONSTRAINT `iduser` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduserpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='when your shit gets reposted';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notificationrequest`
--

DROP TABLE IF EXISTS `notificationrequest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notificationrequest` (
  `idnotificationrequest` int(11) NOT NULL AUTO_INCREMENT,
  `iduserreciever` int(11) DEFAULT NULL,
  `iduserrequester` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `read` tinytext,
  PRIMARY KEY (`idnotificationrequest`),
  KEY `reciever_idx` (`iduserreciever`),
  CONSTRAINT `reciever` FOREIGN KEY (`iduserreciever`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `requester` FOREIGN KEY (`iduserreciever`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='when a freind request is recived';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` blob NOT NULL,
  `datestamp` datetime DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `imagesrc` varchar(45) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `postsprivate` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=674 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userblocks`
--

DROP TABLE IF EXISTS `userblocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userblocks` (
  `iduserblocks` int(11) NOT NULL AUTO_INCREMENT,
  `iduserblocked` int(11) DEFAULT NULL COMMENT 'Id of the user blocked',
  `iduserblocker` int(11) DEFAULT NULL COMMENT 'if of the users who is blocking the user',
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserblocks`),
  KEY `blocked_idx` (`iduserblocked`),
  KEY `blocker_idx` (`iduserblocker`),
  CONSTRAINT `blocked` FOREIGN KEY (`iduserblocked`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `blocker` FOREIGN KEY (`iduserblocker`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='users can block other users';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usercomment`
--

DROP TABLE IF EXISTS `usercomment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usercomment` (
  `idusercomment` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`idusercomment`),
  KEY `iduser5_idx` (`iduser`),
  KEY `idpost_idx` (`iduserpost`),
  CONSTRAINT `idpost2` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser5` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1 COMMENT='user comment on other user posts';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userfavorites`
--

DROP TABLE IF EXISTS `userfavorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userfavorites` (
  `iduserfavorites` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserfavorites`),
  KEY `iduser4_idx` (`iduser`),
  KEY `idpost3_idx` (`iduserpost`),
  CONSTRAINT `idpost3` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser6` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpost`
--

DROP TABLE IF EXISTS `userpost`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpost` (
  `iduserpost` int(11) NOT NULL AUTO_INCREMENT,
  `text` text,
  `datestamp` timestamp NULL DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `iduserpostrepost` int(11) DEFAULT '-1',
  PRIMARY KEY (`iduserpost`),
  KEY `iduser_idx` (`iduser`),
  CONSTRAINT `iduser3` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=783 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userposthashtags`
--

DROP TABLE IF EXISTS `userposthashtags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userposthashtags` (
  `iduserposthashtags` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  `idhashtag` int(11) DEFAULT NULL,
  PRIMARY KEY (`iduserposthashtags`),
  KEY `userpost_idx` (`iduserpost`),
  KEY `hashtag_idx` (`idhashtag`),
  CONSTRAINT `hashtag` FOREIGN KEY (`idhashtag`) REFERENCES `hashtags` (`idhashtags`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpostimage`
--

DROP TABLE IF EXISTS `userpostimage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostimage` (
  `iduserpostimage` int(11) NOT NULL AUTO_INCREMENT,
  `imagesrc` varchar(55) DEFAULT NULL,
  `iduserpost` int(11) DEFAULT NULL,
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpostimage`),
  KEY `iduserpost_idx` (`iduserpost`),
  CONSTRAINT `usepost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userpostlikes`
--

DROP TABLE IF EXISTS `userpostlikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userpostlikes` (
  `iduserpostlikes` int(11) NOT NULL AUTO_INCREMENT,
  `iduserpost` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL COMMENT 'The user who liked the post',
  `datestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`iduserpostlikes`),
  KEY `iduser4_idx` (`iduser`),
  KEY `idpost_idx` (`iduserpost`),
  CONSTRAINT `idpost` FOREIGN KEY (`iduserpost`) REFERENCES `userpost` (`iduserpost`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `iduser4` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-15 23:46:33
