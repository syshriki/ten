package com.nvc.utilities;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordProtectionUtility {
	
	//TODO make non static
	private static final int SALT_LENGTH = 20;
	private static final int ITERATIONS = 10000;
	private static final int KEY_LENGTH = 256;

	public byte[] generateSalt() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[SALT_LENGTH];
		random.nextBytes(bytes);
		return bytes;

	}

	public byte[] applyHash(char[] password, byte[] salt) {
		PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
		Arrays.fill(password, Character.MIN_VALUE);
		try {
			SecretKeyFactory skf = SecretKeyFactory
					.getInstance("PBKDF2WithHmacSHA1");
			return skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new AssertionError("Error while hashing a password: "
					+ e.getMessage(), e);
		} finally {
			spec.clearPassword();
		}
	}

	public char[] bytesToChars(byte[] bytes) {
		char[] convertedChar = new char[bytes.length];
		for (int i = 0; i < bytes.length; i++) {
			convertedChar[i] = (char) bytes[i];
		}
		return convertedChar;
	}

	public boolean isExpectedPassword(byte[] passwordbytes, byte[] salt,
			byte[] expectedHash) {
		char[] password = bytesToChars(passwordbytes);
		byte[] pwdHash = applyHash(password, salt);
		Arrays.fill(password, Character.MIN_VALUE);
		if (pwdHash.length != expectedHash.length)
			return false;
		for (int i = 0; i < pwdHash.length; i++) {
			if (pwdHash[i] != expectedHash[i])
				return false;
		}
		return true;
	}

}
