package com.nvc.utilities;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;

import com.nvc.ten.dao.FollowersDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.models.FollowersModel;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.PostHashtagModel;
import com.nvc.ten.models.PostImageModel;
import com.nvc.ten.models.extended.NewsfeedObject;
import com.nvc.ten.models.extended.CommentObject;
import com.nvc.ten.models.extended.MentionObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.HashtagObject;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.LikeObject;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.FileSizeError;
import com.nvc.ten.throwable.service.InvalidFileFormatException;
import com.nvc.ten.throwable.service.InvalidParameter;

public class TenUtilities {

	private static final long MEGABYTE = 1024L * 1024L;

	public static boolean validUsername(String username) {
		if (username.length() < BaseServlet.USERNAME_MAX_LENGTH
				&& username.length() >= 5)
			return true;
		return false;
	}

	public boolean usernameExists(String username)
			throws ClassNotFoundException, SQLException, InvalidParameter {
		if (username == null || username.equals(""))
			throw new InvalidParameter("username");
		UserDAO userdao = new UserDAO();
		// If a user is returned then the user does exist
		return userdao.findByUsername(username) != null;
	}

	public static String uploadProfilePicture(FileItem item, int iduser) {
		TenUtilities globalUtilities = new TenUtilities();
		String fileprefix = "profile" + TenUtilities.getCurrentDatestamp();
		String fn = null;
		try {
			fn = globalUtilities.uploadImage(
					fileprefix.replaceAll("[^a-zA-Z0-9]", ""), item, iduser);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return fn;
	}

	public static Timestamp getCurrentDatestamp() {
		return new Timestamp(new java.util.Date().getTime());
	}

	public static <T> Set<T> union(Set<T> setA, Set<T> setB) {
		Set<T> tmp = new TreeSet<T>(setA);
		tmp.addAll(setB);
		return tmp;
	}

	public byte[] extractBytes(String ImageName) throws IOException {
		// open image
		File file = new File(ImageName);
		FileInputStream imageInFile = new FileInputStream(file);
		byte imageData[] = new byte[(int) file.length()];
		imageInFile.read(imageData);
		imageInFile.close();
		System.out.println(ImageName + " loaded!");
		return imageData;
	}

	public String encodeToBase64String(byte[] data) {
		String byteDataAsString = Base64.encodeBase64String(data);
		return byteDataAsString;
	}

	private String applyImageExtension(String contentType, String filename) {
		if (contentType.equals("image/png")) {
			return filename.concat(".png");
		} else if (contentType.equals("image/jpg")
				|| contentType.equals("image/jpeg")) {
			return filename.concat(".jpg");
		} else if (filename.equals("image/gif")) {
			return filename.concat(".gif");
		} else {
			System.out.println("invalid file format");
			return filename;
		}
	}

	public String uploadImage(String filename, FileItem item, int iduser)
			throws Exception {
		String imageName = null;
		if (isSupportedImage(item)) {
			imageName = applyImageExtension(item.getContentType(), filename);
			System.out.println("supported");
			processUploadedFile(item, imageName, iduser);
		} else {
			System.out.println("Format not supported (" + item.getContentType()
					+ ")");
			throw new InvalidFileFormatException();
		}
		return imageName;
	}

	public boolean isSupportedImage(FileItem item) {
		return (!item.isFormField() && BaseServlet.SUPPORTED_IMAGE_TYPES
				.contains(item.getContentType()));
	}

	private Dimension getImageDimensions(byte[] image)
			throws ImageReadException, IOException {
		Dimension d = null;
		d = Imaging.getImageSize(image);

		return d;
	}

	private double calcResizeFactor(double curr_width, double curr_height,
			int max_width, int max_height) {
		double widthScaleFactor = curr_width / max_width;
		double heightScaleFactor = curr_height / max_height;
		double scaleFactor = Math.max(widthScaleFactor, heightScaleFactor);
		return scaleFactor;
	}

	private Dimension getScaledDimensions(byte[] image, int max_width,
			int max_height) throws ImageReadException, IOException {
		Dimension currentImageDimensions = getImageDimensions(image);
		double rescaleFactor = calcResizeFactor(
				currentImageDimensions.getWidth(),
				currentImageDimensions.getHeight(), max_width, max_height);
		System.out.println(rescaleFactor);
		if (rescaleFactor > 1) {// This means the image has a dimensions outside
								// the max height and max width and needs to be
								// scaled by rescaleFactor
			int scaledWidth = (int) Math.round(currentImageDimensions
					.getWidth() / rescaleFactor);
			int scaledHeight = (int) Math.round(currentImageDimensions
					.getHeight() / rescaleFactor);
			System.out.println("resize that bish");
			return new Dimension(scaledWidth, scaledHeight);
		}
		return currentImageDimensions;
	}

	private void processUploadedFile(FileItem item, String filename, int iduser)
			throws Exception {
		Dimension scaledDimensions = getScaledDimensions(item.get(),
				BaseServlet.MAX_IMAGE_WIDTH, BaseServlet.MAX_IMAGE_HEIGHT);
		File uploadDir = new File(BaseServlet.PHOTO_STORE_PATH + iduser + "/");
		File uploadedFile = new File(BaseServlet.PHOTO_STORE_PATH + iduser
				+ "/" + filename);
		if (bytesToMeg(item.getSize()) > BaseServlet.MAX_IMAGE_FILE_SIZE)
			throw new FileSizeError("Post image - global util");
		if (!uploadDir.exists()) {
			uploadDir.mkdirs();
		}

		if (!uploadedFile.exists()) {
			uploadedFile.createNewFile();
		}
		item.write(uploadedFile);
		Thumbnails
				.of(BaseServlet.PHOTO_STORE_PATH + iduser + "/" + filename)
				.size((int) scaledDimensions.getWidth(),
						(int) scaledDimensions.getHeight())
				.toFile(new File(BaseServlet.PHOTO_STORE_PATH + iduser + "/"
						+ filename));
	}

	public static float getFileSizeInMB(String fileName) {
		float ret = getFileSizeInBytes(fileName);
		ret = ret / (float) (1024 * 1024);
		return ret;
	}

	public static long getFileSizeInBytes(String fileName) {
		long ret = 0;
		File f = new File(fileName);
		if (f.isFile()) {
			return f.length();
		} else if (f.isDirectory()) {
			File[] contents = f.listFiles();
			for (int i = 0; i < contents.length; i++) {
				if (contents[i].isFile()) {
					ret += contents[i].length();
				} else if (contents[i].isDirectory())
					ret += getFileSizeInBytes(contents[i].getPath());
			}
		}
		return ret;
	}

	public static long bytesToMeg(long bytes) {
		return bytes / MEGABYTE;
	}

	public boolean isFollowingUser(int followingId,int beingFollowedId)
			throws ClassNotFoundException, SQLException {
		FollowersDAO userpdairdao = new FollowersDAO();
		FollowersModel followersModel = userpdairdao.findFollowingID(followingId, beingFollowedId);
		return followersModel != null && followersModel.getApproved()==1;
	}

	public boolean postIsPrivate(int postid) throws ClassNotFoundException,
			SQLException {
		UserPostDAO postdao = new UserPostDAO();
		PostModel postdto = postdao.findByID(postid);
		UserDAO userdao = new UserDAO();
		UserModel userdto = userdao.findByID(postdto.getIdUser());
		return userdto.isPrivate();
	}

	public int followingStatus(int useridRequesting, int user2id)
			throws ClassNotFoundException, SQLException {
		FollowersDAO followersdao = new FollowersDAO();
		FollowersModel following = followersdao.findFollowingID(useridRequesting, user2id);
		return following!=null ? following.getApproved() : -1;
	}

	// This is where the resultset for the news feed is proccessed and put into
	// a user object
	public NewsfeedObject proccessPostListResultSet(
			ResultSet newsfeed_resultset, int userid) throws SQLException,
			IOException, ClassNotFoundException {
		NewsfeedObject newsfeed = new NewsfeedObject();
		List<PostObject> reposts = new ArrayList<>();
		while (newsfeed_resultset.next()) {
			PostObject post = new PostObject();

			if (newsfeed_resultset.getInt(1) > 0) {
				post.setIdUserPost(newsfeed_resultset.getInt(1));
				post.setText(newsfeed_resultset.getString(2));
				post.setDatestamp(newsfeed_resultset.getTimestamp(3));
				post.setIdUser(newsfeed_resultset.getInt(4));
				post.setIdUserPostRepost(newsfeed_resultset.getInt(5));
				post.setExpiration(newsfeed_resultset.getTimestamp(6));
			}

			// Get the user like for the post
			LikeObject like = new LikeObject();
			if (newsfeed_resultset.getInt(7) > 0) {
				like.setIdUserPostLike(newsfeed_resultset.getInt(7));
				like.setIdUserPost(newsfeed_resultset.getInt(8));
				like.setIdUser(newsfeed_resultset.getInt(9));
				like.setDatestamp(newsfeed_resultset.getTimestamp(10));
			}

			// User who liked
			if (newsfeed_resultset.getInt(11) > 0) {
				UserModel user = new UserModel();
				user.setIdUser(newsfeed_resultset.getInt(11));
				user.setUsername(newsfeed_resultset.getString(12));
				user.setPassword(newsfeed_resultset.getBytes(13));
				user.setDatestamp(newsfeed_resultset.getTimestamp(14));
				user.setDescription(newsfeed_resultset.getString(15));
				user.setEmail(newsfeed_resultset.getString(16));
				user.setImagesrc(newsfeed_resultset.getString(17));
				user.setWebsite(newsfeed_resultset.getString(18));
				user.setPrivate(newsfeed_resultset.getBoolean(19));
				like.setUserObject(new UserObject(user));
				post.addLike(like);
			}
			// end got like and user who liked

			// Get all comments for the post
			CommentObject comment = new CommentObject();
			if (newsfeed_resultset.getInt(20) > 0) {
				comment.setIdUserComment(newsfeed_resultset.getInt(20));
				comment.setIdUser(newsfeed_resultset.getInt(21));
				comment.setIdUserPost(newsfeed_resultset.getInt(22));
				comment.setDatestamp(newsfeed_resultset.getTimestamp(23));
				comment.setComment(newsfeed_resultset.getString(24));
			}

			// User who commented
			if (newsfeed_resultset.getInt(25) > 0) {
				UserModel user = new UserModel();
				user.setIdUser(newsfeed_resultset.getInt(25));
				user.setUsername(newsfeed_resultset.getString(26));
				user.setPassword(newsfeed_resultset.getBytes(27));
				user.setDatestamp(newsfeed_resultset.getTimestamp(28));
				user.setDescription(newsfeed_resultset.getString(29));
				user.setEmail(newsfeed_resultset.getString(30));
				user.setImagesrc(newsfeed_resultset.getString(31));
				user.setWebsite(newsfeed_resultset.getString(32));
				user.setPrivate(newsfeed_resultset.getBoolean(33));
				comment.setUser(user);
				post.addComment(comment);
			}

			// Get image associated with post
			if (newsfeed_resultset.getInt(34) > 0) {
				PostImageModel image = new PostImageModel();
				image.setIdUserPostImage(newsfeed_resultset.getInt(34));
				image.setImageSrc(newsfeed_resultset.getString(35));
				image.setIdUserPost(newsfeed_resultset.getInt(36));
				image.setDatestamp(newsfeed_resultset.getTimestamp(37));
				post.addImage(new ImageObject(image));
			}

			// Get all hashtags associated with post
			if (newsfeed_resultset.getInt(38) > 0) {
				PostHashtagModel hashtag = new PostHashtagModel();
				hashtag.setUserPostHashtagid(newsfeed_resultset.getInt(38));
				hashtag.setUserPostid(newsfeed_resultset.getInt(39));
				hashtag.setDatestamp(newsfeed_resultset.getTimestamp(40));
				hashtag.setHashtagid(newsfeed_resultset.getInt(41));

				post.addHashtag(new HashtagObject(hashtag));
			}

			// User who commented
			if (newsfeed_resultset.getInt(42) > 0) {
				UserModel user = new UserModel();
				user.setIdUser(newsfeed_resultset.getInt(42));
				user.setUsername(newsfeed_resultset.getString(43));
				user.setPassword(newsfeed_resultset.getBytes(44));
				user.setDatestamp(newsfeed_resultset.getTimestamp(45));
				user.setDescription(newsfeed_resultset.getString(46));
				user.setEmail(newsfeed_resultset.getString(47));
				user.setImagesrc(newsfeed_resultset.getString(48));
				user.setWebsite(newsfeed_resultset.getString(49));
				user.setPrivate(newsfeed_resultset.getBoolean(50));
				post.setUser(new UserObject(user));
			}

			UserPostDAO postdao = new UserPostDAO();
			post.setReposted(postdao.userReposted(userid, post.getIdUserPost()));
			post.setMentions(extractUserMentions(post));
			// Now build up the post object, merging of posts is implemented in
			// add function
			// This will throw and error if a post with a weird id is passed
			// through
			if (post.getIdUserPostRepost() > 0)
				reposts.add(post);
			else
				newsfeed.add(post);
		}

		// Load images for news feed and check for repost
		for (PostObject post : newsfeed.getNewsfeed()) {
			post.loadImageData();
			for (PostObject repost : reposts) {
				if (repost.getIdUserPostRepost() == post.getIdUserPost())
					post.setReposter(repost.getUser());
			}

		}
		return newsfeed;
	}

	public Set<MentionObject> extractUserMentions(PostObject upo) {
		UserDAO userdao = new UserDAO();
		Set<MentionObject> mentions = new HashSet<MentionObject>();
		Pattern pattern = Pattern.compile("/\\s@\\{[a-zA-Z0-9_]{5,}\\/}");
		Matcher matcher = pattern.matcher(upo.getText());
		while (matcher.find()) {
			UserModel user;
			MentionObject mention;
			try {
				user = userdao.findByUsername(matcher.group());
				mention = new MentionObject();
				mention.setMentioned(new UserObject(user));
				mention.setMentionerId(upo.getIdUser());
				mentions.add(mention);
			} catch (ClassNotFoundException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return mentions;
	}
}
