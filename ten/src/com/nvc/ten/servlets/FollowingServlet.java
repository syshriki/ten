package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.models.transfer.FollowingTO;
import com.nvc.ten.models.transfer.UserTO;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.InvalidID;

public class FollowingServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6030930215107809727L;

	// localhost:8080/ten/following

	@SuppressWarnings("unchecked")
	@Override
	protected Map<String,List<FindFriendTO>> startService(HttpServletRequest request,
			HttpServletResponse response) {
		FollowService followerservice = new FollowService();
		Map<String,List<FindFriendTO>> users = new HashMap<>();
		String useridAsString = request.getParameter("userid");
		int userid = Integer.parseInt(useridAsString);
		String offsetString = request.getParameter("offset");
		String countString = request.getParameter("count");  
		
		int offset = offsetString!=null ? Integer.parseInt(request.getParameter("offset")) : 0;
		int count = countString!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_FOLLOWING_RETURN_COUNT;
		
		if(count > this.MAX_FOLLOWING_RETURN_COUNT){
			count = this.DEFAULT_FOLLOWING_RETURN_COUNT;
		}
		
		if (userid > 0) {
			try {
				users.put("users", followerservice.getFollowing(userid,this.uid,offset,count));
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidID e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return users;
	}
}
