package com.nvc.ten.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.nvc.ten.services.CommentService;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.NotFollowingPrivateException;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class CommentServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3542109560430333161L;

	// localhost:8080/ten/comment?postid=<ID>&message=<MESSAGE>

	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
		CommentService commentservice = new CommentService();
		String postidAsString = "";// = request.getParameter("postid");
		String messageAsString = "";// = request.getParameter("message");

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (isMultipart) {
			ServletContext servletContext = this.getServletConfig()
					.getServletContext();
			File repository = (File) servletContext
					.getAttribute("javax.servlet.context.tempdir");

			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

			// Configure a repository (to ensure a secure temp location
			// is
			// used)
			factory.setRepository(repository);

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			try {
				List<FileItem> items = upload.parseRequest(request);
				for(FileItem fileItem : items){
					if(fileItem.getFieldName().equals("message"))
						messageAsString = fileItem.getString();
					if(fileItem.getFieldName().equals("postid"))
						postidAsString = fileItem.getString();
				}
			} catch (FileUploadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String comment = messageAsString;
		int postid = Integer.parseInt(postidAsString);
		int userid = this.uid;
		try {
			return commentservice.createComment(postid, comment, userid);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (InsertFailed e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (InvalidID e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
		} catch (InvalidParameter e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			//e.printStackTrace();
		} catch (CharLengthError e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			//e.printStackTrace();
		} catch (NotFollowingPrivateException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			//e.printStackTrace();
		}
		return null;
	}

}
