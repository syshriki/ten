package com.nvc.ten.servlets;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.NotificationService;

public class CheckNotificationsServlet extends BaseServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3382089022534256550L;

	@Override
	protected Map<String,Integer> startService(HttpServletRequest request,
			HttpServletResponse response) {
		Integer count = null;
		NotificationService notificationService = new NotificationService();
		count = notificationService.getUnreadNotificationCount(this.uid);
		
		Map<String,Integer> results = new HashMap<String,Integer>();
		results.put("count",count);
		return results;
	}

}
