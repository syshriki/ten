package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.LikeService;
import com.nvc.ten.services.RepostService;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.NotFollowingPrivateException;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class RepostServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3542109560430333161L;

	// localhost:8080/ten/repost?postid=<POSTID>

	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
		RepostService repostservice = new RepostService();
		String postidAsString = request.getParameter("postid");

		int postid = Integer.parseInt(postidAsString);
		int userid = this.uid;
		try {
			return repostservice.repost(postid, userid);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotFollowingPrivateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
