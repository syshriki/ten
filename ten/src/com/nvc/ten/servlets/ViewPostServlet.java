package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.ten.models.transfer.PostTO;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.services.PostService;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class ViewPostServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6030930215107809727L;

	// localhost:8080/ten/view?postid=<POSTID>

	@SuppressWarnings("unchecked")
	@Override
	protected PostTO startService(HttpServletRequest request,
			HttpServletResponse response) {
		PostService postservice = new PostService();
			String postidAsString = request.getParameter("postid");
			PostObject postobject = null;
			try {
				System.out.println(this.uid + " viewing post "+ postidAsString);
				postobject = postservice.getPostObject(Integer
						.parseInt(postidAsString),this.uid);
				return new PostTO(postobject);
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidID e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return null;
	}

}
