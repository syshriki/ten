package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.FindFriendsTO;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.InvalidID;

public class PendingFollowersServlet  extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3542109560430333161L;

	// localhost:8080/ten/like?postid=<ID>

	@SuppressWarnings("unchecked")
	@Override
	protected FindFriendsTO startService(HttpServletRequest request,
			HttpServletResponse response) {
		FollowService followservice = new FollowService();

		int userid = this.uid;
		String offsetString = request.getParameter("offset");
		String countString = request.getParameter("count");  
		
		int offset = offsetString!=null ? Integer.parseInt(request.getParameter("offset")) : 0;
		int count = countString!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_PENDING_RETURN_COUNT;
		
		if(count > this.MAX_PENDING_RETURN_COUNT){
			count = this.DEFAULT_PENDING_RETURN_COUNT;
		}
		
		try {
			return followservice.getPending(userid,offset,count);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidID e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}