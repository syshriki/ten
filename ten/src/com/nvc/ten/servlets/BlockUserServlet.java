package com.nvc.ten.servlets;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.BlockService;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.NullReturned;

@SuppressWarnings("serial")
public class BlockUserServlet extends BaseServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 70873665015757077L;

	// localhost:8080/ten/block?userid=<USER_ID>

	@SuppressWarnings("unchecked")
	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
			//Unfollow then add to block list
			String useridAsString = request.getParameter("userid");
			int userid = Integer.parseInt(useridAsString);
			FollowService followerservice = new FollowService();
			BlockService blockservice = new BlockService();
			
			try {
				int block = blockservice.blockUser(this.uid, userid);
				//Both users unfollow eachother
				int unfollow1 = followerservice.unfollowUser(this.uid,userid);
				int unfollow2 = followerservice.unfollowUser(userid, this.uid);
				if(block == 1 && unfollow1 == 1 || unfollow2 == 1)
					return 1;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidID e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullReturned e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return 0;
	}

}
