package com.nvc.ten.servlets;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.services.PostLikersService;

public class PostLikersServlet  extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3542109560430333161L;

	// localhost:8080/ten/postlikers?postid=<ID>

	@SuppressWarnings("unchecked")
	@Override
	protected Map<String,List<FindFriendTO>>  startService(HttpServletRequest request,
			HttpServletResponse response) {
		PostLikersService postLikersService = new PostLikersService();
		
		String postidAsString = request.getParameter("postid");
		String offsetString = request.getParameter("offset");
		String countString = request.getParameter("count");  
		
		int offset = offsetString!=null ? Integer.parseInt(request.getParameter("offset")) : this.DEFAULT_NEWSFEED_OFFSET;
		int count = countString!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_NEWSFEED_RETURN_COUNT;
		
		if(count > this.MAX_NEWSFEED_RETURN_COUNT){
			count = this.DEFAULT_NEWSFEED_RETURN_COUNT;
		}
		
		int postid = Integer.parseInt(postidAsString);
		Map<String,List<FindFriendTO>> users = new HashMap<String,List<FindFriendTO>>();
		users.put("users", postLikersService.getPostLikers(postid, offset, count,this.uid));
		return users;
	}
}

