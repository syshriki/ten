package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.NewsfeedTO;
import com.nvc.ten.services.NewsfeedService;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidReturnCount;
import com.nvc.ten.throwable.service.NullReturned;

public class NewsfeedServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7462324783751245215L;

	// localhost:8080/ten/newsfeed?offset=<OFFSET>&count=<COUNT>
	// TODO Test blocking and liked
	@SuppressWarnings("unchecked")
	@Override
	protected NewsfeedTO startService(HttpServletRequest request,
            HttpServletResponse response) {
				String offsetString = request.getParameter("offset");
				String countString = request.getParameter("count");  
				
				int offset = offsetString!=null ? Integer.parseInt(request.getParameter("offset")) : this.DEFAULT_NEWSFEED_OFFSET;
				int count = countString!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_NEWSFEED_RETURN_COUNT;
		
				NewsfeedService newsfeedservice = new NewsfeedService();
				NewsfeedTO newsfeedobject = null;
				try {
					newsfeedobject = new NewsfeedTO(newsfeedservice.getNewsfeed(this.uid,offset,count));
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidReturnCount e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvalidID e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (NullReturned e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return newsfeedobject;
	}
}
