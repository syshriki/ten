package com.nvc.ten.servlets;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.UserModel;

public class LogoutServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7462324783751245215L;

	// localhost:8080/ten/logout

	@SuppressWarnings("unchecked")
	@Override
	protected UserModel startService(HttpServletRequest request,
			HttpServletResponse response) {
		return null;
	}
}
