package com.nvc.ten.servlets;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;

public class FollowServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6030930215107809727L;

	// localhost:8080/ten/follow?userid=

	@SuppressWarnings("unchecked")
	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
		FollowService followerservice = new FollowService();
		String useridAsString = request.getParameter("userid");
		try {
			System.out.println("Trying to follow " + useridAsString);
			return followerservice.followUser(this.uid,
					Integer.parseInt(useridAsString));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidID e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InsertFailed e) {
			return 0;
//			e.printStackTrace();
		} catch (SQLException e) {
			return 0;
//			e.printStackTrace();
		}
		return null;
	}
}
