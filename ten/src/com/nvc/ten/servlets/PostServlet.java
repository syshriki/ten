package com.nvc.ten.servlets;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.nvc.ten.models.UserModel;
import com.nvc.ten.services.PostService;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;
import com.nvc.utilities.TenUtilities;

public class PostServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9018201320080293274L;

	@SuppressWarnings("unchecked")
	@Override
	protected Boolean startService(HttpServletRequest request,
			HttpServletResponse response) {
			// Create UserPost service
			PostService userpostservice = new PostService();

			// List of uploaded image filenames
			List<String> filenames = null;

			/*
			 * File upload below must be done in the servlet due to the tight
			 * coupling of fileuploads functionality with the servlet request
			 * and context
			 */

			// Check to see if there we recieved any post data
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (isMultipart) {// If there is an image, do this
				//Remove non alpha numeric chars
				String filebasename = (this.uid + "-" + TenUtilities.getCurrentDatestamp()).replaceAll("[^a-zA-Z0-9]", ""); 
				try {
					ServletContext servletContext = this.getServletConfig()
							.getServletContext();
					File repository = (File) servletContext
							.getAttribute("javax.servlet.context.tempdir");

					// Create a factory for disk-based file items
					DiskFileItemFactory factory = new DiskFileItemFactory();

					// Configure a repository (to ensure a secure temp location
					// is
					// used)
					factory.setRepository(repository);

					// Create a new file upload handler
					ServletFileUpload upload = new ServletFileUpload(factory);
					List<FileItem> items = upload.parseRequest(request);

					filenames = userpostservice.processPostUploadImages(
							filebasename, items, this.uid);
					String messageText = userpostservice.getMessageText();
					// Get hash tags from post
					Set<String> hashtags = userpostservice
							.getHashtags(messageText);
					System.out.println(Arrays.toString(hashtags.toArray()));

					// Add post message to database
					int postid = userpostservice.createPostForUser(
							this.uid, messageText);

					// Add hashtags to post and database
					Set<Integer> hashtagids = userpostservice
							.createHashtags(hashtags); // String set of hashtags
														// converted to ids from
														// db

					// Add hashtag relationships to post
					for (Integer hashid : hashtagids) {
						userpostservice.createUserPostHashTag(postid, hashid);
					}
					// Now we need to add the filenames to the post and db
					for (String filename : filenames) {
						userpostservice.createUserPostImage(filename, postid);
					}
					
					//Lastly notify anyone mentioned in the message text
					userpostservice.createMentions(messageText, this.uid,postid);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				} catch (InsertFailed e) {
					e.printStackTrace();
					return false;
				} catch (InvalidID e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				} catch (InvalidParameter e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				} catch (CharLengthError e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				return true;
			}
		return false;
		/* Done with file upload */
	}

}
