package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.models.transfer.FollowersTO;
import com.nvc.ten.models.transfer.UserTO;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class FollowersServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6030930215107809727L;

	// localhost:8080/ten/followers

	@SuppressWarnings("unchecked")
	@Override
	protected FollowersTO startService(HttpServletRequest request,
			HttpServletResponse response) {
		FollowService followerservice = new FollowService();
		String useridAsString = request.getParameter("userid");
		String offsetString = request.getParameter("offset");
		String countString = request.getParameter("count");  
		
		int offset = offsetString!=null ? Integer.parseInt(request.getParameter("offset")) : 0;
		int count = countString!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_FOLLOWERS_RETURN_COUNT;
		
		if(count > this.MAX_FOLLOWERS_RETURN_COUNT){
			count = this.DEFAULT_FOLLOWERS_RETURN_COUNT;
		}
		
		int userid = Integer.parseInt(useridAsString);
		if(userid>0){
			try {
				System.out.println(this.uid + "is getting followers of " + userid);
				return followerservice.getFollowers(userid,offset,count);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidID e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}

}
