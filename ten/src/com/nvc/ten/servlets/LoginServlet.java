package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.LoginTO;
import com.nvc.ten.services.LoginService;
import com.nvc.ten.throwable.service.NullAuthenticationException;
import com.nvc.ten.throwable.service.NullReturned;
import com.nvc.ten.throwable.service.SaltNotFoundException;
import com.nvc.ten.throwable.servlet.UserAlreadyLoggedInException;

public class LoginServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3127974835735464009L;

	// localhost:8080/ten/login?username=<USERNAME>&password=<PASSWORD>
	//iduser,username,postprivate,description,imagesrc
	protected Object startService(HttpServletRequest request,
			HttpServletResponse response) {
		UserObject u = null;
		String username = request.getParameter("username");
 		String password = request.getParameter("password");
		LoginService loginservice = new LoginService();
		try {
			u = loginservice.login(username, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return 0;
		} catch (NullAuthenticationException e) {
			//e.printStackTrace();
			System.out.println("Bad login");
			return 0;
		} catch (SaltNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return 0;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return 0;
		} catch (NullReturned e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return 0;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return 0;
		}
		if (u != null && u.getIdUser() > 0)
			return new LoginTO(u);
		else
			return 0;
	}
}
