package com.nvc.ten.servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.BestTimeTO;
import com.nvc.ten.services.TopTimeService;

public class BestTimeServlet extends BaseServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3153254845465085571L;

	@Override
	protected Object startService(HttpServletRequest request,
			HttpServletResponse response) {
		TopTimeService topTimeService = new TopTimeService();
		BestTimeTO bestTimeTO = topTimeService.getTopTime(this.uid);
		return bestTimeTO;
	}

}
