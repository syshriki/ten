package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.services.LoginService;
import com.nvc.ten.throwable.service.NullAuthenticationException;
import com.nvc.ten.throwable.service.NullReturned;
import com.nvc.ten.throwable.service.SaltNotFoundException;

public abstract class BaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6907184595758639639L;
	LoginService loginservice = new LoginService();


	protected final String NEWSFEED_OFFSET = "NEWSFEED_OFFSET";
	protected final String NEWSFEED_RETURN_COUNT = "NEWSFEED_RETURN_COUNT";

	// Connection pool max connections
	public static int SALTDB_MAX_CONNECTION_COUNT = 5;
	public static int TENDB_MAX_CONNECTION_COUNT = 10;
	//TODO add these to config file
	public static int MAX_TIMED_RETURN_COUNT = 10;
	public static int MAX_TIMERS_RETURN_COUNT = 9;
	public int MAX_FOLLOWING_RETURN_COUNT = 20;
	public int MAX_FOLLOWERS_RETURN_COUNT = 20;
	public int MAX_PENDING_RETURN_COUNT = 20;
	public int DEFAULT_FOLLOWERS_RETURN_COUNT = 10;
	public int DEFAULT_FOLLOWING_RETURN_COUNT = 10;
	public int DEFAULT_PENDING_RETURN_COUNT = 10;
	protected int DEFAULT_NEWSFEED_OFFSET = 0;
	protected int DEFAULT_NEWSFEED_RETURN_COUNT = 10;
	public static int MAX_NEWSFEED_RETURN_COUNT = 30;
	protected int DEFAULT_FRIEND_SEARCH_OFFSET = 0;
	protected int DEFAULT_FRIEND_SEARCH_RETURN_COUNT = 10;
	public static int MAX_FRIEND_SEARCH_RETURN_COUNT = 20;
	protected int DEFAULT_HASH_SEARCH_OFFSET = 0;
	protected int DEFAULT_HASH_SEARCH_RETURN_COUNT = 10;
	public static int MAX_HASH_SEARCH_RETURN_COUNT = 20;
	protected static String[] IMG_TYPES = new String[] { "image/png",
			"image/jpg", "image/gif", "image/jpeg" };
	public static Set<String> SUPPORTED_IMAGE_TYPES = new HashSet<String>(
			Arrays.asList(IMG_TYPES));
	public static int MAX_IMAGE_WIDTH = 400;
	public static int MAX_IMAGE_HEIGHT = 400;
	public static String PHOTO_STORE_PATH = "C:/dev/Projects/ten/data/photostore/";
	public static int MAX_IMAGE_FILE_SIZE = 10;// MB
	public static String TENDB_USERNAME = "root";
	public static String TENDB_PASSWORD = "login";
	public static String TENDB_CONNECTION_STRING = "jdbc:mysql://localhost:3306/tendb?autoReconnect=true&character-set-client-handshake=FALSE";
	public static String TENSALT_USERNAME = "root";
	public static String TENSALT_PASSWORD = "login";
	public static String TENSALT_CONNECTION_STRING = "jdbc:mysql://localhost:3306/tensalt?autoReconnect=true";

	// USER TABLE CONSTRAINTS
	public static int PASSWORD_MAX_LENGTH = 20;
	public static int USERNAME_MAX_LENGTH = 20;
	public static int DESCRIPTION_MAX_LENGTH = 200;
	public static int EMAIL_MAX_LENGTH = 100;
	public static int WEBSITE_MAX_LENGTH = 100;

	// HASHTAG TABLE CONSTRAINTS
	public static int HASH_MAX_LENGTH = 100;

	// COMMENT TABLE CONSTRAINTS
	public static int COMMENT_MAX_LENGTH = 250;

	// POST TABLE CONSTRAINTS
	public static int POST_MAX_LENGTH = 250;

	// For writing json objects
	private ObjectMapper mapper = new ObjectMapper();
	private Map<String, String> struct = new HashMap<String, String>();

	// User id of current user
	protected int uid;

	public void writeErrToStruct(Exception e) {
		if (e instanceof com.nvc.ten.throwable.UserThrowable) {
			struct.put("error", e.getMessage());
		} else {
			// TODO ERRORS THAT FALL HERE NEED TO BE LOGGED
			e.printStackTrace();
		}
	}

	protected abstract <T> T startService(HttpServletRequest request,
			HttpServletResponse response);

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Writer writer = response.getWriter();
		String json = "Invalid credentials";// default value
		if (this.loginRequired()) {
			String uidAsString = request.getParameter("uid");
			String passwordAsString = request.getParameter("password");
			if (uidAsString != null || passwordAsString != null) {
				if (this.loginservice.isValidLogin(Integer.parseInt(uidAsString),
						passwordAsString)) {
					this.uid = Integer.parseInt(uidAsString);
					json = mapper.writeValueAsString(startService(request,
							response));
				}
			}
		} else {
			json = mapper.writeValueAsString(startService(request, response));
		}
		writer.write(json);
	}

	private boolean loginRequired() {
		String className = this.getClass().getName();
		if (!className.equals(LoginServlet.class.getName())
				&& !className.equals(RegisterServlet.class.getName())
				&& !className.equals(ForgotPasswordServlet.class.getName()))
			return true;
		return false;
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		Writer writer = response.getWriter();
		String json = "Invalid credentials";// default value
		if (this.loginRequired()) {
			String uidAsString = request.getParameter("uid");
			String passwordAsString = request.getParameter("password");
			if (uidAsString != null || passwordAsString != null) {
				if (this.loginservice.isValidLogin(Integer.parseInt(uidAsString),
						passwordAsString)) {
					this.uid = Integer.parseInt(uidAsString);
					json = mapper.writeValueAsString(startService(request,
							response));
				}
			}
		} else {
			json = mapper.writeValueAsString(startService(request, response));
		}
		writer.write(json);
	}


}
