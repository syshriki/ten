package com.nvc.ten.servlets;

import java.io.File;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.nvc.ten.services.ProfileService;

public class ProfileServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5775908848295660828L;

	// localhost:8080/ten/profile?action=view&iduser=<IDUSER>
	// localhost:8080/ten/profile?action=update&username=<USERNAME>&newpassword=<PASSWORD>&description=<DESCRIPTION>&email=<EMAIL>&website=<WEBSITE>&postprivate=<POSTPRIVATE>

	@SuppressWarnings("unchecked")
	@Override
	protected Object startService(HttpServletRequest request,
			HttpServletResponse response) {
		ProfileService profileservice = new ProfileService();
		Object profile = null;
		
		int offset = request.getParameter("offset")!=null ? Integer.parseInt(request.getParameter("offset")) : this.DEFAULT_NEWSFEED_OFFSET;
		int count = request.getParameter("count")!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_NEWSFEED_RETURN_COUNT;
		
		String iduserAsString = request.getParameter("iduser");
		String usernameAsString = request.getParameter("username");
		
		String action = request.getParameter("action");
		try {
			if (action.equals("update")) {
				System.out.println(this.uid+ " trying to update profile");
				ServletContext servletContext = this.getServletConfig()
						.getServletContext();
				File repository = (File) servletContext
						.getAttribute("javax.servlet.context.tempdir");
				DiskFileItemFactory factory = new DiskFileItemFactory();
				factory.setRepository(repository);
				ServletFileUpload upload = new ServletFileUpload(factory);
				List<FileItem> items = upload.parseRequest(request);
				return profileservice.updateProfile(items,this.uid);
			} else if (action.equals("view")) {
				if(iduserAsString!=null)
					profile = profileservice.viewProfileByID(this.uid,Integer.parseInt(iduserAsString), offset, count);
				else if(usernameAsString!=null)
					profile = profileservice.viewProfileByUsername(this.uid,usernameAsString, offset, count);
				else
					profile = profileservice.viewSelfProfile(this.uid,offset, count);
				return profile;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
		return profile;
	}

	

}
