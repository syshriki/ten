package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.NotificationObject;
import com.nvc.ten.models.transfer.NotificationTO;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.services.NotificationService;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class NotificationServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6945111181302570724L;

	// localhost:8080/ten/notifications
	// localhost:8080/ten/notifications?idnotification=<IDNOTIFICATION> - Mark notification read
	// TODO LOGIC ISSUE: Who get notification if repost is liked
	@SuppressWarnings("unchecked")
	@Override
	protected Map<String,List<NotificationTO>> startService(HttpServletRequest request,
			HttpServletResponse response) {
			NotificationService notificationservice = new NotificationService();
			String idNotificationAsString = request
					.getParameter("idnotification");
			String countString = request
					.getParameter("count");
			String offsetString = request
					.getParameter("offset");
			Map<String,List<NotificationTO>> results = new HashMap<String,List<NotificationTO>>();
			if (idNotificationAsString != null) {
				int idNotification = Integer.valueOf(idNotificationAsString);
				try {
					notificationservice.readNotification(idNotification);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null; //TODO
			} else {
				List<NotificationTO> notifications = null;
				try {
					int offset = offsetString!=null ? Integer.parseInt(request.getParameter("offset")) : this.DEFAULT_NEWSFEED_OFFSET;
					int count = countString!=null ? Integer.parseInt(request.getParameter("count")) : this.DEFAULT_NEWSFEED_RETURN_COUNT;
					
					notifications = notificationservice
							.getUnreadNotifications(this.uid,offset,count);
					results.put("notifications", notifications);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				return results;
			}
	}

}
