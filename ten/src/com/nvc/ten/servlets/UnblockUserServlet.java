package com.nvc.ten.servlets;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.BlockService;

public class UnblockUserServlet extends BaseServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 70873665015757077L;

	// localhost:8080/ten/block?userid=<USER_ID>

	@SuppressWarnings("unchecked")
	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
		//Unfollow then add to block list
			String useridAsString = request.getParameter("userid");
			int userid = Integer.parseInt(useridAsString);
			BlockService blockservice = new BlockService();
			try {
				return blockservice.unblockUser(this.uid,userid);
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return 0;
	}

}
