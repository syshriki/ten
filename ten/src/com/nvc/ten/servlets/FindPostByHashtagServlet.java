package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.PostListTO;
import com.nvc.ten.models.transfer.PostTO;
import com.nvc.ten.services.FindPostByHashService;

public class FindPostByHashtagServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 890005984991023011L;

	// localhost:8080/ten/hashsearch?hash=has
	// WILL NOT return posts of users whom are private
	@SuppressWarnings("unchecked")
	@Override
	protected Object startService(HttpServletRequest request,
			HttpServletResponse response) {
		FindPostByHashService findPostByHashService = new FindPostByHashService();
		String hashtag = request.getParameter("hash");
		try {
			return new PostListTO(findPostByHashService.findPostByHash(hashtag,
					DEFAULT_HASH_SEARCH_OFFSET,
					DEFAULT_HASH_SEARCH_RETURN_COUNT));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
