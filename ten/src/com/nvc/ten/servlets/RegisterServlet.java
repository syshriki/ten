package com.nvc.ten.servlets;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.nvc.ten.models.UserModel;
import com.nvc.ten.services.RegisterService;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.RegistrationErrorException;
import com.nvc.ten.throwable.service.UserAlreadyExistsException;
import com.nvc.utilities.JsonKeyValue;
import com.nvc.utilities.TenUtilities;

public class RegisterServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3860316248308140801L;

	@SuppressWarnings("unchecked")
	@Override
	protected Object startService(HttpServletRequest request,
			HttpServletResponse response) {
		RegisterService registerServices = new RegisterService();
		try {
			ServletContext servletContext = this.getServletConfig()
					.getServletContext();
			File repository = (File) servletContext
					.getAttribute("javax.servlet.context.tempdir");
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setRepository(repository);
			ServletFileUpload upload = new ServletFileUpload(factory);
			List<FileItem> items;
			items = upload.parseRequest(request);
			return registerServices.proccessRegisterRequest(items);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidParameter e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RegistrationErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UserAlreadyExistsException e) {
			e.printStackTrace();
			return new JsonKeyValue<String>("msg","Username or email is already taken.");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

}
