package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.models.HashtagModel;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.transfer.FindHashtagTO;
import com.nvc.ten.services.FindFriendService;
import com.nvc.ten.services.FindHashService;
import com.nvc.ten.throwable.service.InvalidReturnCount;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class FindHashtagServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 890005984991023011L;

	// localhost:8080/ten/findhash?search=has
	@Override
	protected FindHashtagTO startService(HttpServletRequest request,
			HttpServletResponse response) {
		FindHashService findHashService = new FindHashService();
		String hashtag = request.getParameter("search");
		FindHashtagTO findHashtagTo = new FindHashtagTO();
		try {
			findHashtagTo.setHashtags(findHashService.findHashtag(hashtag,
					DEFAULT_HASH_SEARCH_OFFSET,
					DEFAULT_HASH_SEARCH_RETURN_COUNT)); 
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidReturnCount e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return findHashtagTo;
	}

}