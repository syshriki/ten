package com.nvc.ten.servlets;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.PopularService;

public class PopularServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3530316925272308311L;
	// localhost:8080/ten/popular?scope=global&type=timers
	// localhost:8080/ten/popular?scope=local&type=timers
	// localhost:8080/ten/popular?scope=global&type=timed
	// localhost:8080/ten/popular?scope=local&type=timed
	@SuppressWarnings("unchecked")
	@Override
	protected Object startService(HttpServletRequest request,
			HttpServletResponse response) {
		PopularService popularservice = new PopularService();
			String viewscope = request.getParameter("scope");
			String viewtype = request.getParameter("type");
			try {
				return popularservice.getPopular(this.uid, viewtype, viewscope);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return 0;
		
	}
}
