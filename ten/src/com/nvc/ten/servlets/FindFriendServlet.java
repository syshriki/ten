package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.context.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.models.transfer.FindFriendsTO;
import com.nvc.ten.services.FindFriendService;
import com.nvc.ten.services.LoginService;
import com.nvc.ten.services.NewsfeedService;
import com.nvc.ten.throwable.service.InvalidReturnCount;
import com.nvc.ten.throwable.service.NullAuthenticationException;
import com.nvc.ten.throwable.service.SaltNotFoundException;
import com.nvc.ten.throwable.servlet.UserAlreadyLoggedInException;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class FindFriendServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2979463453181170206L;

	// localhost:8080/ten/friendsearch?username=ja
	@SuppressWarnings("unchecked")
	@Override
	protected FindFriendsTO startService(HttpServletRequest request,
			HttpServletResponse response) {
		String username = request.getParameter("username");
		FindFriendService friendSearchService = new FindFriendService();
		try {
			return friendSearchService.searchForFriend(username,
					DEFAULT_FRIEND_SEARCH_OFFSET,
					DEFAULT_FRIEND_SEARCH_RETURN_COUNT, this.uid);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidReturnCount e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
