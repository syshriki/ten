package com.nvc.ten.servlets;

import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.NullReturned;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class UnfollowServlet extends BaseServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6030930215107809727L;
	// localhost:8080/ten/unfollow?userid=<USERID>

	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
		FollowService followerservice = new FollowService();
			String useridAsString = request.getParameter("userid");
			int userid = Integer.parseInt(useridAsString);
			try {
				System.out.println(this.uid + " unfollowing " + userid);
				followerservice.unfollowUser(userid, this.uid);
				return 1;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidID e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullReturned e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return 0;
	}
}
