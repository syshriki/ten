package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.models.transfer.NotificationTO;
import com.nvc.ten.models.transfer.StatusTO;
import com.nvc.ten.services.BlockService;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.services.StatusService;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.NullReturned;

public class StatusServlet extends BaseServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 70873665015757077L;

	// localhost:8080/ten/status
	// Should return all active posts which are liked by the user
	
	@Override
	protected Map<String,List<StatusTO>> startService(HttpServletRequest request,
			HttpServletResponse response) {
		
			StatusService statusservice = new StatusService();
			Map<String,List<StatusTO>> results = new HashMap<String,List<StatusTO>>();
			try {
				List<StatusTO> statuses = statusservice.getStatuses(this.uid);
				results.put("notifications", statuses);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return results;
	}
}
