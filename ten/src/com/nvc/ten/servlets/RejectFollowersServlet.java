package com.nvc.ten.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.nvc.ten.services.CommentService;
import com.nvc.ten.services.FollowService;
import com.nvc.ten.services.LikeService;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.NotFollowingPrivateException;
import com.nvc.ten.throwable.servlet.UserNotLoggedInException;

public class RejectFollowersServlet extends BaseServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3542109560430333161L;

	// localhost:8080/ten/like?postid=<ID>

	@SuppressWarnings("unchecked")
	@Override
	protected Integer startService(HttpServletRequest request,
			HttpServletResponse response) {
		FollowService followservice = new FollowService();
		String useridAsString = request.getParameter("userid");

		int requesteeId = Integer.parseInt(useridAsString);
		int recipientId = this.uid;
		try {
			return followservice.rejectFollower(recipientId, requesteeId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidID e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
