package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.PostHashtagModel;

public class UserPostHashtagDAO implements DAOinterface<PostHashtagModel> {
	
	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO userposthashtags (iduserpost,idhashtag,datestamp) VALUES (?,?,?)";
    /** The query for read. */
    private static final String FINDBYID_QUERY = "SELECT * FROM userposthashtags WHERE iduserposthashtags = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE userposthashtags SET iduserpost=?,idhashtag=?,datestamp=? WHERE iduserposthashtags = ?";
    /** The query for delete. */
    private static final String DELETEBYID_QUERY = "DELETE FROM userposthashtags WHERE iduserposthashtags = ?";
    
    private static final String FINDBYPOSTID_QUERY = "SELECT * FROM userposthashtags WHERE iduserpost = ?";
	@Override
	public int insert(PostHashtagModel insertData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, insertData.getUserPostid());
			preparedStatement.setInt(2, insertData.getHashtagid());
			preparedStatement.setString(3, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(PostHashtagModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, updateData.getUserPostid());
			preparedStatement.setInt(2, updateData.getHashtagid());
			preparedStatement.setString(3, updateData.getDatestamp().toString());
			preparedStatement.setInt(4, updateData.getUserPostHashtagid());
			
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public PostHashtagModel findByID(int id) throws ClassNotFoundException, SQLException {
		PostHashtagModel userposthashtag = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userposthashtag = new PostHashtagModel();
				userposthashtag.setUserPostHashtagid(result.getInt(1));
				userposthashtag.setHashtagid(result.getInt(2));
				userposthashtag.setDatestamp(result.getTimestamp(3));
				userposthashtag.setHashtagid(result.getInt(4));
			} else {
				// TODO
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

		return userposthashtag;
	}
	
	public Set<PostHashtagModel> findByPostID(int id) throws ClassNotFoundException, SQLException {
		PostHashtagModel userposthashtag = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<PostHashtagModel> hashes = new HashSet<>();
		
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYPOSTID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while(result.next() && result != null) {
				userposthashtag = new PostHashtagModel();
				userposthashtag.setUserPostHashtagid(result.getInt(1));
				userposthashtag.setHashtagid(result.getInt(2));
				userposthashtag.setDatestamp(result.getTimestamp(3));
				userposthashtag.setHashtagid(result.getInt(4));
				hashes.add(userposthashtag);
			}
			return hashes;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

}
