package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.UserModel;

public class UserDAO implements DAOinterface<UserModel> {

	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO user (username,password,description,datestamp,email,imagesrc,website,isprivate) VALUES (?,?,?,?,?,?,?,?)";
	/** The query for read by ID. */
	private static final String FINDBYID_QUERY = "SELECT * FROM user WHERE iduser = ?";
	/** The query for read by username. */
	private static final String FINDBYUSERNAME_QUERY = "SELECT * FROM user WHERE username = ?";
	/** The query for read by username. */
	private static final String FINDBYEMAIL_QUERY = "SELECT * FROM user WHERE email = ?";
	/** The query for update. */
	private static final String UPDATE_QUERY = "UPDATE user SET username=? , password=? , description=? , datestamp=?, email=?, imagesrc=?, website=?, isprivate=? WHERE iduser=?";
	/** The query for delete. */
	private static final String DELETEBYID_QUERY = "DELETE FROM user WHERE iduser = ?";
	/** The query for read by username. */
	private static final String FINDBYUSERNAMELIMITED_QUERY = "SELECT * FROM user WHERE username LIKE ? AND iduser != ? LIMIT ?,?";

	@Override
	public int insert(UserModel insertData) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();

		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, insertData.getUsername());
			preparedStatement.setBytes(2, insertData.getPassword());
			preparedStatement.setString(3, insertData.getDescription());
			preparedStatement
					.setString(4, insertData.getDatestamp().toString());
			preparedStatement.setString(5, insertData.getEmail());
			preparedStatement.setString(6, insertData.getImagesrc());
			preparedStatement.setString(7, insertData.getWebsite());
			preparedStatement.setBoolean(8, insertData.isPrivate());

			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();
			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(UserModel updateData) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setString(1, updateData.getUsername());
			preparedStatement.setBytes(2, updateData.getPassword());
			preparedStatement.setString(3, updateData.getDescription());
			preparedStatement.setTimestamp(4, updateData.getDatestamp());
			preparedStatement.setString(5, updateData.getEmail());
			preparedStatement.setString(6, updateData.getImagesrc());
			preparedStatement.setString(7, updateData.getWebsite());
			preparedStatement.setBoolean(8, updateData.isPrivate());
			preparedStatement.setInt(9, updateData.getIdUser());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public UserModel findByID(int id) throws SQLException, ClassNotFoundException {
		UserModel userDTO = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userDTO.setWebsite(result.getString(8));
				userDTO.setPrivate(result.getInt(9) == 1 ? true : false);
			} else {
				// TODO
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

		return userDTO;
	}

	public UserModel findByUsername(String username)
			throws ClassNotFoundException, SQLException {
		UserModel userDTO = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYUSERNAME_QUERY);
			preparedStatement.setString(1, username);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userDTO.setWebsite(result.getString(8));
				userDTO.setPrivate(result.getInt(9) == 1 ? true : false);
			} else {
				// TODO
			}
			return userDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

	public List<UserModel> findByUsernameLimit(String username, int offset,
			int count,int iduser) throws ClassNotFoundException, SQLException {
		UserModel userDTO = null;
		List<UserModel> userList = new ArrayList<UserModel>();
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn
					.prepareStatement(FINDBYUSERNAMELIMITED_QUERY);
			preparedStatement.setString(1, username);
			preparedStatement.setInt(2, iduser);
			preparedStatement.setInt(3, offset);
			preparedStatement.setInt(4, offset+count);

			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userDTO.setWebsite(result.getString(8));
				userDTO.setPrivate(result.getInt(9) == 1 ? true : false);
				userList.add(userDTO);
			}
			return userList;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

	public UserModel findByEmail(String Email) throws SQLException,
			ClassNotFoundException {
		UserModel userDTO = null;
		Connection conn = null;
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYEMAIL_QUERY);
			preparedStatement.setString(1, Email);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userDTO.setWebsite(result.getString(8));
				userDTO.setPrivate(result.getInt(9) == 1 ? true : false);
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

		return userDTO;
	}

}
