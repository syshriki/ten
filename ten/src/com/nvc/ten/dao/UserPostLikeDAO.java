package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.LikeModel;

public class UserPostLikeDAO implements DAOinterface<LikeModel> {

	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO userpostlikes (iduserpost,iduser,datestamp) VALUES (?,?,?)";
	/** The query for read. */
	private static final String FINDBYID_QUERY = "SELECT * FROM userpostlikes WHERE iduserpostlikes = ?";
	/** The query for update. */
	private static final String UPDATE_QUERY = "UPDATE userpostlikes SET iduserpost=? , iduser=? , datestamp=?  WHERE iduserpostlikes = ?";
	/** The query for delete. */
	private static final String DELETEBYID_QUERY = "DELETE FROM userpostlikes WHERE iduserpostlikes = ?";
	
	private static final String FINDBYPOSTID_QUERY = "SELECT * FROM userpostlikes WHERE iduserpost = ?";
	
	private static final String FINDBYPOSTUSER_QUERY = "SELECT * FROM userpostlikes WHERE iduserpost = ? AND iduser = ?";


	@Override
	public int insert(LikeModel insertData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, insertData.getIdUserPost());
			preparedStatement.setInt(2, insertData.getIdUser());
			preparedStatement
					.setString(3, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(LikeModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, updateData.getIdUserPost());
			preparedStatement.setInt(2, updateData.getIdUser());
			preparedStatement.setTimestamp(3, updateData.getDatestamp());
			preparedStatement.setInt(4, updateData.getIdUserPostLike());

			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public LikeModel findByID(int id) throws ClassNotFoundException, SQLException {
		LikeModel userPostLike = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userPostLike = new LikeModel();
				userPostLike.setIdUserPostLike(result.getInt(1));
				userPostLike.setIdUser(result.getInt(2));
				userPostLike.setIdUserPost(result.getInt(3));
				userPostLike.setDatestamp(result.getTimestamp(4));
			}
			return userPostLike;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public Set<LikeModel> findByPostID(int id) throws ClassNotFoundException, SQLException {
		LikeModel userPostLike = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<LikeModel> likes = new HashSet<>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYPOSTID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next() && result != null) {
				userPostLike = new LikeModel();
				userPostLike.setIdUserPostLike(result.getInt(1));
				userPostLike.setIdUser(result.getInt(2));
				userPostLike.setIdUserPost(result.getInt(3));
				userPostLike.setDatestamp(result.getTimestamp(4));
				likes.add(userPostLike);
			}
			return likes;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	public LikeModel findByPostUser(int postid, int userid) throws ClassNotFoundException, SQLException {
		LikeModel userPostLike = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYPOSTUSER_QUERY);
			preparedStatement.setInt(1, postid);
			preparedStatement.setInt(2, userid);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if(result.next() && result != null) {
				userPostLike = new LikeModel();
				userPostLike.setIdUserPostLike(result.getInt(1));
				userPostLike.setIdUser(result.getInt(2));
				userPostLike.setIdUserPost(result.getInt(3));
				userPostLike.setDatestamp(result.getTimestamp(4));
			}
			return userPostLike;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
}
