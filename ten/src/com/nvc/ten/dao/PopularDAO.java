package com.nvc.ten.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.PopularTimedObject;
import com.nvc.ten.models.extended.PopularTimerObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.PostObject;

public class PopularDAO {

	/** Get feed given user **/
	private static final String LOCAL_TOPTIMED_QUERY = "SELECT userpost.*,upi.*,COUNT(distinct iduserpostlikes) as timeweight,u.* FROM userpost LEFT JOIN user as u ON u.iduser = userpost.iduser LEFT JOIN userpostimage as upi ON userpost.iduserpost = upi.iduserpost LEFT JOIN userpostlikes as likecount ON likecount.iduserpost = userpost.iduserpost LEFT JOIN userpostimage ON userpost.iduserpost = userpost.iduserpost WHERE userpost.expiration >= NOW() AND (userpost.iduser IN ( SELECT idrecipient from followers WHERE idrequestee = ?  AND followers.approved = 1) OR userpost.iduser = ?) GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT ?,?";

	private static final String LOCAL_TOPTIMERS_QUERY = "SELECT user.*,toplikers.likes FROM(SELECT userpostlikes.*,COUNT(*) as likes FROM userpostlikes WHERE userpostlikes.iduser IN (SELECT followers.idrecipient FROM followers WHERE followers.idrequestee = ? AND followers.approved = 1) GROUP BY userpostlikes.iduser ORDER BY likes DESC LIMIT ?,?) toplikers LEFT JOIN user ON user.iduser = toplikers.iduser";

	private static final String GLOBAL_TOPTIMED_QUERY = "SELECT u.*,userpost.*,userpostimage.*,COUNT(distinct iduserpostlikes) as timeweight FROM userpost LEFT JOIN user as u ON u.iduser = userpost.iduser LEFT JOIN userpostimage ON userpost.iduserpost = userpostimage.iduserpost LEFT JOIN userpostlikes as likecount ON likecount.iduserpost = userpost.iduserpost WHERE userpost.iduser IN ( SELECT iduser from user WHERE user.postsprivate = 0 AND userpost.expiration >= NOW()) GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT ?,?";

	private static final String GLOBAL_TOPTIMERS_QUERY = "Select user.*,toplikers.likes FROM (SELECT userpostlikes.iduser,COUNT(*) as likes FROM userpostlikes GROUP BY userpostlikes.iduser ORDER BY likes DESC LIMIT ?,?) toplikers LEFT JOIN user ON user.iduser = toplikers.iduser";

	public List<PopularTimedObject> getPopularLocalTopTimed(int id,int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		List<PopularTimedObject> popularObjects = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(LOCAL_TOPTIMED_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, id);
			preparedStatement.setInt(3, offset);
			preparedStatement.setInt(4, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			while (result.next()) {
				PopularTimedObject popularobject = new PopularTimedObject();
				
				PostObject userPostObject = new PostObject();
				userPostObject.setIdUserPost(result.getInt(1));
				userPostObject.setText(result.getString(2));
				userPostObject.setDatestamp(result.getTimestamp(3));
				userPostObject.setIdUser(result.getInt(4));
				userPostObject.setIdUserPostRepost(result.getInt(5));
				userPostObject.setExpiration(result.getTimestamp(6));
				//If Post has images
				if(result.getInt(7)>0){
					ImageObject userPostImageDTO = new ImageObject();
					userPostImageDTO.setIdUserPostImage(result.getInt(7));
					userPostImageDTO.setImageSrc(result.getString(8));
					userPostImageDTO.setIdUserPost(result.getInt(9));
					userPostImageDTO.setDatestamp(result.getTimestamp(10));
					userPostObject.setImages(new HashSet<ImageObject>(
							Arrays.asList(userPostImageDTO)));
					if(!userPostImageDTO.equals(null))
						userPostObject.loadImageData();
				}
				
				int points = result.getInt(11);
				
				//User who liked
				if(result.getInt(12)>0){
					UserModel user = new UserModel();
					user.setIdUser(result.getInt(12));
					user.setUsername(result.getString(13));
					user.setPassword(result.getBytes(14));
					user.setDatestamp(result.getTimestamp(15));
					user.setDescription(result.getString(16));
					user.setEmail(result.getString(17));
					user.setImagesrc(result.getString(18));
					user.setWebsite(result.getString(19));
					user.setPrivate(result.getBoolean(20));
					UserObject uo = new UserObject(user);
					userPostObject.setUser(uo);
				}
				
				popularobject.setUserpost(userPostObject);
				popularobject.setScore(points);
				
				popularObjects.add(popularobject);
			}

		return popularObjects;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	public List<PopularTimerObject> getPopularLocalTopTimers(int id,int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		List<PopularTimerObject> popularObjects = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(LOCAL_TOPTIMERS_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			while (result.next()) {
				PopularTimerObject popularobject = new PopularTimerObject();
				UserModel user = new UserModel();
				user.setIdUser(result.getInt(1));
				user.setUsername(result.getString(2));
				user.setPassword(result.getBytes(3));
				user.setDatestamp(result.getTimestamp(4));
				user.setDescription(result.getString(5));
				user.setEmail(result.getString(6));
				user.setImagesrc(result.getString(7));
				user.setWebsite(result.getString(8));
				user.setPrivate(result.getBoolean(9));
				
				int score = result.getInt(10);
				
				popularobject.setScore(score);
				popularobject.setUser(new UserObject(user));
				popularObjects.add(popularobject);
			}

		return popularObjects;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	public List<PopularTimedObject> getPopularGlobalTopTimed(int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		List<PopularTimedObject> popularObjects = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(GLOBAL_TOPTIMED_QUERY);
			preparedStatement.setInt(1, offset);
			preparedStatement.setInt(2, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			while (result.next()) {
				PopularTimedObject popularobject = new PopularTimedObject();
				PostObject userPostObject = new PostObject();
				
				//User who liked
				if(result.getInt(1)>0){
					UserModel user = new UserModel();
					user.setIdUser(result.getInt(1));
					user.setUsername(result.getString(2));
					user.setPassword(result.getBytes(3));
					user.setDatestamp(result.getTimestamp(4));
					user.setDescription(result.getString(5));
					user.setEmail(result.getString(6));
					user.setImagesrc(result.getString(7));
					user.setWebsite(result.getString(8));
					user.setPrivate(result.getBoolean(9));
					UserObject uo = new UserObject(user);
					userPostObject.setUser(uo);
				}
				
				if(result.getInt(10)>0){
					userPostObject.setIdUserPost(result.getInt(10));
					userPostObject.setText(result.getString(11));
					userPostObject.setDatestamp(result.getTimestamp(12));
					userPostObject.setIdUser(result.getInt(13));
					userPostObject.setIdUserPostRepost(result.getInt(14));
					userPostObject.setExpiration(result.getTimestamp(15));
				}
				
				if(result.getInt(16)>0){
					ImageObject userPostImageDTO = new ImageObject();
					userPostImageDTO.setIdUserPostImage(result.getInt(16));
					userPostImageDTO.setImageSrc(result.getString(17));
					userPostImageDTO.setIdUserPost(result.getInt(18));
					userPostImageDTO.setDatestamp(result.getTimestamp(19));
					userPostObject.setImages(new HashSet<ImageObject>(
							Arrays.asList(userPostImageDTO)));
					userPostObject.loadImageData();
				}
				
				int points = result.getInt(20);
				
				popularobject.setUserpost(userPostObject);
				popularobject.setScore(points);
				
				popularObjects.add(popularobject);
			}

		return popularObjects;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

	public List<PopularTimerObject> getPopularGlobalTopTimers(int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		List<PopularTimerObject> popularObjects = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(GLOBAL_TOPTIMERS_QUERY);
			preparedStatement.setInt(1, offset);
			preparedStatement.setInt(2, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			while (result.next()) {
				PopularTimerObject popularobject = new PopularTimerObject();
				UserModel user = new UserModel();
				user.setIdUser(result.getInt(1));
				user.setUsername(result.getString(2));
				user.setPassword(result.getBytes(3));
				user.setDatestamp(result.getTimestamp(4));
				user.setDescription(result.getString(5));
				user.setEmail(result.getString(6));
				user.setImagesrc(result.getString(7));
				user.setWebsite(result.getString(8));
				user.setPrivate(result.getBoolean(9));
				
				int score = result.getInt(10);
				
				popularobject.setScore(score);
				popularobject.setUser(new UserObject(user));
				popularObjects.add(popularobject);
			}

		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
		return popularObjects;
	}

}
