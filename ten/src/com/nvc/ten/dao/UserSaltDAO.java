package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.nvc.ten.factories.SaltDBConnectionFactory;
import com.nvc.ten.models.UserSalt;

public class UserSaltDAO implements DAOinterface<UserSalt> {

	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO salt (iduser,salt,datestamp) VALUES (?,?,?)";
	/** The query for read. */
	private static final String FINDBYUID_QUERY = "SELECT * FROM salt WHERE iduser = ?";
	/** The query for update. */
	private static final String UPDATE_QUERY = "UPDATE salt SET salt = ?, datestamp = ? WHERE iduser = ?";
	/** The query for delete. */
	private static final String DELETEBYUID_QUERY = "DELETE FROM salt WHERE iduser = ?";

	@Override
	public int insert(UserSalt insertData) throws ClassNotFoundException,
			SQLException {
		Connection conn = null;
		SaltDBConnectionFactory dbfactory = new SaltDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, insertData.getIduser());
			preparedStatement.setBytes(2, insertData.getSalt());
			preparedStatement.setString(3, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();
			
			if (result.next() && result != null) 
				return result.getInt(1);
			return -1;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(UserSalt updateData)
			throws ClassNotFoundException, SQLException {
		Connection conn = null;
		SaltDBConnectionFactory dbfactory = new SaltDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setBytes(1, updateData.getSalt());
			preparedStatement
					.setString(2, updateData.getDatestamp().toString());
			preparedStatement.setInt(3, updateData.getIduser());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		SaltDBConnectionFactory dbfactory = new SaltDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYUID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public UserSalt findByID(int id) throws ClassNotFoundException, SQLException {
		UserSalt userSaltDTO = null;
		Connection conn = null;
		SaltDBConnectionFactory dbfactory = new SaltDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYUID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userSaltDTO = new UserSalt();
				userSaltDTO.setIduser(result.getInt(1));
				userSaltDTO.setSalt(result.getBytes(2));
				userSaltDTO.setDatestamp(result.getTimestamp(3));
			}
			return userSaltDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

}
