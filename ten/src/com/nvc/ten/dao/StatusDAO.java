package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.extended.StatusObject;

public class StatusDAO {
    private static final String GETSTATUS_BY_USERID  = "SELECT s.*,(SELECT COUNT(*) from userpostlikes WHERE iduserpost=s.idpost) as likes FROM (SELECT DISTINCT up.iduserpost as idpost,n.datestamp,u.iduser,n.notificationtype,n.idunreadnotifications,n.affectinguser FROM unreadnotifications AS n LEFT JOIN userpost AS up ON n.idpost = up.iduserpost LEFT JOIN user AS u ON up.iduser = u.iduser WHERE n.idpost IN (SELECT iduserpost FROM userpostlikes AS upl WHERE upl.iduser = ? AND up.expiration >= NOW()) ORDER BY n.datestamp DESC) AS s GROUP BY(s.idpost)";
    public List<StatusObject> getStatusByUserId(int userid) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		List<StatusObject> statuses = new ArrayList<>();
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(GETSTATUS_BY_USERID);
			preparedStatement.setInt(1, userid);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			while(result.next()){
				StatusObject status = new StatusObject();
				status.setIdpost(result.getInt(1));
				status.setDatestamp(result.getTimestamp(2));
				status.setIduser(result.getInt(3));
				status.setNotificationType(result.getString(4));
				status.setNotificationId(result.getInt(5));
				status.setAffectingUser(result.getInt(6));
				status.setLikecount(result.getInt(7));
				statuses.add(status);
			}
			return statuses;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
}
