package com.nvc.ten.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.NotificationObject;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.utilities.TenUtilities;

public class UnreadNotificationDAO{
	
    /** The query for read. */
    private static final String FINDUNREADERNOTIFICATIONS_QUERY = "SELECT n.idunreadnotifications,n.notificationtype,n.affectinguser,n.idpost,n.datestamp,(SELECT COUNT(*) FROM userpostlikes WHERE iduserpost=n.idpost) AS likes,n.isread FROM unreadnotifications AS n LEFT JOIN userpost AS up ON n.idpost=up.iduserpost WHERE n.affecteduser = ? AND (ISNULL(n.idpost) OR up.expiration > NOW()) ORDER BY n.datestamp DESC LIMIT ?,?";
    private static final String COUNTUNREADNOTIFICATIONS_QUERY = "SELECT COUNT(*) AS count FROM unreadnotifications AS n LEFT JOIN userpost AS up ON n.idpost=up.iduserpost WHERE n.affecteduser = ? AND (ISNULL(n.idpost) OR up.expiration > NOW()) AND n.isread = 0 ORDER BY n.datestamp DESC";
    private static final String READNOTIFICATION_QUERY = "UPDATE unreadnotifications SET isread = 1  WHERE idunreadnotifications = ?";
    private static final String DELETEBYPOSTID  = "DELETE FROM unreadnotifications WHERE idpost = ?";
    private static final String INSERTMENTION  = "CALL insertUnreadMentionNotification(?,?,?)";
    
	public List<NotificationObject> findUnreadNotifications(int iduser,int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		List<NotificationObject> unreadnotificationobjects = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDUNREADERNOTIFICATIONS_QUERY,Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1,iduser);
			preparedStatement.setInt(2,offset);
			preparedStatement.setInt(3,count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			
			while (result.next()) {
				NotificationObject unreadNotificationObject = new NotificationObject();
				int idUnreadNotifications = result.getInt(1);
				String notificationType = result.getString(2);
				int idUser = result.getInt(3);
				int idPost = result.getInt(4);
				Timestamp timestamp = result.getTimestamp(5);
				int likes = result.getInt(6);
				int isread = result.getInt(7);
				
				unreadNotificationObject.setDatestamp(timestamp);
				unreadNotificationObject.setRead(isread==1);
				unreadNotificationObject.setIdpost(idPost);
				unreadNotificationObject.setIduser(idUser);
				unreadNotificationObject.setLikecount(likes);
				unreadNotificationObject.setNotificationType(notificationType);
				unreadNotificationObject.setIdUnreadNotification(idUnreadNotifications);
				unreadnotificationobjects.add(unreadNotificationObject);
			}
			return unreadnotificationobjects;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	public boolean readNotification(int idunreadnotification) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(READNOTIFICATION_QUERY);
			preparedStatement.setInt(1, idunreadnotification);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
	
	public Integer countUnreadNotifications(int iduser) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(COUNTUNREADNOTIFICATIONS_QUERY);
			preparedStatement.setInt(1, iduser);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			
			int count = 0;
			if (result.next() && result != null) 
				count = result.getInt(1);
			
			return count;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYPOSTID);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
	
	public boolean insertMentionsNotification(int mentionerId,int mentionedId,int userpostId) throws ClassNotFoundException, SQLException{
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERTMENTION);
			preparedStatement.setInt(1, mentionerId);
			preparedStatement.setInt(2, mentionedId);
			preparedStatement.setInt(3, userpostId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
}
