package com.nvc.ten.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.PostHashtagModel;
import com.nvc.ten.models.PostImageModel;
import com.nvc.ten.models.extended.NewsfeedObject;
import com.nvc.ten.models.extended.CommentObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.HashtagObject;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.LikeObject;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.utilities.TenUtilities;

public class NewsfeedObjectDAO {
	
    /** Get feed given user **/
    private static final String NEWSFEED_QUERY = 
    	"SELECT * FROM ( "
    		+"SELECT DISTINCT * FROM userpost " 
				+"WHERE ( "
					+"(userpost.expiration >= NOW() " 
						+"AND NOT ( "
							+"userpost.iduserpostrepost > 0 AND userpost.iduser = ? "
						+")"
					+") "
					+"OR (userpost.iduserpostrepost > 0 " 
						+"OR ("
							+"SELECT reposted.expiration FROM userpost AS reposted WHERE reposted.iduserpost = userpost.iduserpostrepost "
						+") >= NOW() "
						+"AND userpost.iduser != ? "
					+") "
				+") "
				+"AND (userpost.iduser = ? OR EXISTS( "
						+"SELECT * FROM followers WHERE followers.idrequestee = ? AND followers.idrecipient = userpost.iduser AND followers.approved = 1"
					+") "
				+") ORDER BY userpost.datestamp DESC LIMIT ?,? "
			+") AS userpost " 
    		+"LEFT JOIN userpostlikes ON userpost.iduserpost = userpostlikes.iduserpost " 
    		+"LEFT JOIN user AS likeuser ON likeuser.iduser = userpostlikes.iduser "
    		+"LEFT JOIN usercomment ON userpost.iduserpost  = usercomment.iduserpost " 
    		+"LEFT JOIN user AS commentuser ON commentuser.iduser = usercomment.iduser "
    		+"LEFT JOIN userpostimage ON userpost.iduserpost  = userpostimage.iduserpost " 
    		+"LEFT JOIN userposthashtags ON userpost.iduserpost  = userposthashtags.iduserpost " 
    		+"LEFT JOIN user AS post_user ON post_user.iduser = userpost.iduser "
    		+"ORDER BY userpost.datestamp DESC";
    
	public NewsfeedObject getNewsFeed(int id,int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		NewsfeedObject newsfeed = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenUtilities utils = new TenUtilities();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(NEWSFEED_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, id);
			preparedStatement.setInt(3, id);
			preparedStatement.setInt(4, id);
			preparedStatement.setInt(5, offset);
			preparedStatement.setInt(6, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			newsfeed = utils.proccessPostListResultSet(result,id);
			return newsfeed;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

}
