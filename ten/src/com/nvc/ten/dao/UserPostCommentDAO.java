package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.CommentModel;

public class UserPostCommentDAO implements DAOinterface<CommentModel> {
	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO usercomment (text,iduserpost,iduser,datestamp) VALUES (?,?,?,?)";
    /** The query for read. */
    private static final String FINDBYID_QUERY = "SELECT * FROM usercomment WHERE idusercomment = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE usercomment SET iduser=? , iduserpost=? , text=? , datestamp=? WHERE idusercomment = ?";
    /** The query for delete. */
    private static final String DELETEBYID_QUERY = "DELETE FROM usercomment WHERE idusercomment = ?";
    /** The query for read. */
    private static final String FINDBYPOSTID_QUERY = "SELECT * FROM usercomment WHERE iduserpost = ?";
    
	@Override
	public int insert(CommentModel insertData) {//throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, insertData.getComment());
			preparedStatement.setInt(2, insertData.getIdUserPost());
			preparedStatement.setInt(3, insertData.getIdUser());
			preparedStatement.setString(4, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} catch(Exception e){
			e.getMessage();
			return -1;
		}finally {
			dbfactory.returnConnection(conn);
			try {
				result.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean update(CommentModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, updateData.getIdUser());
			preparedStatement.setInt(2, updateData.getIdUserPost());
			preparedStatement.setString(3, updateData.getComment());
			preparedStatement.setString(4, updateData.getDatestamp().toString());
			preparedStatement.setInt(5, updateData.getIdUserComment());

			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public CommentModel findByID(int id) throws ClassNotFoundException, SQLException {
		CommentModel userPostCommentDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userPostCommentDTO = new CommentModel();
				userPostCommentDTO.setIdUserComment(result.getInt(1));
				userPostCommentDTO.setIdUser(result.getInt(2));
				userPostCommentDTO.setIdUserPost(result.getInt(3));
				userPostCommentDTO.setDatestamp(result.getTimestamp(4));
				userPostCommentDTO.setComment(result.getString(5));
			}
			return userPostCommentDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	public Set<CommentModel> findByPostID(int id) throws ClassNotFoundException, SQLException {
		CommentModel userPostCommentDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<CommentModel> comments = new HashSet<>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYPOSTID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next() && result != null) {
				userPostCommentDTO = new CommentModel();
				userPostCommentDTO.setIdUserComment(result.getInt(1));
				userPostCommentDTO.setIdUser(result.getInt(2));
				userPostCommentDTO.setIdUserPost(result.getInt(3));
				userPostCommentDTO.setDatestamp(result.getTimestamp(4));
				userPostCommentDTO.setComment(result.getString(5));
				comments.add(userPostCommentDTO);
			}
			return comments;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
}
