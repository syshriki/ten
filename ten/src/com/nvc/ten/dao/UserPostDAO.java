package com.nvc.ten.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.PostImageModel;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.utilities.TenUtilities;

public class UserPostDAO implements DAOinterface<PostModel> {
	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO userpost (text,datestamp,iduser,iduserpostrepost,expiration) VALUES (?,?,?,?,?)";
    /** The query for read. */
    private static final String FINDBYID_QUERY = "SELECT * FROM userpost WHERE iduserpost = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE userpost SET text=? , datestamp=? , iduserpostrepost=? , iduser=?  WHERE iduserpost = ?";
    /** The query for delete. */
    private static final String DELETEBYID_QUERY = "DELETE FROM userpost WHERE iduserpost = ?";
    /** Query for betting post object. */
    private static final String POSTOBJECT_QUERY = "SELECT * FROM userpost "
    		+ "LEFT JOIN userpostlikes ON userpost.iduserpost = userpostlikes.iduserpost "
    		+ "LEFT JOIN user AS likeuser ON likeuser.iduser = userpostlikes.iduser "
    		+ "LEFT JOIN usercomment ON userpost.iduserpost  = usercomment.iduserpost "
    		+ "LEFT JOIN user AS commentuser ON commentuser.iduser = usercomment.iduser "
    		+ "LEFT JOIN userpostimage ON userpost.iduserpost  = userpostimage.iduserpost "
    		+ "LEFT JOIN userposthashtags ON userpost.iduserpost  = userposthashtags.iduserpost "
    		+ "LEFT JOIN user AS post_user ON post_user.iduser = userpost.iduser "
            + "WHERE userpost.iduserpost = ?";
    
    private static final String POSTIMAGE_BYHASH_QUERY = "SELECT upi.*,up.*  FROM hashtags ht INNER JOIN userposthashtags upht ON ht.idhashtags=upht.idhashtag INNER JOIN userpost up ON upht.iduserpost=up.iduserpost INNER JOIN userpostimage upi ON upi.iduserpost = up.iduserpost WHERE ht.hash=? LIMIT ?,?";
    
    private static final String FIND_REPOST_QUERY = "SELECT * FROM userpost WHERE iduser = ? AND iduserpostrepost = ?";
    
    private static final String UPDATE_EXPIRATION_QUERY = "UPDATE userpost SET expiration = ? WHERE iduserpost = ?";
    
	public boolean updateExpiration(PostModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_EXPIRATION_QUERY);
			preparedStatement.setString(1, updateData.getExpiration().toString());
			preparedStatement.setInt(2, updateData.getIdUserPost());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
	
	@Override
	public int insert(PostModel insertData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, insertData.getText());
			preparedStatement.setString(2, insertData.getDatestamp().toString());
			preparedStatement.setInt(3, insertData.getIdUser());
			preparedStatement.setInt(4, insertData.getIdUserPostRepost());
			preparedStatement.setString(5, insertData.getExpiration().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(PostModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setString(1, updateData.getText());
			preparedStatement.setString(2, updateData.getDatestamp().toString());
			preparedStatement.setInt(3, updateData.getIdUser());
			preparedStatement.setInt(4, updateData.getIdUserPost());
			preparedStatement.setInt(5, updateData.getIdUserPostRepost());

			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public PostModel findByID(int id) throws ClassNotFoundException, SQLException {
		PostModel userPostDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userPostDTO = new PostModel();
				userPostDTO.setIdUserPost(result.getInt(1));
				userPostDTO.setText(result.getString(2));
				userPostDTO.setDatestamp(result.getTimestamp(3));
				userPostDTO.setIdUser(result.getInt(4));
				userPostDTO.setIdUserPostRepost(result.getInt(5));
				userPostDTO.setExpiration(result.getTimestamp(6));
			}
			return userPostDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

	public Integer userReposted(int iduser, int idpost) throws ClassNotFoundException, SQLException{
		PostModel userPostDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FIND_REPOST_QUERY);
			preparedStatement.setInt(1, iduser);
			preparedStatement.setInt(2, idpost);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userPostDTO = new PostModel();
				userPostDTO.setIdUserPost(result.getInt(1));
				userPostDTO.setText(result.getString(2));
				userPostDTO.setDatestamp(result.getTimestamp(3));
				userPostDTO.setIdUser(result.getInt(4));
				userPostDTO.setIdUserPostRepost(result.getInt(5));
				userPostDTO.setExpiration(result.getTimestamp(6));
			}
			if(userPostDTO!=null)
				return 1;
			return 0;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	public PostObject getPostObjectByID(int postid,int userid) throws ClassNotFoundException, SQLException, IOException {
		PostObject postobject = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		TenUtilities utils = new TenUtilities();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(POSTOBJECT_QUERY);
			preparedStatement.setInt(1, postid);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			List<PostObject> postobjects = utils.proccessPostListResultSet(result,userid).getNewsfeed();
			if(postobjects!=null && !postobjects.isEmpty())
				return postobjects.get(0);
			return null;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	public List<PostObject> getImagesByHash(String hash,int offset,int count) throws ClassNotFoundException, SQLException, IOException {
		List<PostObject> posts = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(POSTIMAGE_BYHASH_QUERY);
			preparedStatement.setString(1, hash);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			posts = proccessImageResultSet(result);
			
			return posts;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	private List<PostObject> proccessImageResultSet(ResultSet image_resultset) throws SQLException, IOException{
		List<PostObject> posts = new ArrayList<>();
		TenUtilities globalutils = new TenUtilities();

		while (image_resultset.next()) {
			PostImageModel image = new PostImageModel();
			if(image_resultset.getInt(1)>0){
				image.setIdUserPostImage(1);
				image.setImageSrc(image_resultset.getString(2));
				image.setIdUserPost(image_resultset.getInt(3));
				image.setDatestamp(image_resultset.getTimestamp(4));
			}
			ImageObject upio = new ImageObject(image);
			
			PostObject post = new PostObject();
			
			if(image_resultset.getInt(5)>0){
				post.setIdUserPost(image_resultset.getInt(5));
				post.setText(image_resultset.getString(6));
				post.setDatestamp(image_resultset.getTimestamp(7));
				post.setIdUser(image_resultset.getInt(8));
				post.setIdUserPostRepost(image_resultset.getInt(9));
				post.setExpiration(image_resultset.getTimestamp(10));
			}
			
			String imagePath = BaseServlet.PHOTO_STORE_PATH+post.getIdUser()+"/"+image.getImageSrc();
			upio.setImage(globalutils.encodeToBase64String(globalutils.extractBytes(imagePath)));
			
			if(image!=null && image.getIdUserPost()!=0)
				post.addImage(new ImageObject(image));
		}
		return posts;
	}
	
}
