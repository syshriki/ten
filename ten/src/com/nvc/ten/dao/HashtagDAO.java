package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.HashtagModel;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.extended.PostObject;

public class HashtagDAO implements DAOinterface<HashtagModel> {
	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO hashtags (hash,datestamp) VALUES (?,?)";
    /** The query for read. */
    private static final String FINDBYID_QUERY = "SELECT * FROM hashtags WHERE idhashtags = ?";
    /** The query for finding hashes by name*/
    private static final String FINDLIKEHASH_QUERY = "SELECT * FROM hashtags WHERE hash LIKE ? LIMIT ?,?";
    /** The query for reading where hashes are like */
    private static final String FINDBYHASH_QUERY = "SELECT * FROM hashtags WHERE hash = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE hashtags SET hash=?,datestamp=? WHERE idhashtags = ?";
    /** The query for delete. */
    private static final String DELETEBYID_QUERY = "DELETE FROM hashtags WHERE idhashtags = ?";
    /** Find all posts with hashtags**/ //TODONOT INCLUDE PRIVATE
    private static final String FINDPOSTSBYHASH_QUERY = "SELECT distinct up.* FROM userpost AS up LEFT JOIN userposthashtags as uph ON uph.iduserpost = up.iduserpost LEFT JOIN hashtags AS h ON uph.idhashtag = h.idhashtags LEFT JOIN user AS u ON up.iduser = u.iduser WHERE h.hash LIKE ? AND up.expiration >= NOW() AND u.postsprivate = 0 LIMIT ?,?";
    
	@Override
	public int insert(HashtagModel insertData) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, insertData.getHash());
			preparedStatement.setString(2, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(HashtagModel updateData) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setString(1, updateData.getHash());
			preparedStatement.setString(2, updateData.getDatestamp().toString());
			preparedStatement.setInt(3, updateData.getHashtagid());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public HashtagModel findByID(int id) throws SQLException, ClassNotFoundException {
		HashtagModel hashtagDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				hashtagDTO = new HashtagModel();
				hashtagDTO.setHashtagid(result.getInt(1));
				hashtagDTO.setHash(result.getString(2));
				hashtagDTO.setDatestamp(result.getTimestamp(3));
			}
			return hashtagDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}

	public List<HashtagModel> findLikeHash(String hash,int offset,int count) throws SQLException, ClassNotFoundException {
		HashtagModel hashtagDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		List<HashtagModel> hashtags = new ArrayList<>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDLIKEHASH_QUERY);
			preparedStatement.setString(1, hash);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				hashtagDTO = new HashtagModel();
				hashtagDTO.setHashtagid(result.getInt(1));
				hashtagDTO.setHash(result.getString(2));
				hashtagDTO.setDatestamp(result.getTimestamp(3));
				hashtags.add(hashtagDTO);
			}
			return hashtags;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	public HashtagModel findByHash(String hash) throws ClassNotFoundException, SQLException {
		HashtagModel hashtagDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYHASH_QUERY);
			preparedStatement.setString(1, hash);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if(result.next()) {
				hashtagDTO = new HashtagModel();
				hashtagDTO.setHashtagid(result.getInt(1));
				hashtagDTO.setHash(result.getString(2));
				hashtagDTO.setDatestamp(result.getTimestamp(3));
			}
			return hashtagDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public List<PostModel> findPostByHash(String hash,int offset,int count) throws SQLException, ClassNotFoundException{
		PostModel userPostDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		List<PostModel> userposts = new ArrayList<>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDPOSTSBYHASH_QUERY);
			preparedStatement.setString(1, hash+"%");
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while(result.next() && result != null) {
				userPostDTO = new PostModel();
				userPostDTO.setIdUserPost(result.getInt(1));
				userPostDTO.setText(result.getString(2));
				userPostDTO.setDatestamp(result.getTimestamp(3));
				userPostDTO.setIdUser(result.getInt(4));
				userPostDTO.setIdUserPostRepost(result.getInt(5));
				userposts.add(userPostDTO);
			}
			return userposts;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
}
