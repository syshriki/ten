package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.FollowersModel;
import com.nvc.ten.models.UserModel;

public class FollowersDAO implements DAOinterface<FollowersModel> {

	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO followers (idrequestee,idrecipient,datestamp,approved) VALUES (?,?,?,?)";
	/** The query for read. */
	private static final String FINDBYID_QUERY = "SELECT * FROM followers WHERE idfollowers = ?";
	/** Query for approving a follower request **/
	private static final String UPDATE_APPROVED_QUERY = "UPDATE followers SET approved=? WHERE idrequestee=? AND idrecipient=?";
	/** The query for update. */
	private static final String UPDATE_QUERY = "UPDATE followers SET idrequestee=? , idrecipient=? , datestamp=?, approved=? WHERE idfollowers = ?";
	/** The query for delete. */
	private static final String DELETEBYID_QUERY = "DELETE FROM followers WHERE idfollowers = ?";
	/** Get all followers **/
	private static final String FIND_FOLLOWING_QUERY = "SELECT * FROM user WHERE EXISTS (SELECT * FROM followers WHERE user.iduser = followers.idrecipient AND followers.idrequestee = ? AND followers.approved = 1) ORDER BY user.username LIMIT ?,?";
	/** Get all followers **/
	private static final String FIND_PENDING_QUERY = "SELECT * FROM user WHERE EXISTS (SELECT * FROM followers WHERE user.iduser = followers.idrequestee AND followers.idrecipient = ? AND followers.approved = 0) ORDER BY user.username LIMIT ?,?";
	/** Get all following **/
	private static final String FIND_FOLLOWERS_QUERY = "SELECT * FROM user WHERE EXISTS (SELECT * FROM followers WHERE user.iduser = followers.idrequestee AND followers.idrecipient = ? AND followers.approved = 1) ORDER BY user.username LIMIT ?,?";
	/** Find by user pair. */
	private static final String FINDBYFOLLOWINGID_QUERY = "SELECT * FROM followers WHERE idrequestee = ? AND idrecipient = ?";
	/** Is user following */
	private static final String GETFOLLOWING_QUERY = "SELECT * FROM followers WHERE followers.idrequestee = ? AND followers.idrecipient = ? AND followers.approved = 1";

	private static final String REJECT_QUERY = "DELETE FROM followers WHERE idrequestee=? AND idrecipient=?";

	@Override
	public int insert(FollowersModel insertData) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, insertData.getIdUserSlave());
			preparedStatement.setInt(2, insertData.getIdUserMaster());
			preparedStatement
					.setString(3, insertData.getDatestamp().toString());
			preparedStatement.setInt(4, insertData.getApproved());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(FollowersModel updateData) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, updateData.getIdUserSlave());
			preparedStatement.setInt(2, updateData.getIdUserMaster());
			preparedStatement
					.setString(3, updateData.getDatestamp().toString());
			preparedStatement.setInt(4, updateData.getApproved());
			preparedStatement.setInt(5, updateData.getIdPair());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
	
	public boolean rejectFollower(FollowersModel updateData) throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(REJECT_QUERY);
			preparedStatement.setInt(1, updateData.getIdUserSlave());
			preparedStatement.setInt(2, updateData.getIdUserMaster());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}
	@Override
	public FollowersModel findByID(int id) throws SQLException,
			ClassNotFoundException {
		FollowersModel followersModel = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				followersModel = new FollowersModel();
				followersModel.setIdPair(result.getInt(1));
				followersModel.setIdUserSlave(result.getInt(2));
				followersModel.setIdUserMaster(result.getInt(3));
				followersModel.setApproved(result.getInt(4));
				followersModel.setDatestamp(result.getTimestamp(4));
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
		return followersModel;
	}

	public Set<UserModel> findFollowers(int id,int offset,int count) throws SQLException,
			ClassNotFoundException {
		UserModel userDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<UserModel> userpairs = new HashSet<UserModel>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FIND_FOLLOWERS_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userpairs.add(userDTO);
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
		return userpairs;
	}

	public Set<UserModel> findFollowing(int id, int offset, int count) throws SQLException,
			ClassNotFoundException {
		UserModel userDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<UserModel> userpairs = new HashSet<UserModel>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FIND_FOLLOWING_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userpairs.add(userDTO);
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
		return userpairs;
	}

	public FollowersModel findFollowingID(int idrequestee, int idrecipient)
			throws SQLException, ClassNotFoundException {
		FollowersModel followersModel = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYFOLLOWINGID_QUERY);
			preparedStatement.setInt(1, idrequestee);
			preparedStatement.setInt(2, idrecipient);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				followersModel = new FollowersModel();
				followersModel.setIdPair(result.getInt(1));
				followersModel.setIdUserSlave(result.getInt(2));
				followersModel.setIdUserMaster(result.getInt(3));
				followersModel.setDatestamp(result.getTimestamp(4));
				followersModel.setApproved(result.getInt(5));
			}
			return followersModel;

		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	public FollowersModel getFollowing(int idrequestee, int idrecipient)
			throws SQLException {
		FollowersModel followersModel = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(GETFOLLOWING_QUERY);
			preparedStatement.setInt(1, idrequestee);
			preparedStatement.setInt(2, idrecipient);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				followersModel = new FollowersModel();
				followersModel.setIdPair(result.getInt(1));
				followersModel.setIdUserSlave(result.getInt(2));
				followersModel.setIdUserMaster(result.getInt(3));
				followersModel.setDatestamp(result.getTimestamp(4));
			}
		} catch (SQLException | ClassNotFoundException e) {
			dbfactory.returnConnection(conn);
			e.getMessage();
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
		return followersModel;
	}

	public boolean approveFollower(FollowersModel updateData)
			throws SQLException, ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_APPROVED_QUERY);
			preparedStatement.setInt(1, updateData.getApproved());
			preparedStatement.setInt(2, updateData.getIdUserSlave());
			preparedStatement.setInt(3, updateData.getIdUserMaster());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	public Set<UserModel> findPending(int userid,int offset, int count) throws SQLException,
			ClassNotFoundException {
		UserModel userDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<UserModel> userpairs = new HashSet<UserModel>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FIND_PENDING_QUERY);
			preparedStatement.setInt(1,userid);
			preparedStatement.setInt(2,offset);
			preparedStatement.setInt(3,count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				userDTO = new UserModel();
				userDTO.setIdUser(result.getInt(1));
				userDTO.setUsername(result.getString(2));
				userDTO.setPassword(result.getBytes(3));
				userDTO.setDatestamp(result.getTimestamp(4));
				userDTO.setDescription(result.getString(5));
				userDTO.setEmail(result.getString(6));
				userDTO.setImagesrc(result.getString(7));
				userpairs.add(userDTO);
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
		return userpairs;
	}
}
