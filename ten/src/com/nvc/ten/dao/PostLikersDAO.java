package com.nvc.ten.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.PopularTimerObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.UserTO;

public class PostLikersDAO {
	private static final String POSTLIKERS_QUERY = "SELECT u.* FROM user AS u INNER JOIN userpostlikes AS upl ON upl.iduser=u.iduser INNER JOIN userpost AS up ON upl.iduserpost=up.iduserpost WHERE up.iduserpost=? ORDER BY u.username LIMIT ?,?";
	
	public List<UserModel> getPostLikers(int postid,int offset,int count) throws SQLException, ClassNotFoundException, IOException {
		List<UserModel> users = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(POSTLIKERS_QUERY);
			preparedStatement.setInt(1, postid);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, offset+count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			while (result.next()) {
				UserModel user = new UserModel();
				user.setIdUser(result.getInt(1));
				user.setUsername(result.getString(2));
				user.setPassword(result.getBytes(3));
				user.setDatestamp(result.getTimestamp(4));
				user.setDescription(result.getString(5));
				user.setEmail(result.getString(6));
				user.setImagesrc(result.getString(7));
				user.setWebsite(result.getString(8));
				user.setPrivate(result.getBoolean(9));
				users.add(user);
			}
		return users;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
}
