package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.BlockUserModel;

public class UserBlocksDAO implements DAOinterface<BlockUserModel> {
	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO userblocks (iduserblocked,iduserblocker,datestamp) VALUES (?,?,?)";
    /** The query for read. */
    private static final String FINDBYID_QUERY = "SELECT * FROM userblocks WHERE iduserblocks = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE userblocks SET iduserblocked=? , iduserblocker=? , datestamp=? WHERE iduserblocks = ?";
    /** The query for delete. */
    private static final String DELETEBYID_QUERY = "DELETE FROM userblocks WHERE iduserblocks = ?";
    /** The query for read. */
    private static final String FINDBYUSERID_QUERY = "SELECT * FROM userblocks WHERE iduserblocked = ? AND iduserblocker = ?";
    
	@Override
	public int insert(BlockUserModel insertData) throws SQLException, ClassNotFoundException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, insertData.getIdUserSlave());
			preparedStatement.setInt(2, insertData.getIdUserMaster());
			preparedStatement.setString(3, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		}finally{
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(BlockUserModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, updateData.getIdUserSlave());
			preparedStatement.setInt(2, updateData.getIdUserMaster());
			preparedStatement.setString(3, updateData.getDatestamp().toString());
			preparedStatement.setInt(4, updateData.getIdPair());
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public BlockUserModel findByID(int id) throws ClassNotFoundException, SQLException {
		BlockUserModel blockUserModel = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				blockUserModel = new BlockUserModel();
				blockUserModel.setIdPair(result.getInt(1));
				blockUserModel.setIdUserSlave(result.getInt(2));
				blockUserModel.setIdUserMaster(result.getInt(3));
				blockUserModel.setDatestamp(result.getTimestamp(4));
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

		return blockUserModel;
	}
	
	public BlockUserModel findByUserID(int idblocker,int idblocked) throws ClassNotFoundException, SQLException {
		BlockUserModel blockUserModel = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYUSERID_QUERY);
			preparedStatement.setInt(1, idblocked);
			preparedStatement.setInt(2, idblocker);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				blockUserModel = new BlockUserModel();
				blockUserModel.setIdPair(result.getInt(1));
				blockUserModel.setIdUserSlave(result.getInt(2));
				blockUserModel.setIdUserMaster(result.getInt(3));
				blockUserModel.setDatestamp(result.getTimestamp(4));
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

		return blockUserModel;
	}
}
