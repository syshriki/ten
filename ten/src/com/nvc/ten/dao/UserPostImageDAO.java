package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.PostImageModel;

public class UserPostImageDAO implements DAOinterface<PostImageModel> {
	/** The query for creation. */
	private static final String INSERT_QUERY = "INSERT INTO userpostimage (iduserpost,imagesrc,datestamp) VALUES (?,?,?)";
    /** The query for read. */
    private static final String FINDBYID_QUERY = "SELECT * FROM userpostimage WHERE iduserpostimage = ?";
    /** The query for update. */
    private static final String UPDATE_QUERY = "UPDATE userpostimage SET iduserpost=? , imagesrc=? , datestamp=? WHERE iduserpostimage = ?";
    /** The query for delete. */
    private static final String DELETEBYID_QUERY = "DELETE FROM userpostimage WHERE iduserpostimage = ?";
    
    private static final String FINDBYPOSTID_QUERY = "SELECT * FROM userpostimage WHERE iduserpost = ?";

    
	@Override
	public int insert(PostImageModel insertData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(INSERT_QUERY,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setInt(1, insertData.getIdUserPost());
			preparedStatement.setString(2, insertData.getImageSrc());
			preparedStatement.setString(3, insertData.getDatestamp().toString());
			preparedStatement.execute();
			result = preparedStatement.getGeneratedKeys();

			if (result.next() && result != null) {
				return result.getInt(1);
			} else {
				return -1;
			}
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

	@Override
	public boolean update(PostImageModel updateData) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(UPDATE_QUERY);
			preparedStatement.setInt(1, updateData.getIdUserPost());
			preparedStatement.setString(2, updateData.getImageSrc());
			preparedStatement.setString(3, updateData.getDatestamp().toString());
			preparedStatement.setInt(4, updateData.getIdUserPostImage());

			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public boolean deleteByID(int deleteDataId) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(DELETEBYID_QUERY);
			preparedStatement.setInt(1, deleteDataId);
			preparedStatement.execute();
			return true;
		} finally {
			dbfactory.returnConnection(conn);
			preparedStatement.close();
		}
	}

	@Override
	public PostImageModel findByID(int id) throws ClassNotFoundException, SQLException {
		PostImageModel userPostImageDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			if (result.next() && result != null) {
				userPostImageDTO = new PostImageModel();
				userPostImageDTO.setIdUserPostImage(result.getInt(1));
				userPostImageDTO.setImageSrc(result.getString(2));
				userPostImageDTO.setIdUserPost(result.getInt(3));
				userPostImageDTO.setDatestamp(result.getTimestamp(4));
			}
			return userPostImageDTO;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public Set<PostImageModel> findByPostID(int id) throws ClassNotFoundException, SQLException {
		PostImageModel userPostImageDTO = null;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		Set<PostImageModel> images = new HashSet<>();
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(FINDBYPOSTID_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next() && result != null) {
				userPostImageDTO = new PostImageModel();
				userPostImageDTO.setIdUserPostImage(result.getInt(1));
				userPostImageDTO.setImageSrc(result.getString(2));
				userPostImageDTO.setIdUserPost(result.getInt(3));
				userPostImageDTO.setDatestamp(result.getTimestamp(4));
				images.add(userPostImageDTO);
			}
			return images;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}

}
