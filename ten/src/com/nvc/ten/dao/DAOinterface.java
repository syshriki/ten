package com.nvc.ten.dao;

import java.sql.SQLException;


public interface DAOinterface<T> {
	public int insert(T insertData) throws SQLException, ClassNotFoundException;
	public boolean update(T updateData) throws SQLException, ClassNotFoundException;
	public boolean deleteByID(int deleteDataId) throws SQLException, ClassNotFoundException;
	public T findByID(int id) throws SQLException, ClassNotFoundException;
}
