package com.nvc.ten.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.nvc.ten.factories.TenDBConnectionFactory;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.extended.ProfileBestTimeObject;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.PostObject;

public class ProfileDAO {
	private static final String PROFILE_POST_COUNT_QUERY = "(SELECT COUNT(userpost.iduserpost) FROM userpost,user WHERE user.iduser = ? AND userpost.iduser = user.iduser)";
	private static final String PROFILE_FOLLOWERS_QUERY = "(SELECT COUNT(followers.idfollowers) FROM followers,user WHERE user.iduser = followers.idrequestee AND followers.idrecipient = ? AND followers.approved = 1) LIMIT ?,?";
	private static final String PROFILE_FOLLOWING_QUERY = "(SELECT COUNT(followers.idfollowers) FROM followers,user WHERE user.iduser = followers.idrecipient AND followers.idrequestee = ? AND followers.approved = 1) LIMIT ?,?";
	private static final String PROFILE_TOP_LIKERS_QUERY = "SELECT user.*,COUNT(*) as count FROM userpostlikes LEFT JOIN user ON userpostlikes.iduser = user.iduser WHERE userpostlikes.iduserpost IN (SELECT iduserpost FROM userpost WHERE userpost.iduser = ?) GROUP BY iduser ORDER BY count DESC LIMIT 3";
	private static final String PROFILE_ALL_POSTS_QUERY ="SELECT * FROM userpost LEFT JOIN userpostimage ON userpostimage.iduserpost = userpost.iduserpost WHERE userpost.iduser = ? ORDER BY userpost.expiration DESC LIMIT ?,?";
	private static final String PROFILE_ACTIVE_POSTS_QUERY ="SELECT * FROM userpost LEFT JOIN userpostimage ON userpostimage.iduserpost = userpost.iduserpost WHERE userpost.expiration >= NOW() AND userpost.iduser = ? ORDER BY userpost.datestamp DESC LIMIT ?,?";
	private static final String PROFILE_LIKE_COUNT_QUERY ="SELECT COUNT(*) as likecount FROM userpostlikes WHERE (userpostlikes.iduserpost) IN (SELECT userpost.iduserpost as posts FROM userpost WHERE userpost.iduser = ?) GROUP BY userpostlikes.iduserpost ORDER BY likecount DESC LIMIT 1";
	private static final String PROFILE_COMMENT_COUNT_QUERY ="SELECT COUNT(*) as commentcount FROM usercomment WHERE (usercomment.iduserpost) IN (SELECT userpost.iduserpost as posts FROM userpost where userpost.iduser = ?) GROUP BY usercomment.iduserpost ORDER BY commentcount DESC LIMIT 1";
	private static final String PROFILE_BESTTIME_QUERY = "SELECT userpost.*,COUNT(distinct iduserpostlikes) as timeweight FROM userpost LEFT JOIN userpostlikes as likecount ON likecount.iduserpost = userpost.iduserpost WHERE userpost.iduser = ? GROUP BY userpost.iduserpost ORDER BY timeweight DESC LIMIT 1";
	
	//Map of post dto to best time
	public ProfileBestTimeObject getBestTime(int id) throws SQLException, ClassNotFoundException {
		ProfileBestTimeObject bestTime= new ProfileBestTimeObject();
		PostModel userPostDTO;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();//TODO TEST THIS DAO, BOTTOM 2 IDS MIGHT BE SWAPPED
			preparedStatement = conn.prepareStatement(PROFILE_BESTTIME_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			if (result.next() && result != null) {
				userPostDTO = new PostModel();
				if(result.getInt(1)!=0){
					userPostDTO.setIdUserPost(result.getInt(1));
					userPostDTO.setText(result.getString(2));
					userPostDTO.setDatestamp(result.getTimestamp(3));
					userPostDTO.setIdUser(result.getInt(4));
					userPostDTO.setIdUserPostRepost(result.getInt(5));
				}
				bestTime.setPost(userPostDTO);
				bestTime.setTime(result.getInt("timeweight"));
			} else {
				// TODO
			}
			
		return bestTime;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public int getCommentCount(int id) throws SQLException, ClassNotFoundException {
		int commentCount = 0;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_COMMENT_COUNT_QUERY );
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			if(result.next()){
				commentCount = result.getInt(1);
			}
			else
				commentCount = 0;
		return commentCount;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	public int getTopLikeCount(int id) throws SQLException, ClassNotFoundException {
		int likeCount = 0;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_LIKE_COUNT_QUERY );
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			if(result.next())
				likeCount = result.getInt(1);
			else
				likeCount = 0;

		return likeCount;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	public List<PostObject> getPosts(int id,int offset,int count) throws SQLException, ClassNotFoundException{
		List<PostObject> posts = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_ALL_POSTS_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				PostObject userPostObject = new PostObject();
				userPostObject.setIdUserPost(result.getInt(1));
				userPostObject.setText(result.getString(2));
				userPostObject.setDatestamp(result.getTimestamp(3));
				userPostObject.setIdUser(result.getInt(4));
				userPostObject.setIdUserPostRepost(result.getInt(5));
				userPostObject.setExpiration(result.getTimestamp(6));
				ImageObject userPostImageDTO = new ImageObject();
				userPostImageDTO.setIdUserPostImage(result.getInt(7));
				userPostImageDTO.setImageSrc(result.getString(8));
				userPostImageDTO.setIdUserPost(result.getInt(9));
				userPostImageDTO.setDatestamp(result.getTimestamp(10));
				userPostObject.setImages(new HashSet<ImageObject>(Arrays.asList(userPostImageDTO)));
				
				posts.add(userPostObject);
			}
		return posts;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public List<PostObject> getActivePosts(int id,int offset,int count) throws SQLException, ClassNotFoundException{
		List<PostObject> posts = new ArrayList<>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_ACTIVE_POSTS_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			while (result.next()) {
				PostObject userPostObject = new PostObject();
				userPostObject.setIdUserPost(result.getInt(1));
				userPostObject.setText(result.getString(2));
				userPostObject.setDatestamp(result.getTimestamp(3));
				userPostObject.setIdUser(result.getInt(4));
				userPostObject.setIdUserPostRepost(result.getInt(5));
				userPostObject.setExpiration(result.getTimestamp(6));
				ImageObject userPostImageDTO = new ImageObject();
				userPostImageDTO.setIdUserPostImage(result.getInt(7));
				userPostImageDTO.setImageSrc(result.getString(8));
				userPostImageDTO.setIdUserPost(result.getInt(9));
				userPostImageDTO.setDatestamp(result.getTimestamp(10));
				userPostObject.setImages(new HashSet<ImageObject>(Arrays.asList(userPostImageDTO)));
				
				posts.add(userPostObject);
			}
		return posts;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	
	public List<UserModel> getTopLikers(int id) throws SQLException, ClassNotFoundException{
		List<UserModel> topLikers = new ArrayList<UserModel>();
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_TOP_LIKERS_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();

			topLikers = proccessTopLikersResultSet(result);

		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

		return topLikers;
	}
	
	private List<UserModel> proccessTopLikersResultSet(ResultSet toplikers_resultset) throws SQLException{
		List<UserModel> topLikers = new ArrayList<UserModel>();
		while (toplikers_resultset.next()) {
			UserModel user = new UserModel();
			
			if(toplikers_resultset.getInt(1)>0){
				user.setIdUser(toplikers_resultset.getInt(1));
				user.setUsername(toplikers_resultset.getString(2));
				user.setPassword(toplikers_resultset.getBytes(3));
				user.setDatestamp(toplikers_resultset.getTimestamp(4));
				user.setDescription(toplikers_resultset.getString(5));
				user.setEmail(toplikers_resultset.getString(6));
				user.setImagesrc(toplikers_resultset.getString(7));
				user.setWebsite(toplikers_resultset.getString(8));
				user.setPrivate(toplikers_resultset.getBoolean(9));
			}
			topLikers.add(user);
		}
		return topLikers;
	}
	
	public int getProfilePosts(int id) throws SQLException, ClassNotFoundException {
		int postCount = 0;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_POST_COUNT_QUERY );
			preparedStatement.setInt(1, id);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			if (result.next()) 
				postCount = result.getInt(1);
			else
				postCount = 0;

		return postCount;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}

	}
	
	public int getFollowerCount(int id,int offset, int count) throws SQLException, ClassNotFoundException {
		int followerCount = 0;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_FOLLOWERS_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			if(result.next())
				followerCount = result.getInt(1);
			else
				followerCount = 0;

		return followerCount;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	public int getFollowingCount(int id, int offset, int count) throws SQLException, ClassNotFoundException {
		int followingCount = 0;
		Connection conn = null;
		TenDBConnectionFactory dbfactory = new TenDBConnectionFactory();
		PreparedStatement preparedStatement = null;
		ResultSet result = null;
		try {
			conn = dbfactory.getConnection();
			preparedStatement = conn.prepareStatement(PROFILE_FOLLOWING_QUERY);
			preparedStatement.setInt(1, id);
			preparedStatement.setInt(2, offset);
			preparedStatement.setInt(3, count);
			preparedStatement.execute();
			result = preparedStatement.getResultSet();
			
			if(result.next())
				followingCount = result.getInt(1);
			else
				followingCount  = 0;

		return followingCount;
		} finally {
			dbfactory.returnConnection(conn);
			result.close();
			preparedStatement.close();
		}
	}
	
	
}
