package com.nvc.ten;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.nvc.ten.pooling.MYSQLPool;
import com.nvc.ten.pooling.MYSQLPools;
import com.nvc.ten.pooling.timertask.PingConnectionsTask;
import com.nvc.ten.servlets.BaseServlet;
//This only runs once on tocmat startup
public class Application implements ServletContextListener {
	public static final String saltPoolName = "saltdbpool";
	public static final String tenPoolName = "tendbpool";
	private ScheduledExecutorService scheduler;
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		scheduler.shutdownNow();
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try {
			this.loadProps();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fillConnectionPools();
		scheduler = Executors.newSingleThreadScheduledExecutor();
	    scheduler.scheduleAtFixedRate(new PingConnectionsTask(), 0, 4, TimeUnit.HOURS);
	}

	
	private void fillConnectionPools(){
		if(MYSQLPools.getPool(saltPoolName)==null){
			MYSQLPools.addPool(saltPoolName, new MYSQLPool(BaseServlet.TENSALT_CONNECTION_STRING,BaseServlet.TENSALT_USERNAME,BaseServlet.TENSALT_PASSWORD,BaseServlet.SALTDB_MAX_CONNECTION_COUNT));
		}
		if(MYSQLPools.getPool(tenPoolName)==null){
			MYSQLPools.addPool(tenPoolName, new MYSQLPool(BaseServlet.TENDB_CONNECTION_STRING,BaseServlet.TENDB_USERNAME,BaseServlet.TENDB_PASSWORD,BaseServlet.TENDB_MAX_CONNECTION_COUNT));
		}
	}
	
	
	private void loadProps() throws IOException {
		Properties prop = new Properties();
		String propFileName = "config.properties";

		InputStream inputStream = getClass().getClassLoader()
				.getResourceAsStream("com/nvc/ten/config/" + propFileName);

		if (inputStream != null) {
			prop.load(inputStream);
		} else {
			throw new FileNotFoundException("property file '" + propFileName
					+ "' not found in the classpath");
		}

		BaseServlet.SALTDB_MAX_CONNECTION_COUNT = Integer.parseInt(prop
				.getProperty("SALTDB_MAX_CONNECTION_COUNT"));
		BaseServlet.TENDB_MAX_CONNECTION_COUNT = Integer.parseInt(prop
				.getProperty("TENDB_MAX_CONNECTION_COUNT"));

		BaseServlet.MAX_IMAGE_WIDTH = Integer.parseInt(prop
				.getProperty("MAX_IMAGE_WIDTH"));
		BaseServlet.MAX_IMAGE_HEIGHT = Integer.parseInt(prop
				.getProperty("MAX_IMAGE_HEIGHT"));
		BaseServlet.PHOTO_STORE_PATH = prop.getProperty("PHOTO_STORE_PATH");
		BaseServlet.MAX_IMAGE_FILE_SIZE = Integer.parseInt(prop
				.getProperty("MAX_IMAGE_FILE_SIZE"));
		BaseServlet.TENSALT_USERNAME = prop.getProperty("TENSALT_USERNAME");
		BaseServlet.TENSALT_PASSWORD = prop.getProperty("TENSALT_PASSWORD");
		BaseServlet.TENSALT_CONNECTION_STRING = prop.getProperty("TENSALT_CONNECTION_STRING");
		BaseServlet.TENDB_USERNAME = prop.getProperty("TENDB_USERNAME");
		BaseServlet.TENDB_PASSWORD = prop.getProperty("TENDB_PASSWORD");
		BaseServlet.TENDB_CONNECTION_STRING = prop.getProperty("TENDB_CONNECTION_STRING");		
	}
	
}
