package com.nvc.ten.models;

public class BlockUserModel extends ModelBase{
	private int idPair;
	private int idUserMaster;
	private int idUserSlave;
	public int getIdPair() {
		return idPair;
	}
	public void setIdPair(int idPair) {
		this.idPair = idPair;
	}
	public int getIdUserMaster() {
		return idUserMaster;
	}
	public void setIdUserMaster(int idUserMaster) {
		this.idUserMaster = idUserMaster;
	}
	public int getIdUserSlave() {
		return idUserSlave;
	}
	public void setIdUserSlave(int idUserSlave) {
		this.idUserSlave = idUserSlave;
	}
	
}
