package com.nvc.ten.models;


public class HashtagModel extends ModelBase{
	private int hashtagid;
	private String hash;
	public int getHashtagid() {
		return hashtagid;
	}
	public void setHashtagid(int hashtagid) {
		this.hashtagid = hashtagid;
	}
	public String getHash() {
		return hash;
	}
	public void setHash(String hash) {
		this.hash = hash;
	}
}
