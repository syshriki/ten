package com.nvc.ten.models;


public class LikeModel extends ModelBase{
	private int idUserPostLike;
	private int idUserPost;
	private int idUser;
	public int getIdUserPostLike() {
		return idUserPostLike;
	}
	public void setIdUserPostLike(int idUserPostLike) {
		this.idUserPostLike = idUserPostLike;
	}
	public int getIdUserPost() {
		return idUserPost;
	}
	public void setIdUserPost(int idUserPost) {
		this.idUserPost = idUserPost;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
}
