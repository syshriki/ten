package com.nvc.ten.models;


public class CommentModel extends ModelBase{
	private int idUserComment;
	private int idUser;
	private int idUserPost;
	private String comment;
	public int getIdUserComment() {
		return idUserComment;
	}
	public void setIdUserComment(int idUserComment) {
		this.idUserComment = idUserComment;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdUserPost() {
		return idUserPost;
	}
	public void setIdUserPost(int idUserPost) {
		this.idUserPost = idUserPost;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
}
