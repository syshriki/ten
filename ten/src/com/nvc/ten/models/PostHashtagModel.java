package com.nvc.ten.models;


public class PostHashtagModel extends ModelBase{
	private int userPostHashtagid;
	private int userPostid;
	private int hashtagid;
	public int getUserPostid() {
		return userPostid;
	}
	public void setUserPostid(int userPostid) {
		this.userPostid = userPostid;
	}
	public int getHashtagid() {
		return hashtagid;
	}
	public void setHashtagid(int hashtagid) {
		this.hashtagid = hashtagid;
	}
	public int getUserPostHashtagid() {
		return userPostHashtagid;
	}
	public void setUserPostHashtagid(int userPostHashtagid) {
		this.userPostHashtagid = userPostHashtagid;
	}
}
