package com.nvc.ten.models;


public class UserSalt extends ModelBase{
	private int iduser;
	private byte[] salt;
	public int getIduser() {
		return iduser;
	}
	public void setIduser(int iduser) {
		this.iduser = iduser;
	}
	public byte[] getSalt() {
		return salt;
	}
	public void setSalt(byte[] salt) {
		this.salt = salt;
	}
}
