package com.nvc.ten.models.transfer;

import java.util.List;

//This is  how the iphone reads it so this is how I made it
public class PostListTO {
	private List<PostTO> posts;

	public PostListTO(List<PostTO> posts){
		this.posts = posts;
	}
	public List<PostTO> getPosts() {
		return posts;
	}

	public void setPosts(List<PostTO> posts) {
		this.posts = posts;
	}
	
}
