package com.nvc.ten.models.transfer;

import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.models.extended.ProfileObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.PostObject;

public class ProfileSelfTO {
	private Integer followerCount;
	private Integer followingCount;
	private Integer postCount;
	private Integer topTime;
	private String userimage;
	private String userdescription;
	private String username;
	private String website;
	private List<PostTO> posts;
	private List<ProfileLikerTO> likers;
	
	public ProfileSelfTO(ProfileObject po){
		this.followerCount = po.getFollowers();
		this.followingCount = po.getFollowing();
		this.postCount = po.getPostCount();
		this.topTime = po.getTopTime();
		this.userimage = po.getUser().getImage();
		this.userdescription = po.getUser().getDescription();
		this.posts = this.extractPosts(po);
		this.likers = this.extractLikers(po);
		this.username = po.getUser().getUsername();
		this.website = po.getUser().getWebsite();
	}
	
	public Integer getFollowerCount() {
		return followerCount;
	}
	public void setFollowerCount(Integer followerCount) {
		this.followerCount = followerCount;
	}
	public Integer getFollowingCount() {
		return followingCount;
	}
	public void setFollowingCount(Integer followingCount) {
		this.followingCount = followingCount;
	}
	public Integer getPostCount() {
		return postCount;
	}
	public void setPostCount(Integer postCount) {
		this.postCount = postCount;
	}
	public Integer getTopTime() {
		return topTime;
	}
	public void setTopTime(Integer topTime) {
		this.topTime = topTime;
	}
	public String getUserimage() {
		return userimage;
	}
	public void setUserimage(String userimage) {
		this.userimage = userimage;
	}
	public String getUserdescription() {
		return userdescription;
	}
	public void setUserdescription(String userdescription) {
		this.userdescription = userdescription;
	}

	public List<PostTO> getPosts() {
		return posts;
	}

	public void setPosts(List<PostTO> posts) {
		this.posts = posts;
	}
	
	public List<PostTO> extractPosts(ProfileObject po){
		List<PostTO> posts = new ArrayList<>();
		for(PostObject upo : po.getUserposts()){
			posts.add(new PostTO(upo));
		}
		return posts;
	}
	
	public List<ProfileLikerTO> extractLikers(ProfileObject po){
		List<ProfileLikerTO> likers = new ArrayList<>();
		for(UserObject uo : po.getTopLikers()){
			if(uo.getIdUser()>0){
				likers.add(new ProfileLikerTO(uo));
			}
		}
		return likers;
	}

	public List<ProfileLikerTO> getLikers() {
		return likers;
	}

	public void setLikers(List<ProfileLikerTO> likers) {
		this.likers = likers;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
}
