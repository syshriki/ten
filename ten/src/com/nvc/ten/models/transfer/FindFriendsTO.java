package com.nvc.ten.models.transfer;

import java.util.List;

public class FindFriendsTO {
	private List<FindFriendTO> users;

	public List<FindFriendTO> getUsers() {
		return users;
	}

	public void setUsers(List<FindFriendTO> users) {
		this.users = users;
	}
}
