package com.nvc.ten.models.transfer;

import java.util.List;

public class FollowingTO {
	private List<UserTO> users;

	public List<UserTO> getUsers() {
		return users;
	}

	public void setUsers(List<UserTO> users) {
		this.users = users;
	}
}
