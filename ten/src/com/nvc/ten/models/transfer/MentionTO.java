package com.nvc.ten.models.transfer;

import com.nvc.ten.models.extended.MentionObject;

public class MentionTO {
	private UserTO mentioned;

	public MentionTO(MentionObject umo) {
		this.mentioned = new UserTO(umo.getMentioned());
	}
	
	public UserTO getMentioned() {
		return mentioned;
	}

	public void setMentioned(UserTO Mentioned) {
		this.mentioned = Mentioned;
	}
}
