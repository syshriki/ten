package com.nvc.ten.models.transfer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.nvc.ten.models.extended.CommentObject;
import com.nvc.ten.models.extended.MentionObject;
import com.nvc.ten.models.extended.HashtagObject;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.LikeObject;
import com.nvc.ten.models.extended.PostObject;

public class PostTO {
	private Integer postid;
	private String image;
	private Integer repostid;
	private String text;
	private Date datestamp;
	private List<Integer> likes;
	private List<NewsfeedHashtagTO> hashes;
	private List<CommentTO> comments;
	private UserTO user;
	private Integer reposted;
	private Set<MentionTO> mentions;
	private UserTO reposter;
	
	public PostTO(PostObject upo){
		this.setPostid(upo.getIdUserPost());
		this.image = this.extractImage(upo);
		this.likes = this.extractLikes(upo);
		this.hashes = this.extractHashtag(upo);
		//User is not always included since it is redundant in some cases
		if(upo.getUser()!=null)
			this.user = new UserTO(upo.getUser());
		if(upo.getReposter()!=null)
			this.reposter = new UserTO(upo.getReposter());
		this.repostid = upo.getIdUserPostRepost();
		this.text = upo.getText();
		this.datestamp = upo.getDatestamp();
		this.setReposted(upo.isReposted());
		this.comments = this.extractComments(upo);
		this.mentions = this.extractMentions(upo);
	}
	
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public Integer getRepostid() {
		return repostid;
	}
	public void setRepostid(Integer repostid) {
		this.repostid = repostid;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	} 
	
	public List<Integer> extractLikes(PostObject upo){
		List<Integer> likers = new ArrayList<>();
		for(LikeObject uplo : upo.getLikes()){
			if(uplo.getIdUserPostLike()>0)
				likers.add(uplo.getIdUser());
		}
		return likers;
	}
	
	public List<NewsfeedHashtagTO> extractHashtag(PostObject upo){
		List<NewsfeedHashtagTO> hashtags = new ArrayList<>();
		for(HashtagObject upho : upo.getUserPostHashtags()){
			if(upho.getUserPostHashtagid()>0){
				hashtags.add(new NewsfeedHashtagTO(upho));
			}
		}
		return hashtags;
	}
	
	private String extractImage(PostObject upo){
		if(upo.getImages()!=null && upo.getImages().size()>0){
			Object[] imgs = upo.getImages().toArray();
			Object img = imgs[0];
			ImageObject upi = (ImageObject)img;
			return upi.getImage();
		}
		return "";
	}

	private List<CommentTO> extractComments(PostObject upo){
		List<CommentTO> comments = new ArrayList<>();
		for(CommentObject c : upo.getComments()){
			comments.add(new CommentTO(c));
		}
		return comments;
	}
	
	private Set<MentionTO> extractMentions(PostObject upo){
		Set<MentionObject> umo = upo.getMentions();
		Set<MentionTO> umtos = new HashSet<>();
		for(MentionObject u : umo){
			umtos.add(new MentionTO(u));
		}
		return umtos;
	}
	
	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	public List<Integer> getLikes() {
		return likes;
	}

	public void setLikes(List<Integer> likes) {
		this.likes = likes;
	}

	public List<NewsfeedHashtagTO> getHashes() {
		return hashes;
	}

	public void setHashes(List<NewsfeedHashtagTO> hashes) {
		this.hashes = hashes;
	}

	public UserTO getUser() {
		return user;
	}

	public void setUser(UserTO user) {
		this.user = user;
	}

	public Integer getPostid() {
		return postid;
	}

	public void setPostid(Integer postid) {
		this.postid = postid;
	}

	public Integer getReposted() {
		return reposted;
	}

	public void setReposted(Integer reposted) {
		this.reposted = reposted;
	}

	public List<CommentTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentTO> comments) {
		this.comments = comments;
	}

	public UserTO getReposter() {
		return reposter;
	}

	public void setReposter(UserTO reposter) {
		this.reposter = reposter;
	}
	
}
