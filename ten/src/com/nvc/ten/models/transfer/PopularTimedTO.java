package com.nvc.ten.models.transfer;


public class PopularTimedTO {
	private PostTO post;
	private int score;
	public PopularTimedTO(PostTO post) {
		this.post = post;
	}
	public PostTO getPost() {
		return post;
	}
	public void setPost(PostTO post) {
		this.post = post;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
}
