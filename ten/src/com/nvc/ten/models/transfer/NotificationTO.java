package com.nvc.ten.models.transfer;

import java.util.Date;

public class NotificationTO {
	private Date datestamp;
	private PostTO post;
	private UserTO user; //User that caused notification
	private String notificationType;
	private int notificationId;
	private int likecount;
	
	public Date getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
	public PostTO getPost() {
		return post;
	}
	public void setPost(PostTO post) {
		this.post = post;
	}
	public UserTO getUser() {
		return user;
	}
	public void setUser(UserTO user) {
		this.user = user;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public int getLikecount() {
		return likecount;
	}
	public void setLikecount(int likecount) {
		this.likecount = likecount;
	}
	public int getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}
}
