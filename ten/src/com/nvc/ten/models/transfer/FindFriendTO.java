package com.nvc.ten.models.transfer;

import com.nvc.ten.models.extended.UserObject;

public class FindFriendTO {
	private Integer friended;
	private String image;
	private String username;
	private Integer userid;
	private boolean isPrivate;
	//TODO FindFriendTO and User ar saem
	public FindFriendTO(UserObject uo){
		this.image = uo.getImage();
		this.username = uo.getUsername();
		this.userid = uo.getIdUser();
		this.isPrivate = uo.isPrivate();
	}
	
	public Integer getFriended() {
		return friended;
	}
	public void setFriended(Integer friended) {
		this.friended = friended;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public boolean isPrivate() {
		return isPrivate;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
}
