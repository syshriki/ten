package com.nvc.ten.models.transfer;

import java.util.List;

import com.nvc.ten.models.HashtagModel;

public class FindHashtagTO {
	private List<HashtagModel> hashtags;

	public List<HashtagModel> getHashtags() {
		return hashtags;
	}

	public void setHashtags(List<HashtagModel> hashtags) {
		this.hashtags = hashtags;
	}
}
