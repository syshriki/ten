package com.nvc.ten.models.transfer;

import com.nvc.ten.models.extended.HashtagObject;

public class NewsfeedHashtagTO {
	private Integer hashtagid;
	private String hashtag;
	
	public NewsfeedHashtagTO(HashtagObject upho) {
		this.hashtagid = upho.getHashtagid();
		//this.hashtag = upho.getHashtag().getHash();
	}
	public Integer getHashtagid() {
		return hashtagid;
	}
	public void setHashtagid(Integer hashtagid) {
		this.hashtagid = hashtagid;
	}
	public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
}
