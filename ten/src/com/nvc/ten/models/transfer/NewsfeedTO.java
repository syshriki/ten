package com.nvc.ten.models.transfer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.nvc.ten.models.extended.NewsfeedObject;
import com.nvc.ten.models.extended.PostObject;

public class NewsfeedTO {
	private List<PostTO> posts;

	public NewsfeedTO(NewsfeedObject no){
		this.posts = this.extractPosts(no);
	}
	public List<PostTO> getPosts() {
		return posts;
	}
	public void setPosts(List<PostTO> posts) {
		this.posts = posts;
	}
	public List<PostTO> extractPosts(NewsfeedObject no){
		List<PostTO> p = new ArrayList<>();
		for(PostObject upo : no.getNewsfeed()){
			p.add(new PostTO(upo));
		}
		return p;
	}
}
