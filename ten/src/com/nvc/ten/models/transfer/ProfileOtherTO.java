package com.nvc.ten.models.transfer;

import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.models.extended.ProfileObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.PostObject;

public class ProfileOtherTO {
	private Integer followerCount;
	private Integer followingCount;
	private Integer postCount;
	private Integer topTime;
	private String userimage;
	private String userdescription;
	private Integer userid;
	private String username;
	private Integer blocked;
	private Integer friended;
	private List<ProfileLikerTO> likers;
	private List<PostTO> posts;
	private Integer isPrivate;

	public ProfileOtherTO(ProfileObject po) {
		this.followerCount = po.getFollowers();
		this.followingCount = po.getFollowing();
		this.postCount = po.getPostCount();
		this.topTime = po.getTopTime();
		this.userdescription = po.getUser().getDescription();
		this.userid = po.getUser().getIdUser();
		this.username = po.getUser().getUsername();
		this.likers = this.extractLikers(po);
		this.userimage = po.getUser().getImage();
		this.friended = po.getFriended();
		this.setBlocked(po.getBlocked());
		this.isPrivate = po.getUser().isPrivate() == true ? 1 : 0;
		posts = new ArrayList<>();
		for (PostObject upo : po.getUserposts()) {
			posts.add(new PostTO(upo));
		}
	}

	public Integer getFollowerCount() {
		return followerCount;
	}

	public void setFollowerCount(Integer followerCount) {
		this.followerCount = followerCount;
	}

	public Integer getFollowingCount() {
		return followingCount;
	}

	public void setFollowingCount(Integer followingCount) {
		this.followingCount = followingCount;
	}

	public Integer getPostCount() {
		return postCount;
	}

	public void setPostCount(Integer postCount) {
		this.postCount = postCount;
	}

	public Integer getTopTime() {
		return topTime;
	}

	public void setTopTime(Integer topTime) {
		this.topTime = topTime;
	}

	public String getUserimage() {
		return userimage;
	}

	public void setUserimage(String userimage) {
		this.userimage = userimage;
	}

	public String getUserdescription() {
		return userdescription;
	}

	public void setUserdescription(String userdescription) {
		this.userdescription = userdescription;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<ProfileLikerTO> extractLikers(ProfileObject po) {
		List<ProfileLikerTO> likers = new ArrayList<>();
		for (UserObject uo : po.getTopLikers()) {
			if (uo.getIdUser() > 0) {
				likers.add(new ProfileLikerTO(uo));
			}
		}
		return likers;
	}

	public List<ProfileLikerTO> getLikers() {
		return likers;
	}

	public void setLikers(List<ProfileLikerTO> likers) {
		this.likers = likers;
	}

	public Integer getFriended() {
		return friended;
	}

	public void setFriended(Integer friended) {
		this.friended = friended;
	}

	public Integer getBlocked() {
		return blocked;
	}

	public void setBlocked(Integer blocked) {
		this.blocked = blocked;
	}

	public Integer getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(Integer isPrivate) {
		this.isPrivate = isPrivate;
	}

	public List<PostTO> getPosts() {
		return posts;
	}

	public void setPosts(List<PostTO> posts) {
		this.posts = posts;
	}
}
