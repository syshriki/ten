package com.nvc.ten.models.transfer;

import com.nvc.ten.models.extended.ProfileBestTimeObject;
import com.nvc.ten.models.extended.PostObject;

public class BestTimeTO {
	private int bestTime;
	private PostTO post;
	
	public BestTimeTO(ProfileBestTimeObject pbto){
		if(pbto.getPost()!=null){
			this.post = new PostTO(new PostObject(pbto.getPost()));
			this.setBestTime(pbto.getTime());
		}else{
			bestTime = 0;
		}
	}
	public PostTO getPost() {
		return post;
	}

	public void setPost(PostTO post) {
		this.post = post;
	}
	public int getBestTime() {
		return bestTime;
	}
	public void setBestTime(int bestTime) {
		this.bestTime = bestTime;
	}
	
}
