package com.nvc.ten.models.transfer;

import com.nvc.ten.models.extended.UserObject;

public class ProfileLikerTO {
	private Integer userid;
	private String image;
	private String username;
	
	public ProfileLikerTO(UserObject uo){
		this.image = uo.getImage();
		this.userid = uo.getIdUser();
		this.username = uo.getUsername();
	}
	
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
