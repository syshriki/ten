package com.nvc.ten.models.transfer;

import java.io.IOException;
import java.util.Date;

import com.nvc.ten.models.extended.CommentObject;
import com.nvc.ten.models.extended.UserObject;

public class CommentTO {

	private Integer userid;
	private String username;
	private String userimage;
	private String text;
	private Integer commentid;
	private Date datestamp;

	public CommentTO(CommentObject c) {
		this.userid = c.getIdUser();
		this.username = c.getUser().getUsername();
		this.text = c.getComment();
		this.commentid = c.getIdUserComment();
		this.datestamp = c.getDatestamp();
		try {
			this.userimage = new UserObject(c.getUser()).getImage();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getCommentid() {
		return commentid;
	}

	public void setCommentid(Integer commentid) {
		this.commentid = commentid;
	}

	public Date getDatestamp() {
		return datestamp;
	}

	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}

	public String getUserimage() {
		return userimage;
	}

	public void setUserimage(String userimage) {
		this.userimage = userimage;
	}
}
