package com.nvc.ten.models.transfer;

import com.nvc.ten.models.extended.UserObject;

public class UserTO {
	private String username;
	private Integer iduser;
	private String imageSrc;
	private boolean isPrivate;
	public UserTO(UserObject uo) {
		this.username = uo.getUsername();
		this.iduser = uo.getIdUser();
		this.imageSrc = uo.getImage();
		this.setPrivate(uo.isPrivate());
	}
	public UserTO() {
		// TODO Auto-generated constructor stub
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Integer getIdUser() {
		return iduser;
	}
	public void setIdUser(Integer iduser) {
		this.iduser = iduser;
	}
	public String getImageSrc() {
		return imageSrc;
	}
	public void setImageSrc(String image) {
		this.imageSrc = image;
	}
	public boolean isPrivate() {
		return isPrivate;
	}
	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

}
