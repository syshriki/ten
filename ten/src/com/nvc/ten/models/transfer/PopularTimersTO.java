package com.nvc.ten.models.transfer;

import java.util.List;

public class PopularTimersTO {
	private List<UserTO> users;

	public PopularTimersTO(List<UserTO> users) {
		this.users = users;
	}
	
	public Object getPopulars() {
		return users;
	}

	public void setPopulars(List<UserTO> users) {
		this.users = users;
	}
}
