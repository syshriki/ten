package com.nvc.ten.models.transfer;

import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.utilities.TenUtilities;

public class LoginTO {
	private int userid;
	private String username;
	private String password;
	private String image;
	private String description;
	private Integer isprivate;
	
	public LoginTO(UserObject u){
		this.userid = u.getIdUser();
		this.username = u.getUsername();
		this.password = this.toBase64(u.getPassword());
		this.image = u.getImage();
		this.isprivate = u.isPrivate() ? 1 : 0;
		this.setDescription(u.getDescription());
	}
	
	private String toBase64(byte[] data)
	{
		TenUtilities globalutils = new TenUtilities();
		return globalutils.encodeToBase64String(data);
	}
	
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public Integer getIsprivate() {
		return isprivate;
	}

	public void setIsprivate(Integer isprivate) {
		this.isprivate = isprivate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
