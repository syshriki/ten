package com.nvc.ten.models;


public class UnreadNotificationModel extends ModelBase{
	private int idUnreadNotification;
	private String notificationType;
	private int idpost;
	private boolean read;
	private String leftimagesrc;
	private String rightimagesrc;
	private int affectingUser;
	private int affectedUser;
	

	public int getAffectingUser() {
		return affectingUser;
	}
	public void setAffectingUser(int affectingUser) {
		this.affectingUser = affectingUser;
	}
	public int getAffectedUser() {
		return affectedUser;
	}
	public void setAffectedUser(int affectedUser) {
		this.affectedUser = affectedUser;
	}
	public int getIdUnreadNotification() {
		return idUnreadNotification;
	}
	public void setIdUnreadNotification(int idUnreadNotifications) {
		this.idUnreadNotification = idUnreadNotifications;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public int getIdpost() {
		return idpost;
	}
	public void setIdpost(int idpost) {
		this.idpost = idpost;
	}
	public boolean isRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public String getLeftimagesrc() {
		return leftimagesrc;
	}
	public void setLeftimagesrc(String leftimagesrc) {
		this.leftimagesrc = leftimagesrc;
	}
	public String getRightimagesrc() {
		return rightimagesrc;
	}
	public void setRightimagesrc(String rightimagesrc) {
		this.rightimagesrc = rightimagesrc;
	}

}
