package com.nvc.ten.models;


public class RepostModel extends ModelBase{
	private int idUserRepost;
	private int idUser;
	private int idUserPost;
	public int getIdUserRepost() {
		return idUserRepost;
	}
	public void setIdUserRepost(int idUserRepost) {
		this.idUserRepost = idUserRepost;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdUserPost() {
		return idUserPost;
	}
	public void setIdUserPost(int idUserPost) {
		this.idUserPost = idUserPost;
	}
}
