package com.nvc.ten.models;


public class FollowersModel extends ModelBase{
	private int idPair;
	private int idUserMaster;
	private int idUserSlave;
	private int approved = 0;
	public int getIdPair() {
		return idPair;
	}
	public void setIdPair(int idPair) {
		this.idPair = idPair;
	}
	public int getIdUserMaster() {
		return idUserMaster;
	}
	public void setIdUserMaster(int idUserMaster) {
		this.idUserMaster = idUserMaster;
	}
	public int getIdUserSlave() {
		return idUserSlave;
	}
	public void setIdUserSlave(int idUserSlave) {
		this.idUserSlave = idUserSlave;
	}
	public int getApproved() {
		return approved;
	}
	public void setApproved(int approved) {
		this.approved = approved;
	}
}
