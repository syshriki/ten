package com.nvc.ten.models;

import java.sql.Timestamp;


public abstract class ModelBase {
	private Timestamp datestamp;
	
	public Timestamp getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Timestamp datestamp) {
		this.datestamp = datestamp;
	}

}
