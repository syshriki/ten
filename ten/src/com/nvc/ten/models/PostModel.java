package com.nvc.ten.models;

import java.sql.Timestamp;


public class PostModel extends ModelBase{
	private int idUserPost = 0;
	private String text;
	private int idUser;
	private int idUserPostRepost = 0;
	private Timestamp expiration;
	private Integer reposted;
	
	public int getIdUserPostRepost() {
		return idUserPostRepost;
	}
	public void setIdUserPostRepost(int idUserPostRepost) {
		this.idUserPostRepost = idUserPostRepost;
	}
	public int getIdUserPost() {
		return idUserPost;
	}
	public void setIdUserPost(int idUserPost) {
		this.idUserPost = idUserPost;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public Integer isReposted() {
		return reposted;
	}
	public void setReposted(Integer reposted) {
		this.reposted = reposted;
	}
	public Timestamp getExpiration() {
		return expiration;
	}
	public void setExpiration(Timestamp expiration) {
		this.expiration = expiration;
	}
}
