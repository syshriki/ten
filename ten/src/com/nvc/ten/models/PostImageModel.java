package com.nvc.ten.models;


public class PostImageModel extends ModelBase{
	private int idUserPostImage;
	private String imageSrc;
	private int idUserPost;
	public int getIdUserPostImage() {
		return idUserPostImage;
	}
	public void setIdUserPostImage(int idUserPostImage) {
		this.idUserPostImage = idUserPostImage;
	}
	public String getImageSrc() {
		return imageSrc;
	}
	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}
	public int getIdUserPost() {
		return idUserPost;
	}
	public void setIdUserPost(int idUserPost) {
		this.idUserPost = idUserPost;
	}
}
