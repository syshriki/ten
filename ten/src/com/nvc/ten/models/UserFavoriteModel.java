package com.nvc.ten.models;


public class UserFavoriteModel extends ModelBase{
	private int idUserFavorites;
	private int idUserPost;
	private int idUser;
	public int getIdUserFavorites() {
		return idUserFavorites;
	}
	public void setIdUserFavorites(int idUserFavorites) {
		this.idUserFavorites = idUserFavorites;
	}
	public int getIdUserPost() {
		return idUserPost;
	}
	public void setIdUserPost(int idUserPost) {
		this.idUserPost = idUserPost;
	}
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
}
