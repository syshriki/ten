package com.nvc.ten.models.extended;

public class PopularTimerObject{
	private UserObject user; 
	private int score;
	
	public UserObject getUser() {
		return user;
	}
	public void setUser(UserObject user) {
		this.user = user;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
