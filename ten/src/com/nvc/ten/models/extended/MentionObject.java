package com.nvc.ten.models.extended;

import com.nvc.ten.models.MentionModel;

public class MentionObject extends MentionModel{
	private UserObject mentioned;
	private int mentionerid;
	private int userpostid;

	public UserObject getMentioned() {
		return mentioned;
	}
	public void setMentioned(UserObject mentioned) {
		this.mentioned = mentioned;
	}
	public int getMentionerId() {
		return mentionerid;
	}
	public void setMentionerId(int mentionerid) {
		this.mentionerid = mentionerid;
	}
	public int getUserpostid() {
		return userpostid;
	}
	public void setUserpostid(int userpostid) {
		this.userpostid = userpostid;
	}
}
