package com.nvc.ten.models.extended;

import com.nvc.ten.models.PostModel;

public class ProfileBestTimeObject {
	private int time;
	private PostModel post;
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public PostModel getPost() {
		return post;
	}
	public void setPost(PostModel post) {
		this.post = post;
	}
}
