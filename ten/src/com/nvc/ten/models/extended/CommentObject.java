package com.nvc.ten.models.extended;

import com.nvc.ten.models.CommentModel;
import com.nvc.ten.models.UserModel;

public class CommentObject extends CommentModel implements Comparable<CommentObject>{
	
	private UserModel user;
	
	public CommentObject(){
		super();
	}
	
	public CommentObject(CommentModel comment){
		super();
		this.setComment(comment.getComment());
		this.setDatestamp(comment.getDatestamp());
		this.setIdUser(comment.getIdUser());
		this.setIdUserComment(comment.getIdUserComment());
		this.setIdUserPost(comment.getIdUser());
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getIdUserComment();
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentModel other = (CommentModel) obj;
		if (this.getIdUserComment() != other.getIdUserComment())
			return false;
		return true;
	}
	
	@Override
	public int compareTo(CommentObject o) {
		return (o.equals(this) ? 0 : -1);
	}

	public UserModel getUser() {
		return user;
	}

	public void setUser(UserModel user) {
		this.user = user;
	}

}
