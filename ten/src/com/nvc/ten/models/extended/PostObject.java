package com.nvc.ten.models.extended;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.nvc.ten.models.PostModel;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.utilities.TenUtilities;


public class PostObject extends PostModel{
	private Set<LikeObject> likes;
	private Set<HashtagObject> hashtags;
	private Set<ImageObject> images;
	private Set<CommentObject> comments;
	private Set<MentionObject> mentions;
	private UserObject user;
	private UserObject reposter;
	//private UserDTO user;
	
	public void loadImageData() throws IOException {
		TenUtilities globalutils = new TenUtilities();
		for(ImageObject image : this.getImages()){
			if(image.getImageSrc()!=null){
				String imagePath;
				//Hopefully report should work now
				//if(this.getIdUserPostRepost()>0){
				//	imagePath = BaseServlet.PHOTO_STORE_PATH+image.getImageSrc();
				//}else{
					imagePath = BaseServlet.PHOTO_STORE_PATH+this.getIdUser()+"/"+image.getImageSrc();
				//}
				image.setImage(globalutils.encodeToBase64String(globalutils.extractBytes(imagePath)));
			}
		}
	}
	public PostObject(){
		super();
		this.likes = new HashSet<LikeObject>();
		this.hashtags = new HashSet<HashtagObject>();
		this.images = new HashSet<ImageObject>();
		this.comments = new HashSet<CommentObject>();
		this.mentions = new HashSet<MentionObject>();
	}
	public UserObject getUser() {
		return user;
	}
	public void setUser(UserObject user) {
		this.user = user;
	}
	public PostObject(PostModel post){
		super();
		this.likes = new HashSet<LikeObject>();
		this.hashtags = new HashSet<HashtagObject>();
		this.images = new HashSet<ImageObject>();
		this.comments = new HashSet<CommentObject>();
		this.mentions = new HashSet<MentionObject>();
		
		this.setDatestamp(post.getDatestamp());
		this.setIdUser(post.getIdUser());
		this.setIdUserPost(post.getIdUserPost());
		this.setText(post.getText());
	}
	
	public Set<LikeObject> getLikes() {
		return likes;
	}
	public void setLikes(Set<LikeObject> likes) {
		this.likes = likes;
	}
	public Set<HashtagObject> getUserPostHashtags() {
		return hashtags;
	}
	public void setUserPostHashtags(Set<HashtagObject> userposthashtags) {
		this.hashtags = userposthashtags;
	}
	public Set<ImageObject> getImages() {
		return images;
	}
	public void setImages(Set<ImageObject> images) {
		this.images = images;
	}
	public Set<CommentObject> getComments() {
		return comments;
	}
	public void setComments(Set<CommentObject> comments) {
		this.comments = comments;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getIdUserPost();
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostObject other = (PostObject) obj;
		if (this.getIdUserPost() != other.getIdUserPost())
			return false;
		return true;
	}
	public void addHashtag(HashtagObject hashtag) {
		this.hashtags.add(hashtag);
		
	}
	public void addLike(LikeObject like) {
		this.likes.add(like);
		
	}
	public void addImage(ImageObject image) {
		this.images.add(image);
		
	}
	public void addComment(CommentObject comment) {
		this.comments.add(comment);
		
	}
	
	public int compareTo(PostObject o) {
		return (o.equals(this) ? 0 : -1);
	}

	public Set<MentionObject> getMentions() {
		return mentions;
	}

	public void setMentions(Set<MentionObject> mentions) {
		this.mentions = mentions;
	}
	public UserObject getReposter() {
		return reposter;
	}
	public void setReposter(UserObject reposter) {
		this.reposter = reposter;
	}
}
