

package com.nvc.ten.models.extended;

import java.util.List;

public class ProfileObject {
	private UserObject user;
	private int followers;
	private int following;
	private int postCount;
	private int topTime;
	private int blocked;
	private Integer friended;
	private List<UserObject> topLikers;
	private List<PostObject> userposts;
	
	public UserObject getUser() {
		return user;
	}
	public void setUser(UserObject user) {
		this.user = user;
	}
	public int getFollowers() {
		return followers;
	}
	public void setFollowers(int followers) {
		this.followers = followers;
	}
	public int getFollowing() {
		return following;
	}
	public void setFollowing(int following) {
		this.following = following;
	}
	public int getPostCount() {
		return postCount;
	}
	public void setPostCount(int post_count) {
		this.postCount = post_count;
	}
	public int getTopTime() {
		return topTime;
	}
	public void setTopTime(int topTime) {
		this.topTime = topTime;
	}
	public List<UserObject> getTopLikers() {
		return topLikers;
	}
	public void setTopLikers(List<UserObject> topLikers) {
		this.topLikers = topLikers;
	}
	public List<PostObject> getUserposts() {
		return userposts;
	}
	public void setUserposts(List<PostObject> userposts) {
		this.userposts = userposts;
	}
	public Integer getFriended() {
		return friended;
	}
	public void setFriended(Integer friended) {
		this.friended = friended;
	}
	public int getBlocked() {
		return blocked;
	}
	public void setBlocked(int blocked) {
		this.blocked = blocked;
	}
}
