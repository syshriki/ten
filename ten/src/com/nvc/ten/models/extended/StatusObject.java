package com.nvc.ten.models.extended;

import java.sql.Timestamp;
import java.util.Date;

import com.nvc.ten.models.UserModel;

public class StatusObject {
	private int idpost;
	private Date datestamp;
	private PostObject post;
	private int iduser;
	private String username;
	private String notificationType;
	private int notificationId;
	private Date postdate;
	private int likecount;
	private int affectingUser;
	
	public int getIdpost() {
		return idpost;
	}
	public void setIdpost(int idpost) {
		this.idpost = idpost;
	}
	public Date getDatestamp() {
		return datestamp;
	}
	public void setDatestamp(Date datestamp) {
		this.datestamp = datestamp;
	}
	public PostObject getPost() {
		return post;
	}
	public void setPost(PostObject post) {
		this.post = post;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getIduser() {
		return iduser;
	}
	public void setIduser(int iduser) {
		this.iduser = iduser;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public Date getPostdate() {
		return postdate;
	}
	public void setPostdate(Timestamp postdate) {
		this.postdate = postdate;
	}
	public int getLikecount() {
		return likecount;
	}
	public void setLikecount(int likecount) {
		this.likecount = likecount;
	}
	public int getNotificationId() {
		return notificationId;
	}
	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}
	public int getAffectingUser() {
		return affectingUser;
	}
	public void setAffectingUser(int affectingUser) {
		this.affectingUser = affectingUser;
	}
}
