package com.nvc.ten.models.extended;

import com.nvc.ten.models.HashtagModel;
import com.nvc.ten.models.PostHashtagModel;

public class HashtagObject extends PostHashtagModel implements Comparable<HashtagObject>{
	
	private HashtagModel hashtag;
	
	public HashtagObject(){
		super();
	}
	
	public HashtagObject(PostHashtagModel hashtag){
		super();
		this.setDatestamp(hashtag.getDatestamp());
		this.setHashtagid(hashtag.getHashtagid());
		this.setUserPostHashtagid(hashtag.getUserPostHashtagid());
		this.setUserPostid(hashtag.getUserPostid());
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getUserPostHashtagid();
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostHashtagModel other = (PostHashtagModel) obj;
		if (this.getUserPostHashtagid() != other.getUserPostHashtagid())
			return false;
		return true;
	}

	@Override
	public int compareTo(HashtagObject o) {
		return (o.equals(this) ? 0 : -1);
	}

	public HashtagModel getHashtag() {
		return hashtag;
	}

	public void setHashtag(HashtagModel hashtag) {
		this.hashtag = hashtag;
	}
	
	
}
