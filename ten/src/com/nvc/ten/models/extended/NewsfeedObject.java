package com.nvc.ten.models.extended;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.nvc.utilities.TenUtilities;

public class NewsfeedObject {
	private List<PostObject> newsfeed = new ArrayList<PostObject>();

	public List<PostObject> getNewsfeed() {
		return newsfeed;
	}

	public void setNewsfeed(List<PostObject> newsfeed) {
		this.newsfeed = newsfeed;
	}
	
	public void add(PostObject userpost) {
		for (int i = 0; i < newsfeed.size(); i++) {
			if (userpost.equals(newsfeed.get(i))) {
				try {
					newsfeed.set(i,mergeUserPostElements(newsfeed.get(i),userpost));
				} catch (InstantiationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
			}
		}
		newsfeed.add(userpost);
	}
	//TODO FIX REPOST ID TO FOREIGN KEY
	public PostObject mergeUserPostElements(PostObject post1, PostObject post2) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		PostObject mergedUserPost = new PostObject();
		
		Set<LikeObject> post1Likes = post1.getLikes();
		Set<LikeObject> post2Likes = post2.getLikes();
		
		Set<CommentObject> post1Comments = post1.getComments();
		Set<CommentObject> post2Comments = post2.getComments();
		
		Set<HashtagObject> post1Hashtags = post1.getUserPostHashtags();
		Set<HashtagObject> post2Hashtags = post2.getUserPostHashtags();
		
		Set<ImageObject> post1Images = post1.getImages();
		Set<ImageObject> post2Images = post2.getImages();
		
		mergedUserPost.setIdUser(post1.getUser().getIdUser());
		mergedUserPost.setDatestamp(post1.getDatestamp());
		mergedUserPost.setIdUserPost(post1.getIdUserPost());
		mergedUserPost.setIdUserPostRepost(post1.getIdUserPostRepost());
		mergedUserPost.setText(post1.getText());
		mergedUserPost.setUser(post1.getUser());

		mergedUserPost.setLikes(TenUtilities.union(post1Likes, post2Likes));
		mergedUserPost.setComments(TenUtilities.union(post1Comments, post2Comments));
		mergedUserPost.setUserPostHashtags(TenUtilities.union(post1Hashtags, post2Hashtags));
		mergedUserPost.setImages(TenUtilities.union(post1Images, post2Images));
		
		mergedUserPost.setReposted(post1.isReposted());//post1,post2 it shouldn't matter

		return mergedUserPost;
	}

}
