package com.nvc.ten.models.extended;

import java.sql.Timestamp;
import java.util.Date;

import com.nvc.ten.models.UnreadNotificationModel;

public class NotificationObject extends UnreadNotificationModel{
	private PostObject post;
	private int iduser;
	private String username;
	private Date postdate;
	private int likecount;
	
	public PostObject getPost() {
		return post;
	}
	public void setPost(PostObject post) {
		this.post = post;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getIduser() {
		return iduser;
	}
	public void setIduser(int iduser) {
		this.iduser = iduser;
	}
	public Date getPostdate() {
		return postdate;
	}
	public void setPostdate(Timestamp postdate) {
		this.postdate = postdate;
	}
	public int getLikecount() {
		return likecount;
	}
	public void setLikecount(int likecount) {
		this.likecount = likecount;
	}
}
