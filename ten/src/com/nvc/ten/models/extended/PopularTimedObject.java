package com.nvc.ten.models.extended;

public class PopularTimedObject{
	private PostObject userpost; 
	private int score;
	
	public PostObject getUserpost() {
		return userpost;
	}
	public void setUserpost(PostObject userpost) {
		this.userpost = userpost;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
}
