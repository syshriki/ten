package com.nvc.ten.models.extended;

import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.LikeModel;

public class LikeObject extends LikeModel implements Comparable<LikeObject>{
	private UserObject userobject;
	
	public UserModel getUserdto() {
		return userobject;
	}
	public void setUserObject(UserObject userobject) {
		this.userobject = userobject;
	}
	public LikeObject(){
		super();
	}
	
	public LikeObject(int id){
		super();
		this.setIdUserPostLike(id);
	}
	
	public LikeObject(LikeModel like){
		super();
		this.setDatestamp(like.getDatestamp());
		this.setIdUser(like.getIdUser());
		this.setIdUserPost(like.getIdUserPost());
		this.setIdUserPostLike(like.getIdUserPostLike());
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getIdUserPostLike();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LikeModel other = (LikeModel) obj;
		if (this.getIdUserPostLike() != other.getIdUserPostLike())
			return false;
		return true;
	}

	@Override
	public int compareTo(LikeObject o) {
		return o.equals(this) ?  0 : -1;
	}
}
