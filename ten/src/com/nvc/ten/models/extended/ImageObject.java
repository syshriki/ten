package com.nvc.ten.models.extended;

import com.nvc.ten.models.PostImageModel;

public class ImageObject extends PostImageModel implements Comparable<ImageObject>{
	//This gets converted from byte data to a string since JSON objects cannot contain byte data
	private String image;
	
	public ImageObject(){
		super();
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public ImageObject(PostImageModel image){
		super();
		this.setDatestamp(image.getDatestamp());
		this.setIdUserPost(image.getIdUserPost());
		this.setIdUserPostImage(image.getIdUserPostImage());
		this.setImageSrc(image.getImageSrc());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.getIdUserPostImage();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostImageModel other = (PostImageModel) obj;
		if (this.getIdUserPostImage() != other.getIdUserPostImage())
			return false;
		return true;
	}

	@Override
	public int compareTo(ImageObject o) {
		return (o.equals(this) ?  0 : -1);
	}
}
