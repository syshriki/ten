package com.nvc.ten.models.extended;

import java.io.IOException;

import com.nvc.ten.models.UserModel;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.utilities.TenUtilities;

public class UserObject extends UserModel{
	private String image;

	public UserObject(UserModel user) throws IOException{
		super();
		this.setDatestamp(user.getDatestamp());
		this.setDescription(user.getDescription());
		this.setEmail(user.getEmail());
		this.setImagesrc(user.getImagesrc());
		this.setPassword(user.getPassword());
		this.setPrivate(user.isPrivate());
		this.setUsername(user.getUsername());
		this.setWebsite(user.getWebsite());
		this.setIdUser(user.getIdUser());
		TenUtilities globalutils = new TenUtilities();
		
		if(this.getImagesrc()!=null && this.getImagesrc()!=""){
			String imagePath = BaseServlet.PHOTO_STORE_PATH+this.getIdUser()+"/"+this.getImagesrc();
			this.setImage(globalutils.encodeToBase64String(globalutils.extractBytes(imagePath)));
		}
	}
	
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
}
