package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.dao.UnreadNotificationDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.models.extended.NotificationObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.NotificationTO;
import com.nvc.ten.models.transfer.PostTO;
import com.nvc.ten.models.transfer.StatusTO;
import com.nvc.ten.models.transfer.UserTO;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.utilities.TenUtilities;

public class NotificationService {
	private UnreadNotificationDAO unreadnotificatinodao = new UnreadNotificationDAO();

	public Integer getUnreadNotificationCount(int userid){
		try {
			return unreadnotificatinodao.countUnreadNotifications(userid);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public List<NotificationTO> getUnreadNotifications(int iduser,int offset, int count)
			throws ClassNotFoundException, SQLException, IOException {
		List<NotificationObject> notificationObjects = new ArrayList<>();
		List<NotificationTO> notifications = new ArrayList<>();
		UserDAO userdao = new UserDAO();
		UserPostDAO postdao = new UserPostDAO();
		notificationObjects = unreadnotificatinodao
				.findUnreadNotifications(iduser,offset,count);
		for (NotificationObject notificationObject : notificationObjects) {
			NotificationTO notification = new NotificationTO();
			notification.setDatestamp(notificationObject.getDatestamp());
			System.out.println("ID:"+notificationObject.getIdUnreadNotification() +"     TYPE:"+notificationObject.getNotificationType());
			System.out.println("POSTID:"+notificationObject.getIdpost());	
			if(notificationObject.getIdpost()>0){
				notification.setPost(new PostTO(postdao.getPostObjectByID(notificationObject.getIdpost(), notificationObject.getIduser())));
			}
			notification.setNotificationType(notificationObject.getNotificationType());
			notification.setLikecount(notificationObject.getLikecount());
			
			//If user id is valid set notification user
			if(notificationObject.getIduser()>0)
				notification.setUser(new UserTO(new UserObject(userdao.findByID(notificationObject.getIduser()))));
			
			//If notification is not read, read it
			if(!notificationObject.isRead())
				this.readNotification(notificationObject.getIdUnreadNotification());
			
			notification.setNotificationId(notificationObject.getIdUnreadNotification());
			notifications.add(notification);
		}
		System.out.println(iduser + " pulled notifications");
		return notifications;
	}

	public void readNotification(int idnotification)
			throws ClassNotFoundException, SQLException {
		UnreadNotificationDAO unreadnotificatinodao = new UnreadNotificationDAO();
		unreadnotificatinodao.readNotification(idnotification);
	}
}
