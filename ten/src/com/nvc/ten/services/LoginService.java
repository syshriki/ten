package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;

import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserSaltDAO;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.UserSalt;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.throwable.service.NullAuthenticationException;
import com.nvc.ten.throwable.service.NullReturned;
import com.nvc.ten.throwable.service.SaltNotFoundException;
import com.nvc.utilities.PasswordProtectionUtility;

public class LoginService {
	public UserObject login(String username, String password)
			throws NullAuthenticationException, SaltNotFoundException,
			ClassNotFoundException, SQLException, NullReturned, IOException {
		UserDAO userdao = new UserDAO();
		PasswordProtectionUtility ppu = new PasswordProtectionUtility();
		boolean validPass = false;
		UserModel userDTO = userdao.findByUsername(username);
		if (userDTO == null)
			return null;

		UserSaltDAO usersaltdao = new UserSaltDAO();
		byte[] salt;
		UserSalt usersalt = usersaltdao.findByID(userDTO.getIdUser());
		if (usersalt == null)
			throw new SaltNotFoundException();
		salt = usersalt.getSalt();
		if (salt == null)
			throw new SaltNotFoundException();
		validPass = ppu.isExpectedPassword(password.getBytes(), salt,
				userDTO.getPassword());
		if (validPass) {// If password is correct
			System.out.println(username+ " successfully logged in");
			return new UserObject(userDTO);
		} else{
			System.out.println(username + " FAILED authentication");
			return null;
		}
	}

	public UserObject login(int userid, String password) throws NullReturned,
			ClassNotFoundException, SQLException, SaltNotFoundException,
			IOException, NullAuthenticationException {
		UserDAO userdao = new UserDAO();
		PasswordProtectionUtility ppu = new PasswordProtectionUtility();
		boolean validPass = false;
		UserModel userDTO = userdao.findByID(userid);
		if (userDTO == null)
			return null;

		UserSaltDAO usersaltdao = new UserSaltDAO();
		byte[] salt;
		UserSalt usersalt = usersaltdao.findByID(userDTO.getIdUser());
		if (usersalt == null)
			throw new SaltNotFoundException();
		salt = usersalt.getSalt();
		if (salt == null)
			throw new SaltNotFoundException();
		validPass = ppu.isExpectedPassword(password.getBytes(), salt,
				userDTO.getPassword());
		if (validPass) {// If password is correct
			System.out.println(userid+ " successfully authenticated");;
			return new UserObject(userDTO);
		} else
			return null;
	}
	
	public boolean isValidLogin(int userid, String password) {
		try {
			return this.login(userid, password)!=null;
		} catch (ClassNotFoundException | NullReturned | SQLException
				| SaltNotFoundException | IOException
				| NullAuthenticationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
