package com.nvc.ten.services;

import java.sql.SQLException;
import java.util.List;

import com.nvc.ten.dao.HashtagDAO;
import com.nvc.ten.models.HashtagModel;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.InvalidReturnCount;

public class FindHashService {
	public List<HashtagModel> findHashtag(String hashtag, int offset,int count) throws ClassNotFoundException, SQLException, InvalidReturnCount{
		if(offset<0 || count<0 || count > BaseServlet.MAX_FRIEND_SEARCH_RETURN_COUNT)
			throw new InvalidReturnCount();
		HashtagDAO hashtagdao = new HashtagDAO();
		return hashtagdao.findLikeHash(hashtag, offset, count);
	}
}
