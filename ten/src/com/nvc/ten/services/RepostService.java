package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.dao.UserPostHashtagDAO;
import com.nvc.ten.dao.UserPostImageDAO;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.PostHashtagModel;
import com.nvc.ten.models.PostImageModel;
import com.nvc.ten.models.extended.HashtagObject;
import com.nvc.ten.models.extended.ImageObject;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.ten.throwable.service.NotFollowingPrivateException;
import com.nvc.utilities.TenUtilities;

public class RepostService {
	public int repost(int idpost,int iduser) throws ClassNotFoundException, SQLException, IOException, NotFollowingPrivateException{
		TenUtilities globablutils = new TenUtilities();
		UserPostDAO postdao = new UserPostDAO();
		PostObject post = postdao.getPostObjectByID(idpost,iduser);
		PostModel postDTO = (PostModel)post;
		UserPostImageDAO imagedao = new UserPostImageDAO();
		UserPostHashtagDAO hashtagdao = new UserPostHashtagDAO();
		
		int originalUser = postDTO.getIdUser();
		
		if(!globablutils.isFollowingUser(iduser, originalUser)){
			if(globablutils.postIsPrivate(postDTO.getIdUserPost()))
				throw new NotFollowingPrivateException();
		}
		
		//So user cannot repost own post
		if(postDTO.getIdUser()==iduser){
			System.out.println(iduser + " cannot repost own post " + idpost);
			return 0;
		}
		
		if(postDTO.getIdUserPostRepost()>0){
			post = postdao.getPostObjectByID(postDTO.getIdUserPostRepost(),iduser);
			postDTO = (PostModel)post;
		}
			
		postDTO.setIdUserPostRepost(postDTO.getIdUserPost());
		postDTO.setIdUser(iduser);
		postDTO.setDatestamp(TenUtilities.getCurrentDatestamp());
		
		int newpostid = postdao.insert(postDTO);
		
		System.out.println(iduser + " reposted " + idpost);
		
		return newpostid;
	}
}
