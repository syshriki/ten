package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.nvc.ten.dao.FollowersDAO;
import com.nvc.ten.dao.UserBlocksDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.models.FollowersModel;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.models.transfer.FindFriendsTO;
import com.nvc.ten.models.transfer.FollowersTO;
import com.nvc.ten.models.transfer.FollowingTO;
import com.nvc.ten.models.transfer.UserTO;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.NullReturned;
import com.nvc.utilities.TenUtilities;

public class FollowService {
	TenUtilities utils = new TenUtilities();
	// Master is the one recieving the follower
	public int followUser(int slaveid, int masterid) throws InvalidID,
			InsertFailed, ClassNotFoundException, SQLException {
		if (masterid <= 0 || slaveid <= 0 || slaveid == masterid)
			throw new InvalidID();
		FollowersDAO followersdao = new FollowersDAO();
		UserBlocksDAO userblocksdao = new UserBlocksDAO();
		TenUtilities globalutilities = new TenUtilities();
		UserDAO user = new UserDAO();
		FollowersModel userpair = new FollowersModel();
		int pairid = 0;
		int followingStatus = globalutilities.followingStatus(slaveid, masterid);
		if (followingStatus==1) {
			System.out.println(slaveid + " already following " + masterid);
			return 0;
		}
		if (userblocksdao.findByUserID(slaveid, masterid) == null) {
			userpair.setIdUserMaster(masterid);
			userpair.setIdUserSlave(slaveid);
			boolean isPrivate = user.findByID(masterid).isPrivate();
			if (!isPrivate) {
				userpair.setApproved(1);
			} else {
				userpair.setApproved(0);
			}
			userpair.setDatestamp(TenUtilities.getCurrentDatestamp());
			if(userpair!=null){
				TenUtilities utils = new TenUtilities();
				
				if(followingStatus==0){
					followersdao.deleteByID(followersdao.findFollowingID(userpair.getIdUserSlave(), userpair.getIdUserMaster()).getIdPair());
					System.out.println(slaveid + " NO LONGER following " + masterid);
					return -1;
				}
				else{
					pairid = followersdao.insert(userpair);
					if (pairid <= 0)
						throw new InsertFailed(this.getClass().getCanonicalName());
					else {
						pairid = 1;
						System.out.println(slaveid + " NOW following " + masterid);
					}
				}
			}
		}
		return pairid;
	}

	// Master is the on being unfollowed
	public Integer unfollowUser(int followingid, int userid) throws InvalidID,
			ClassNotFoundException, SQLException, NullReturned {
		if (followingid <= 0 || userid <= 0)
			throw new InvalidID();

		FollowersDAO followersdao = new FollowersDAO();
		FollowersModel follower = followersdao
				.findFollowingID(userid, followingid);
		if (follower != null)
			return followersdao.deleteByID(follower.getIdPair()) ? 1 : 0;
		return 0;
	}

	// Get all users following user with <id>
	public FollowersTO getFollowers(int iduser,int offset,int count) throws ClassNotFoundException,
			SQLException, InvalidID, IOException {
		if (iduser <= 0)
			throw new InvalidID();

		FollowersDAO followersdao = new FollowersDAO();
		Set<UserModel> userObjs = followersdao.findFollowers(iduser,offset,count);
		List<FindFriendTO> users = new ArrayList<>();

		for (UserModel u : userObjs) {
			FindFriendTO user = new FindFriendTO(new UserObject(u));
			// -1 = not friended, 0 = pending 1 = friended
			user.setFriended(utils.followingStatus(iduser, u.getIdUser()));
			users.add(user);
		}
		FollowersTO followersto = new FollowersTO();
		followersto.setUsers(users);
		return followersto;
	}

	public List<FindFriendTO> getFollowing(int iduser, int iduserMaster, int offset, int count) throws ClassNotFoundException,
			SQLException, InvalidID, IOException {
		if (iduser <= 0)
			throw new InvalidID();
		FollowersDAO followersdao = new FollowersDAO();
		Set<UserModel> userObjs = followersdao.findFollowing(iduser,offset,count);
		List<FindFriendTO> users = new ArrayList<>();
		for (UserModel u : userObjs) {
			FindFriendTO user = new FindFriendTO(new UserObject(u));
			// -1 = not friended, 0 = pending 1 = friended
			user.setFriended(utils.followingStatus(iduserMaster, u.getIdUser()));
			users.add(user);
		}
		return users;
	}

	public Integer approveFollower(int recipient, int requestee)
			throws ClassNotFoundException, SQLException, InvalidID, IOException {
		if (recipient <= 0 && requestee <= 0)
			throw new InvalidID();
		FollowersDAO followersdao = new FollowersDAO();
		FollowersModel followersModel = new FollowersModel();
		followersModel.setApproved(1);
		followersModel.setIdUserMaster(recipient);
		followersModel.setIdUserSlave(requestee);
		boolean approved = followersdao.approveFollower(followersModel);
		System.out.print(recipient+" approved follower "+requestee);
		return approved ? 1 : 0;
	}

	// Get all users following user with <id>
	public FindFriendsTO getPending(int userid,int offset,int count) throws ClassNotFoundException,
			SQLException, InvalidID, IOException {
		if (userid <= 0)
			throw new InvalidID();

		TenUtilities utils = new TenUtilities();
		FollowersDAO followersdao = new FollowersDAO();
		Set<UserModel> userObjs = followersdao.findPending(userid,offset,count);
		List<FindFriendTO> friendtos = new ArrayList<>();
		FindFriendsTO users = new FindFriendsTO();
		for (UserModel u : userObjs) {
			friendtos.add(new FindFriendTO(new UserObject(u)));
		}
		users.setUsers(friendtos);
		return users;
	}

	public Integer rejectFollower(int recipient, int requestee)
			throws ClassNotFoundException, SQLException, InvalidID, IOException {
		if (recipient <= 0 && requestee <= 0)
			throw new InvalidID();
		FollowersDAO followersdao = new FollowersDAO();
		FollowersModel followersModel = new FollowersModel();
		followersModel.setApproved(-1);
		followersModel.setIdUserMaster(recipient);
		followersModel.setIdUserSlave(requestee);
		boolean approved = followersdao.rejectFollower(followersModel);
		System.out.print(recipient+" rejected follower "+requestee);
		return approved ? 1 : 0;
	}
}
