package com.nvc.ten.services;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.fileupload.FileItem;

import com.nvc.ten.dao.ArchivedNotificationDAO;
import com.nvc.ten.dao.HashtagDAO;
import com.nvc.ten.dao.UnreadNotificationDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserPostCommentDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.dao.UserPostHashtagDAO;
import com.nvc.ten.dao.UserPostImageDAO;
import com.nvc.ten.dao.UserPostLikeDAO;
import com.nvc.ten.models.CommentModel;
import com.nvc.ten.models.HashtagModel;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.PostHashtagModel;
import com.nvc.ten.models.PostImageModel;
import com.nvc.ten.models.LikeModel;
import com.nvc.ten.models.extended.MentionObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.utilities.TenUtilities;

public class PostService {
	private boolean alreadySearched = false;
	private Set<String> hashtags = null;
	private String messageText = "";

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public Set<String> findHashtags(String message) {
		String[] words = message.split(" ");
		Set<String> hashtags = new HashSet<String>();
		for (String word : words) {
			if (word.startsWith("#") && word.length()<=BaseServlet.HASH_MAX_LENGTH) {
				hashtags.add(word);
			}
		}
		this.hashtags = hashtags;
		return hashtags;
	}

	public boolean hasHashtags(String message) {
		if (!alreadySearched)
			findHashtags(message);
		return this.hashtags != null;
	}

	public Set<String> getHashtags() {
		return hashtags;
	}

	public Set<String> getHashtags(String message) {
		findHashtags(message);
		return hashtags;
	}

	public int createPostForUser(int userid, String message)
			throws ClassNotFoundException, SQLException, InvalidID, CharLengthError {
		if (userid <= 0)
			throw new InvalidID();
		if(message.length()>BaseServlet.POST_MAX_LENGTH)
			throw new CharLengthError("post");
		PostModel post = new PostModel();
		UserPostDAO postdao = new UserPostDAO();
		post.setIdUser(userid);
		post.setText(message);
		post.setDatestamp(TenUtilities.getCurrentDatestamp());

		//Set expiration
        int sec = 600; //Initial 10 minutes (600 seconds)

        Timestamp original = post.getDatestamp();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(original.getTime());
        cal.add(Calendar.SECOND, sec);
        Timestamp later = new Timestamp(cal.getTime().getTime());
        post.setExpiration(later);
        
		return postdao.insert(post);
	}
	

	public int createHashtag(String hashtag_msg) throws ClassNotFoundException,
			SQLException {
		HashtagModel hashtagDTO = new HashtagModel();
		HashtagDAO hashdao = new HashtagDAO();
		hashtagDTO.setHash(hashtag_msg);
		hashtagDTO.setDatestamp(TenUtilities.getCurrentDatestamp());
		HashtagModel existingHashID = null;
		if (hashtag_msg != "" && hashtag_msg != null)
			existingHashID = hashdao.findByHash(hashtag_msg);// Get first
																// hashtag that
																// comes back
		// If hashtag doesnt exist, then return the id of the newly inserted
		// hashtag
		return existingHashID != null ? existingHashID.getHashtagid() : hashdao
				.insert(hashtagDTO);
	}

	public Set<Integer> createHashtags(Set<String> hashtags)
			throws ClassNotFoundException, SQLException {
		Set<Integer> hashtagids = new HashSet<Integer>();
		for (String hashtag : hashtags) {
			hashtagids.add(createHashtag(hashtag));
		}
		return hashtagids;
	}

	public int createUserPostHashTag(int postid, int hashid)
			throws ClassNotFoundException, SQLException, InvalidID,
			InsertFailed {
		if (postid <= 0 || hashid <= 0)
			throw new InvalidID();
		UserPostHashtagDAO userhashdao = new UserPostHashtagDAO();
		PostHashtagModel userhash = new PostHashtagModel();
		userhash.setUserPostid(postid);
		userhash.setHashtagid(hashid);
		userhash.setDatestamp(TenUtilities.getCurrentDatestamp());
		int iduserhash = userhashdao.insert(userhash);
		if (iduserhash <= 0)
			throw new InsertFailed(this.getClass().getCanonicalName());
		return iduserhash;
	}

	public int createUserPostImage(String imgpath, int postid)
			throws ClassNotFoundException, SQLException, InsertFailed, InvalidID, InvalidParameter {
		if (postid <= 0)
			throw new InvalidID();
		if(imgpath==null)
			throw new InvalidParameter("createUserPostImage");
		UserPostImageDAO userimagedao = new UserPostImageDAO();
		PostImageModel userimage = new PostImageModel();
		userimage.setIdUserPost(postid);
		userimage.setImageSrc(imgpath);
		userimage.setDatestamp(TenUtilities.getCurrentDatestamp());
		int iduserimage = userimagedao.insert(userimage);
		if (iduserimage <= 0)
			throw new InsertFailed(this.getClass().getCanonicalName());

		return iduserimage;
	}

	public PostObject getPostObject(int idpost, int iduser)
			throws ClassNotFoundException, SQLException, InvalidID, IOException {
		if (idpost <= 0)
			throw new InvalidID();
		UserPostDAO userpostdao = new UserPostDAO();
		PostObject userpost = userpostdao.getPostObjectByID(idpost,iduser);
		if(userpost.getIdUserPostRepost()>0)
			return getPostObject(userpost.getIdUserPostRepost(),iduser);
		return userpost;
	}

	// Proccess form field
	private void proccessFieldItem(FileItem field) {
		if (field.getFieldName().equals("message"))
			messageText = field.getString();
	}

	// Returns list of all image names
	public List<String> processPostUploadImages(String filename,
			List<FileItem> items, int iduser) throws Exception {
		TenUtilities globalUtilities = new TenUtilities();
		List<String> imageNames = new ArrayList<>();
		int imageCounter = 0; // This is so we can give all images in the post a
								// unique name
		Iterator<FileItem> iter = items.iterator();
		imageNames = new ArrayList<String>();
		while (iter.hasNext()) {
			FileItem item = iter.next();
			if (globalUtilities.isSupportedImage(item)) {
				imageCounter++;
				imageNames.add(globalUtilities.uploadImage(filename
						+ imageCounter, item, iduser));
			} else if (item.isFormField()) {
				proccessFieldItem(item);
			}
		}
		return imageNames;
	}
	
	public Integer removePost(int idpost, int iduser) throws ClassNotFoundException, SQLException{
		UserPostLikeDAO likedao = new UserPostLikeDAO();
		UserPostCommentDAO commentdao = new UserPostCommentDAO();
		UserPostImageDAO imagesdao = new UserPostImageDAO();
		UserPostHashtagDAO hashdao = new UserPostHashtagDAO();
		ArchivedNotificationDAO anotifications = new ArchivedNotificationDAO();
		UnreadNotificationDAO unotifications = new UnreadNotificationDAO();
		
		Set<LikeModel> likes = likedao.findByPostID(idpost);
		Set<PostImageModel> images = imagesdao.findByPostID(idpost);
		Set<CommentModel> comments = commentdao.findByPostID(idpost);
		Set<PostHashtagModel> hashes = hashdao.findByPostID(idpost);
		
		for(LikeModel like : likes){
			likedao.deleteByID(like.getIdUserPostLike());
		}
		
		for(PostImageModel image : images){
			imagesdao.deleteByID(image.getIdUserPostImage());
		   	try{
	    		File file = new File(BaseServlet.PHOTO_STORE_PATH+iduser+"/"+image.getImageSrc());
	    		file.delete();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
		}

		for(CommentModel comment : comments){
			commentdao.deleteByID(comment.getIdUserComment());
		}
		
		for(PostHashtagModel hash : hashes){
			hashdao.deleteByID(hash.getUserPostHashtagid());
		}
		
		anotifications.deleteByID(idpost);
		
		unotifications.deleteByID(idpost);
		
		UserPostDAO postdao = new UserPostDAO();
		PostModel post = postdao.findByID(idpost);
		if(post.getIdUserPost()>0 && post.getIdUser()==iduser){
			postdao.deleteByID(idpost);
			return idpost;
		}
		return 0;
	}

	public void createMentions(String messageText,int mentionerId,int userpostId) throws ClassNotFoundException, SQLException{
		UserDAO userdao = new UserDAO();
		Set<MentionObject> mentions = new HashSet<MentionObject>();
		Pattern pattern = Pattern.compile("@([a-zA-Z0-9_]{5,})"); 
		Matcher matcher = pattern.matcher(messageText);
		while (matcher.find()) {
			UserModel user;
			MentionObject mention;
			System.out.println(userpostId + " by " + mentionerId + " mentions " + matcher.group(1));
			try {
				user = userdao.findByUsername(matcher.group(1));
				mention = new MentionObject();
				if(user!=null){
					mention.setMentioned(new UserObject(user));
					mention.setMentionerId(mentionerId);
					mention.setDatestamp(TenUtilities.getCurrentDatestamp());
					mention.setUserpostid(userpostId);
					mentions.add(mention);
				}
			} catch (ClassNotFoundException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(mentions.size()>0)
			insertMentionNotifications(mentions);
	}
	
	public void insertMentionNotifications(Set<MentionObject> mentions) throws ClassNotFoundException, SQLException{
		for(MentionObject mention : mentions){
			this.insertMentionNotification(mention);
		}
	}
	
	public boolean insertMentionNotification(MentionObject mention) throws ClassNotFoundException, SQLException{
		UnreadNotificationDAO unreadnotificationdao = new UnreadNotificationDAO();
		return unreadnotificationdao.insertMentionsNotification(mention.getMentionerId(), mention.getMentioned().getIdUser(),mention.getUserpostid());
	}
}
