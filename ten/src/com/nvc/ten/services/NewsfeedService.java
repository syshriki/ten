package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;

import com.nvc.ten.dao.NewsfeedObjectDAO;
import com.nvc.ten.models.extended.NewsfeedObject;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidReturnCount;
import com.nvc.ten.throwable.service.NullReturned;

public class NewsfeedService {
	NewsfeedObjectDAO newsfeeddao = new NewsfeedObjectDAO();
	public NewsfeedObject getNewsfeed(int userid,int offset,int count) throws ClassNotFoundException, SQLException, IOException, InvalidReturnCount, InvalidID, NullReturned{
		//This sets a range, so now we have offset = 20 amount = 5, range is 20-25
		if(offset<0 || count<0 || count > BaseServlet.MAX_NEWSFEED_RETURN_COUNT)
			throw new InvalidReturnCount();
		if(userid<=0)
			throw new InvalidID();
		
		NewsfeedObject newsfeed= newsfeeddao.getNewsFeed(userid,offset,count);
		
		if(newsfeed==null)
			throw new NullReturned();
		
		return newsfeed;
	}
}
