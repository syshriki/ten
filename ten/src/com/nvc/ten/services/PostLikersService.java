package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nvc.ten.dao.PostLikersDAO;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.utilities.TenUtilities;

public class PostLikersService {
	private TenUtilities utils = new TenUtilities();
	public List<FindFriendTO> getPostLikers(int postid,int offset,int count,int userid){
		List<FindFriendTO> findFriendTos = new ArrayList<>();
		PostLikersDAO postLikersDao = new PostLikersDAO();
		try {
			List<UserModel> users = postLikersDao.getPostLikers(postid, offset, count);
			for(UserModel u : users){
				UserObject userObj = new UserObject(u);
				FindFriendTO findFriendTo = new FindFriendTO(userObj);
				findFriendTo.setFriended(utils.followingStatus(userid, findFriendTo.getUserid()));
				findFriendTos.add(findFriendTo);
			}
			return findFriendTos;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
