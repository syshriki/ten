package com.nvc.ten.services;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

public class ForgotPasswordService {
	public void sendEmail(String email,String recoverLink) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "my-mail-server");
		Session session = Session.getInstance(props, null);
		try {
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom("forgotpass@ten.com");
			msg.setRecipients(Message.RecipientType.TO, email);
			msg.setSubject("Ten - Forgot Password");
			msg.setSentDate(new Date());
			msg.setText(recoverLink+"\n"+"DO NOT REPLY TO THIS EMAIL");
			Transport.send(msg, "forgotpass@ten.com", "my-password");
		} catch (MessagingException mex) {
			System.out.println("send failed, exception: " + mex);
		}
	}
}
