package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.dao.HashtagDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.transfer.PostTO;

public class FindPostByHashService {
	public List<PostTO> findPostByHash(String hash,int offset, int count) throws ClassNotFoundException, SQLException, IOException{
		HashtagDAO hashdao = new HashtagDAO();
		UserPostDAO postdao = new UserPostDAO();
		List<PostModel> userposts = hashdao.findPostByHash(hash, offset, count);
		List<PostTO> userposttos = new ArrayList<>();
		for(PostModel up : userposts){
			userposttos.add(new PostTO(postdao.getPostObjectByID(up.getIdUserPost(),up.getIdUser())));
		}
		return userposttos;
	}
			
}
