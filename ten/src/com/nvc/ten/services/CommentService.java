package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.nvc.ten.dao.UnreadNotificationDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserPostCommentDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.models.CommentModel;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.extended.MentionObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.NotFollowingPrivateException;
import com.nvc.utilities.TenUtilities;

public class CommentService {
	public int createComment(int postid,String comment,int userid) throws InsertFailed, ClassNotFoundException, SQLException, InvalidID, InvalidParameter, CharLengthError, NotFollowingPrivateException{
		
		//Invalid Comment Checks
		if(postid <= 0 || userid <= 0)
			return 0;
		if(comment==null|| comment.length()==0 || comment.replaceAll(" ","").length()==0)
			return 0;
		if(comment.length()>=BaseServlet.COMMENT_MAX_LENGTH){
			return 0;
		}
		
		//Check following and private
		UserPostDAO postdao = new UserPostDAO();
		PostModel postDTO = postdao.findByID(postid);
		
		//Could not find post
		if(postDTO.getIdUserPost()<=0)
			return 0;
		
		TenUtilities globalutils = new TenUtilities();
		
		//Can only post comment if user is public or they are following you
		if(!globalutils.isFollowingUser(userid, postDTO.getIdUser())&& postDTO.getIdUser()!=userid){
			if(globalutils.postIsPrivate(postDTO.getIdUserPost()))
				return 0;
		}
		if(postDTO.getIdUserPostRepost()>0){
			PostModel repostDTO = postdao.findByID(postDTO.getIdUserPostRepost());
			postDTO = repostDTO;
		}
		
		UserPostCommentDAO usercommentdao = new UserPostCommentDAO();
		CommentModel usercomment = new CommentModel();
		
		usercomment.setIdUserPost(postid);
		usercomment.setComment(comment);
		usercomment.setIdUser(userid);
		usercomment.setDatestamp(TenUtilities.getCurrentDatestamp());
		
		int idcomment = usercommentdao.insert(usercomment);
		//Create mention notification
		createMentions(comment,userid,postid);
		
		if(idcomment<=0)
			throw new InsertFailed(this.getClass().getCanonicalName());
		
		return idcomment > 0 ? 1 : 0;
	}
	
	public Map<String,Integer> removeComment(int idComment,int iduser) throws ClassNotFoundException, SQLException{
		UserPostCommentDAO usercommentdao = new UserPostCommentDAO();
		UserPostDAO userpostdao = new UserPostDAO();
		CommentModel comment = usercommentdao.findByID(idComment);
		PostModel post = null;
		if(comment!=null)
			post = userpostdao.findByID(comment.getIdUserPost());
		Map<String,Integer> results = new HashMap<>();
		if(comment !=null && comment.getIdUserComment()>0 && (iduser == comment.getIdUser() || (post !=null && post.getIdUser()==iduser))){//Comment exists
			usercommentdao.deleteByID(idComment);
		}
		results.put("deletecomment", idComment > 0 ? 1 : 0);
		return results;
	}
	
	public void createMentions(String commentText,int mentionerId,int userpostId) throws ClassNotFoundException, SQLException{
		UserDAO userdao = new UserDAO();
		Set<MentionObject> mentions = new HashSet<MentionObject>();
		Pattern pattern = Pattern.compile("@([a-zA-Z0-9_]{5,})"); 
		Matcher matcher = pattern.matcher(commentText);
		while (matcher.find()) {
			UserModel user;
			MentionObject mention;
			System.out.println(userpostId + " by " + mentionerId + " mentions " + matcher.group(1));
			try {
				user = userdao.findByUsername(matcher.group(1));
				mention = new MentionObject();
				if(user!=null){
					mention.setMentioned(new UserObject(user));
					mention.setMentionerId(mentionerId);
					mention.setDatestamp(TenUtilities.getCurrentDatestamp());
					mention.setUserpostid(userpostId);
					mentions.add(mention);
				}
			} catch (ClassNotFoundException | SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(mentions.size()>0)
			insertMentionNotifications(mentions);
	}
	
	public void insertMentionNotifications(Set<MentionObject> mentions) throws ClassNotFoundException, SQLException{
		for(MentionObject mention : mentions){
			this.insertMentionNotification(mention);
		}
	}
	
	public boolean insertMentionNotification(MentionObject mention) throws ClassNotFoundException, SQLException{
		UnreadNotificationDAO unreadnotificationdao = new UnreadNotificationDAO();
		return unreadnotificationdao.insertMentionsNotification(mention.getMentionerId(), mention.getMentioned().getIdUser(),mention.getUserpostid());
	}
}
