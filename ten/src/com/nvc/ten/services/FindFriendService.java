package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.FindFriendTO;
import com.nvc.ten.models.transfer.FindFriendsTO;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.InvalidReturnCount;
import com.nvc.utilities.TenUtilities;

public class FindFriendService {
	public FindFriendsTO searchForFriend(String username, int offset,int count, int iduser) throws ClassNotFoundException, SQLException, InvalidReturnCount, IOException{
		if(offset<0 || count<0 || count > BaseServlet.MAX_FRIEND_SEARCH_RETURN_COUNT)
			throw new InvalidReturnCount();
		
		UserDAO userdao = new UserDAO();
		TenUtilities utils = new TenUtilities(); 
		List<UserModel> userdtos = userdao.findByUsernameLimit(username+"%", offset, count, iduser);
		List<FindFriendTO> userobjs = new ArrayList<>();
		FindFriendsTO findFriends = new FindFriendsTO();
		for(UserModel userdto : userdtos){
			FindFriendTO user = new FindFriendTO(new UserObject(userdto));
			//TODO add into findByUsernameLimit
			// -1 = not friended, 0 = pending 1 = friended
			user.setFriended(utils.followingStatus(iduser, userdto.getIdUser()));
			userobjs.add(user);
		}
		findFriends.setUsers(userobjs);
		return findFriends;
	}
}
