package com.nvc.ten.services;

import java.sql.SQLException;
import java.util.List;

import org.apache.commons.fileupload.FileItem;

import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserSaltDAO;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.UserSalt;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.RegistrationErrorException;
import com.nvc.ten.throwable.service.UserAlreadyExistsException;
import com.nvc.utilities.TenUtilities;
import com.nvc.utilities.PasswordProtectionUtility;

public class RegisterService {
	
	public int proccessRegisterRequest(List<FileItem> parameters) throws ClassNotFoundException, SQLException, InvalidParameter, RegistrationErrorException, InsertFailed, CharLengthError, UserAlreadyExistsException{
		String username = null;
		String password = null;
		String description = null;
		String email = null;
		String website = null;
		String postprivate = null;
		String imageName = null;
		FileItem imageItem = null;

		for (FileItem item : parameters) {
			if (item.isFormField()) {
				switch (item.getFieldName()) {
				case "username":
					username = item.getString();
					break;
				case "password":
					password = item.getString();
					break;
				case "description":
					description = item.getString();
					break;
				case "email":
					email = item.getString();
					break;
				case "website":
					website = item.getString();
					break;
				case "postprivate":
					postprivate = item.getString();
					break;
				}
			} else {
				imageItem = item;
			}
		}
		if (website == null)
			website = "";
		if (postprivate == null)
			postprivate = "false";
		if (description == null)
			description = "";

		// Create user object to add to database
		int iduser = 0;
		UserModel userDTO = new UserModel();
		//TODO WORk out what an invalid username is
		if(!TenUtilities.validUsername(username)){
			System.out.println("Registration failed, invalid username " + username);
			return 0;
		}
		userDTO.setUsername(username);
		userDTO.setPassword(password.getBytes());
		userDTO.setDescription(description);
		userDTO.setDatestamp(com.nvc.utilities.TenUtilities
				.getCurrentDatestamp());
		userDTO.setEmail(email);
		userDTO.setWebsite(website);
		userDTO.setPrivate(Boolean.valueOf(postprivate));
		TenUtilities globalUtilities = new TenUtilities();
		if (!globalUtilities.usernameExists(username)
				&& !this.emailExists(email)) {
			iduser = this.registerUser(userDTO);
			userDTO.setIdUser(iduser);
			// User has uploaded a profile image
			if (imageItem != null) {
				imageName = TenUtilities.uploadProfilePicture(imageItem, iduser);
				userDTO.setImagesrc(imageName);
				this.registerUser(userDTO);
				// Add user image to user table
			}
			// Logs user in, not sure if needed
			// HttpSession sess = request.getSession(true);
			// sess.setAttribute("USER", userDTO);
		} else {
			throw new UserAlreadyExistsException();
		}
		return 1;
	}
	
	public int registerUser(UserModel userDTO) throws RegistrationErrorException,
			ClassNotFoundException, SQLException, InsertFailed, CharLengthError {
		// TODO dto validation
		if (userDTO.getWebsite() != null) {
			if (userDTO.getWebsite().length() >= BaseServlet.WEBSITE_MAX_LENGTH)
				throw new CharLengthError("register -> website("
						+ BaseServlet.WEBSITE_MAX_LENGTH + ")");
		}
		if (userDTO.getUsername() != null) {
			if (userDTO.getUsername().length() >= BaseServlet.USERNAME_MAX_LENGTH)
				throw new CharLengthError("register -> user("
						+ BaseServlet.USERNAME_MAX_LENGTH + ")");
		}
		if (userDTO.getPassword().toString().length() >= BaseServlet.PASSWORD_MAX_LENGTH)
			throw new CharLengthError("register -> password("
					+ BaseServlet.PASSWORD_MAX_LENGTH + ")");
		if (userDTO.getEmail() != null) {
			if (userDTO.getEmail().length() >= BaseServlet.EMAIL_MAX_LENGTH)
				throw new CharLengthError("register -> email("
						+ BaseServlet.EMAIL_MAX_LENGTH + ")");
		}
		if (userDTO.getDescription() != null) {
			if (userDTO.getDescription().length() >= BaseServlet.DESCRIPTION_MAX_LENGTH)
				throw new CharLengthError("register -> description("
						+ BaseServlet.DESCRIPTION_MAX_LENGTH + ")");
		}
		UserDAO userdao = new UserDAO();
		UserSalt usersalt = new UserSalt();
		UserSaltDAO saltdao = new UserSaltDAO();
		PasswordProtectionUtility ppu = new PasswordProtectionUtility();

		// Generate salt
		byte[] salt = ppu.generateSalt();

		// Store salt in object as required by dao to insert
		usersalt.setSalt(salt);
		usersalt.setDatestamp(TenUtilities.getCurrentDatestamp());

		// Convert password to chars
		char[] unhashed_password = ppu.bytesToChars(userDTO.getPassword());

		// Apply salt to password
		byte[] hashed_password = ppu.applyHash(unhashed_password, salt);

		// Set the user password to the hashed password to pass user object
		// to dao and have it store encrypted password.
		userDTO.setPassword(hashed_password);

		// If an integer is returned, the user has been successfully created
		int userid = userdao.insert(userDTO);

		// If there was an error inserting user throw error
		if (userid < 1)
			throw new InsertFailed(this.getClass().getCanonicalName());

		// User was successfully inserted into database, now insert user salt
		usersalt.setIduser(userid);
		userDTO.setIdUser(userid);
		saltdao.insert(usersalt);

		System.out.println("New user created [" + userDTO.getIdUser() + "] "
				+ userDTO.getUsername());// NEEDS LOGGING
		return 1;
	}

	
	public boolean emailExists(String email) throws ClassNotFoundException,
			SQLException, InvalidParameter {
		if (email == null || email.equals(""))
			throw new InvalidParameter("email");
		UserDAO userdao = new UserDAO();
		// If a user is returned then the user does exist
		return userdao.findByEmail(email) != null;
	}

}
