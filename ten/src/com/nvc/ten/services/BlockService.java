package com.nvc.ten.services;

import java.sql.SQLException;

import com.nvc.ten.dao.UserBlocksDAO;
import com.nvc.ten.models.BlockUserModel;
import com.nvc.utilities.TenUtilities;

public class BlockService {
	public Integer blockUser(int idblocker, int idblocked) throws ClassNotFoundException, SQLException{
		UserBlocksDAO userblocksdao = new UserBlocksDAO();
		BlockUserModel userpair = new BlockUserModel();
		userpair.setIdUserMaster(idblocker);
		userpair.setIdUserSlave(idblocked);
		userpair.setDatestamp(TenUtilities.getCurrentDatestamp());
		if(userblocksdao.findByUserID(idblocker, idblocked)==null)
			return userblocksdao.insert(userpair) > 0 ? 1 : 0;
		return 0;
	}
	
	public Integer unblockUser(int idblocker, int idblocked) throws ClassNotFoundException, SQLException{
		UserBlocksDAO userblocksdao = new UserBlocksDAO();
		BlockUserModel userpair = userblocksdao.findByUserID(idblocker, idblocked);
		if(userpair!=null){
			if(userblocksdao.deleteByID(userpair.getIdPair()))
				return 1;
		}
		return 0;
	}
}
