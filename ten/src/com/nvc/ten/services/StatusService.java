package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.nvc.ten.dao.StatusDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.models.extended.StatusObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.transfer.PostTO;
import com.nvc.ten.models.transfer.StatusTO;
import com.nvc.ten.models.transfer.UserTO;

public class StatusService {

	public List<StatusTO> getStatuses(int iduser) throws ClassNotFoundException, SQLException, IOException{
		List<StatusObject> statusObjs = new ArrayList<>();
		List<StatusTO> statuses = new ArrayList<>();
		StatusDAO statusdao = new StatusDAO();
		UserDAO userdao = new UserDAO();
		UserPostDAO postdao = new UserPostDAO();
		statusObjs = statusdao.getStatusByUserId(iduser);
		
		for(StatusObject statusobj : statusObjs){
			StatusTO status = new StatusTO();
			status.setDatestamp(statusobj.getDatestamp());
			status.setPost(new PostTO(postdao.getPostObjectByID(statusobj.getIdpost(), statusobj.getIduser())));
			status.setNotificationType(statusobj.getNotificationType());
			status.setLikecount(statusobj.getLikecount());
			status.setUser(new UserTO(new UserObject(userdao.findByID(statusobj.getIduser()))));
			status.setNotificationId(statusobj.getNotificationId());
			status.setUserAffecting(new UserTO(new UserObject(userdao.findByID(statusobj.getAffectingUser()))));
			statuses.add(status);
		}
		return statuses;
	}
}
