package com.nvc.ten.services;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;

import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.dao.UserPostLikeDAO;
import com.nvc.ten.models.PostModel;
import com.nvc.ten.models.LikeModel;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.NotFollowingPrivateException;
import com.nvc.utilities.TenUtilities;

public class LikeService {
	public int createUserPostLike(int postid,int userid) throws ClassNotFoundException, SQLException, InvalidID, InsertFailed, NotFollowingPrivateException{
		if(postid <= 0 || userid <= 0)
			throw new InvalidID();
		UserPostLikeDAO userlikedao = new UserPostLikeDAO();
		LikeModel userlike = new LikeModel();
		//Check following and private
		UserPostDAO postdao = new UserPostDAO();
		PostModel postDTO = postdao.findByID(postid);
		TenUtilities globalutils = new TenUtilities();
		
		if(!globalutils.isFollowingUser(userid, postDTO.getIdUser())){
			if(globalutils.postIsPrivate(postDTO.getIdUserPost()))
				return 0;
		}
		
		//User cannot like their own post
		if(userlikedao.findByPostUser(postid,userid)!=null){
			return 0;
		}
		//Also like the repost, now will like all the way up to parent post
		if(postDTO.getIdUserPostRepost()>0){
			PostModel repostDTO = postdao.findByID(postDTO.getIdUserPostRepost());
			postDTO = repostDTO;
		}
		
		//Add to expiration
        int sec = 60; //1 Minute

        Timestamp original = postDTO.getExpiration();
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(original.getTime());
        cal.add(Calendar.SECOND, sec);
        Timestamp later = new Timestamp(cal.getTime().getTime());
        postDTO.setExpiration(later);
		
		userlike.setIdUserPost(postid);
		userlike.setIdUser(userid);
		userlike.setDatestamp(TenUtilities.getCurrentDatestamp());
		
		int iduserlike = 0;
		if(postDTO.getExpiration().after(TenUtilities.getCurrentDatestamp())){
			iduserlike = userlikedao.insert(userlike);
			postdao.updateExpiration(postDTO);
			if(iduserlike <= 0)
				throw new InsertFailed(this.getClass().getCanonicalName());
			else
				System.out.println(userid+ " created like " + iduserlike + " on post " + postid);
		}
		return iduserlike > 0 ? 1 : 0;
	}
}
