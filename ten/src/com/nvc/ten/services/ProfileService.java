package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.fileupload.FileItem;

import com.nvc.ten.dao.ProfileDAO;
import com.nvc.ten.dao.UserBlocksDAO;
import com.nvc.ten.dao.UserDAO;
import com.nvc.ten.dao.UserPostLikeDAO;
import com.nvc.ten.dao.UserSaltDAO;
import com.nvc.ten.models.LikeModel;
import com.nvc.ten.models.UserModel;
import com.nvc.ten.models.UserSalt;
import com.nvc.ten.models.extended.LikeObject;
import com.nvc.ten.models.extended.ProfileBestTimeObject;
import com.nvc.ten.models.extended.ProfileObject;
import com.nvc.ten.models.extended.UserObject;
import com.nvc.ten.models.extended.PostObject;
import com.nvc.ten.models.transfer.ProfileOtherTO;
import com.nvc.ten.models.transfer.ProfileSelfTO;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.CharLengthError;
import com.nvc.ten.throwable.service.InsertFailed;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.NullReturned;
import com.nvc.ten.throwable.service.RegistrationErrorException;
import com.nvc.utilities.TenUtilities;
import com.nvc.utilities.PasswordProtectionUtility;

public class ProfileService {
	public ProfileObject getProfileObject(int userid, int offset, int count)
			throws ClassNotFoundException, SQLException, NullReturned,
			InvalidID, IOException {
		return getProfileObject(userid, userid, offset, count);
	}

	public ProfileObject getProfileObject(String username, int r_userid,
			int offset, int count) throws ClassNotFoundException, SQLException,
			NullReturned, InvalidID, IOException {
		UserDAO userdao = new UserDAO();
		UserModel user = userdao.findByUsername(username);
		int userid = 0;
		if (user != null)
			userid = user.getIdUser();
		return getProfileObject(userid, r_userid, offset, count);
	}

	// r_userid is the user requesting
	public ProfileObject getProfileObject(int userid, int r_userid, int offset,
			int count) throws ClassNotFoundException, SQLException,
			NullReturned, InvalidID, IOException {
		if (userid <= 0)
			throw new InvalidID();
		ProfileObject profile = new ProfileObject();
		ProfileDAO profiledao = new ProfileDAO();
		UserDAO userdao = new UserDAO();
		UserBlocksDAO userblocks = new UserBlocksDAO();
		UserPostLikeDAO userlikes = new UserPostLikeDAO();
		TenUtilities utils = new TenUtilities();

		UserModel userdto = userdao.findByID(userid);
		if (userdto == null)
			throw new NullReturned();
		UserObject userobject = new UserObject(userdto);

		int followerCount = profiledao.getFollowerCount(userid,offset,count);
		int followingCount = profiledao.getFollowingCount(userid,offset,count);
		ProfileBestTimeObject topTime = profiledao.getBestTime(userid);
		List<UserModel> topLikers = profiledao.getTopLikers(userid);
		int postCount = profiledao.getProfilePosts(userid);
		int blocked = userblocks.findByUserID(userid, r_userid) == null ? 0 : 1;
		if (blocked == 0)
			blocked = userblocks.findByUserID(r_userid, userid) == null ? 0 : 1;

		List<UserObject> topLikerobjs = new ArrayList<>();
		// Make dtos into objects
		for (UserModel user : topLikers) {
			topLikerobjs.add(new UserObject(user));
		}
		// Users prospecting other unfollowed users cannot see posts
		List<PostObject> userposts = null;
		if (r_userid == userid) {
			userposts = profiledao.getPosts(userid, offset, count);
		} else {
			userposts = profiledao.getActivePosts(userid, offset, count);
			// for(UserPostObject upo: userposts){
			// upo.setUser(profile.getUser());
			// }
		}
		if (userposts != null) {
			for (PostObject p : userposts) {
				Set<LikeModel> likes = userlikes.findByPostID(p.getIdUserPost());
				for(LikeModel likeModel : likes){
					p.addLike(new LikeObject(likeModel));
				}
				p.loadImageData();
			}
		}
		profile.setUserposts(userposts);
		profile.setBlocked(blocked);
		profile.setFollowers(followerCount);
		profile.setPostCount(postCount);
		profile.setFollowing(followingCount);
		profile.setTopLikers(topLikerobjs);
		profile.setUser(userobject);
		profile.setTopTime(topTime.getTime());
		profile.setFriended(utils.followingStatus(r_userid, userid));

		return profile;
	}

	/*
	 * public boolean isProspect(int idrequestee,int idrecipient) throws
	 * SQLException, InvalidID{ if(idrequestee <= 0 || idrecipient <= 0) throw
	 * new InvalidID(); FollowersDAO followdao = new FollowersDAO(); UserPairDTO
	 * followed = followdao.getFollowing(idrequestee, idrecipient); return
	 * followed==null; }
	 */

	public Object viewProfileByID(int iduserRequestee, int iduserRecipient,
			int offset, int count) throws ClassNotFoundException, SQLException,
			InvalidID, NullReturned, IOException {
		System.out.println(iduserRequestee + " trying to view profile (BY ID)"
				+ iduserRecipient);
		return new ProfileOtherTO(this.getProfileObject(iduserRecipient,
				iduserRequestee, offset, count));

	}

	public Object viewProfileByUsername(int iduserRequestee,
			String usernameRecipient, int offset, int count)
			throws ClassNotFoundException, SQLException, InvalidID,
			NullReturned, IOException {
		System.out.println(iduserRequestee + " trying to view profile "
				+ usernameRecipient);
		return new ProfileOtherTO(this.getProfileObject(usernameRecipient,
				iduserRequestee, offset, count));

	}

	public Object viewSelfProfile(int iduser, int offset, int count) {
		System.out.println(iduser + " trying to view SELF profile");
		try {
			return new ProfileSelfTO(this.getProfileObject(iduser, offset,
					count));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullReturned e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidID e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Object updateProfile(List<FileItem> parameters, int uid)
			throws Exception {
		String username = null;
		String password = null;
		String description = null;
		String email = null;
		String website = null;
		String postprivate = null;
		String imagesrc = null;
		String imageName = null;

		FileItem imageItem = null;

		for (FileItem item : parameters) {
			if (item.isFormField()) {
				switch (item.getFieldName()) {
				case "username":
					username = item.getString();
					break;
				case "newpassword":
					password = item.getString();
					break;
				case "description":
					description = item.getString();
					break;
				case "email":
					email = item.getString();
					break;
				case "website":
					website = item.getString();
					break;
				case "postprivate":
					postprivate = item.getString();
					break;
				}
			} else {
				imageItem = item;
			}
		}
		// Create user object to add to database
		int iduser = uid;
		UserDAO userdao = new UserDAO();
		UserModel userOld = userdao.findByID(iduser);

		return this.updateUserProfile(userOld, username, password, description,
				email, website, postprivate, imageItem);

	}

	public int updateUserProfile(UserModel userOld, String username,
			String password, String description, String email, String website,
			String postprivate, FileItem image)
			throws RegistrationErrorException, ClassNotFoundException,
			SQLException, InsertFailed, CharLengthError {
		// TODO dto validation
		if (userOld.getWebsite() != null) {
			if (userOld.getWebsite().length() >= BaseServlet.WEBSITE_MAX_LENGTH)
				throw new CharLengthError("register -> website("
						+ BaseServlet.WEBSITE_MAX_LENGTH + ")");
		}
		if (userOld.getUsername() != null) {
			if (userOld.getUsername().length() >= BaseServlet.USERNAME_MAX_LENGTH)
				throw new CharLengthError("register -> user("
						+ BaseServlet.USERNAME_MAX_LENGTH + ")");
		}
		if ((password != null)
				&& userOld.getPassword().toString().length() >= BaseServlet.PASSWORD_MAX_LENGTH)
			throw new CharLengthError("register -> password("
					+ BaseServlet.PASSWORD_MAX_LENGTH + ")");
		if (userOld.getEmail() != null) {
			if (userOld.getEmail().length() >= BaseServlet.EMAIL_MAX_LENGTH)
				throw new CharLengthError("register -> email("
						+ BaseServlet.EMAIL_MAX_LENGTH + ")");
		}
		if (userOld.getDescription() != null) {
			if (userOld.getDescription().length() >= BaseServlet.DESCRIPTION_MAX_LENGTH)
				throw new CharLengthError("register -> description("
						+ BaseServlet.DESCRIPTION_MAX_LENGTH + ")");
		}

		if (username != null)
			userOld.setUsername(username);
		if (password != null)
			userOld.setPassword(password.getBytes());
		if (description != null)
			userOld.setDescription(description);
		if (email != null)
			userOld.setEmail(email);
		if (website != null)
			userOld.setWebsite(website);
		if (postprivate != null)
			userOld.setPrivate(Boolean.valueOf(postprivate));
		if (image != null) { // User has uploaded a profile image
			// Add user image to user table
			String imageName;
			imageName = TenUtilities.uploadProfilePicture(image,
					userOld.getIdUser());
			userOld.setImagesrc(imageName);
		}

		UserDAO userdao = new UserDAO();
		UserSalt usersalt = new UserSalt();
		UserSaltDAO saltdao = new UserSaltDAO();
		if (password != null) {
			PasswordProtectionUtility ppu = new PasswordProtectionUtility();

			// Generate salt
			byte[] salt = ppu.generateSalt();

			// Store salt in object as required by dao to insert
			usersalt.setSalt(salt);
			usersalt.setDatestamp(TenUtilities.getCurrentDatestamp());

			// Convert password to chars
			char[] unhashed_password = ppu.bytesToChars(userOld.getPassword());

			// Apply salt to password
			byte[] hashed_password = ppu.applyHash(unhashed_password, salt);

			// Set the user password to the hashed password to pass user object
			// to dao and have it store encrypted password.
			userOld.setPassword(hashed_password);
		}
		// If an integer is returned, the user has been successfully created
		int userid = 0;
		userid = userOld.getIdUser();
		String oldUsername = userdao.findByID(userid).getUsername();
		if (!oldUsername.equals(userOld.getUsername())) {// username has changed
			if (userdao.findByUsername(userOld.getUsername()) != null)
				return -1;
		}
		userdao.update(userOld);

		// If there was an error inserting user throw error
		if (userid < 1)
			throw new InsertFailed(this.getClass().getCanonicalName());

		// User was successfully inserted into database, now insert user salt
		if (password != null) {
			usersalt.setIduser(userid);
			userOld.setIdUser(userid);
			saltdao.update(usersalt);
		}

		System.out.println("User updated[" + userOld.getIdUser() + "] "
				+ userOld.getUsername());// NEEDS LOGGING
		return 1;
	}



}
