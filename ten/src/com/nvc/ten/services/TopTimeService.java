package com.nvc.ten.services;

import java.sql.SQLException;

import com.nvc.ten.dao.ProfileDAO;
import com.nvc.ten.models.extended.ProfileBestTimeObject;
import com.nvc.ten.models.transfer.BestTimeTO;

public class TopTimeService {
	public BestTimeTO getTopTime(int userid){
		ProfileDAO profiledao = new ProfileDAO();
		ProfileBestTimeObject bestTimeObj = null;
		BestTimeTO bestTimeTO = null;
		try {
			bestTimeObj = profiledao.getBestTime(userid);
			bestTimeTO = new BestTimeTO(bestTimeObj);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (bestTimeTO);
		
	}
}
