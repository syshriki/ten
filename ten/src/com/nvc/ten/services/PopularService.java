package com.nvc.ten.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nvc.ten.dao.PopularDAO;
import com.nvc.ten.dao.UserPostDAO;
import com.nvc.ten.models.extended.LikeObject;
import com.nvc.ten.models.extended.PopularTimedObject;
import com.nvc.ten.models.extended.PopularTimerObject;
import com.nvc.ten.models.transfer.PopularTimedTO;
import com.nvc.ten.models.transfer.PopularTimersTO;
import com.nvc.ten.models.transfer.PostTO;
import com.nvc.ten.models.transfer.UserTO;
import com.nvc.ten.servlets.BaseServlet;
import com.nvc.ten.throwable.service.InvalidID;
import com.nvc.ten.throwable.service.InvalidParameter;
import com.nvc.ten.throwable.service.InvalidReturnCount;
import com.nvc.ten.throwable.service.NullReturned;

public class PopularService {
	
	public List<PostTO> getTopTimed(String scope,int offset,int count) throws ClassNotFoundException, SQLException, InvalidID, NullReturned, InvalidReturnCount, InvalidParameter, IOException{
		return getTopTimed(scope,0,offset,count);
	}
	
	public List<PostTO> getTopTimed(String scope,int id,int offset,int count) throws ClassNotFoundException, SQLException, InvalidID, NullReturned, InvalidReturnCount, InvalidParameter, IOException{

		if(scope==null)
			throw new NullReturned();
		if(offset < 0 || count < 0 || count > BaseServlet.MAX_TIMED_RETURN_COUNT)
			throw  new InvalidReturnCount();
		
		PopularDAO populardao =  new PopularDAO();
		if(scope.equals("local")){
			if(id<=0)
				throw new InvalidID();
			List<PopularTimedObject> pto = populardao.getPopularLocalTopTimed(id,offset,count);
			
			//Move objects to transfer objects
			List<PostTO> postTos = new ArrayList<>(); 
			for(PopularTimedObject popularTimedObject : pto){
				//Add phantom likes
				this.addPhantomLikes(popularTimedObject);
				postTos.add(new PostTO(popularTimedObject.getUserpost()));
			}
			
			return postTos;
		} else if(scope.equals("global")){
			List<PopularTimedObject> pto = populardao.getPopularGlobalTopTimed(offset,count);
			//Move objects to transfer objects
			List<PostTO> postTos = new ArrayList<>(); 
			for(PopularTimedObject popularTimedObject : pto){
				//Add phantom likes
				this.addPhantomLikes(popularTimedObject);
				postTos.add(new PostTO(popularTimedObject.getUserpost()));
			}
			
			return postTos;
		} else {
			throw new InvalidParameter("scope");
		}
	}
	
	//This is a hack to add like to a post object so we have a count of likes, they are empty deliberatly
	private void addPhantomLikes(PopularTimedObject popluarTimedObject){
		for(int i = 0; i < popluarTimedObject.getScore(); i++){
			popluarTimedObject.getUserpost().addLike(new LikeObject(i+1));
		}
	}
	
	public List<UserTO> getTopTimers(int offset,int count) throws Exception{
		return getTopTimers("global",0,offset,count);
	}
	
	public List<UserTO> getTopTimers(String scope,int id,int offset,int count) throws ClassNotFoundException, SQLException, InvalidID, NullReturned, Exception{
		if(id<0)
			throw new InvalidID();
		if(scope==null)
			throw new NullReturned();
		
		PopularDAO populardao =  new PopularDAO();
		if(scope.equals("local")){
			return timerListToPostList(populardao.getPopularLocalTopTimers(id,offset,count));
		} else if(scope.equals("global")){
			return timerListToPostList(populardao.getPopularGlobalTopTimers(offset,count));
		} else {
			throw new InvalidParameter("view scope");
		}
	}
	
	public List<UserTO> getTopTimers(int userid, String view_scope) throws Exception{
		if (view_scope.equals("local")) {
			return this.getTopTimers(view_scope, userid, 0, BaseServlet.MAX_TIMERS_RETURN_COUNT);
		} else if (view_scope.equals("global")) {
			return this.getTopTimers(0, BaseServlet.MAX_TIMERS_RETURN_COUNT);
		} else {
			throw new InvalidParameter("view scope");
		}
	}
	
	public List<PostTO> getTopTimed(int userid,String view_scope) throws Exception{
		if (view_scope.equals("local")) {
			return this.getTopTimed(view_scope, userid, 0, BaseServlet.MAX_TIMED_RETURN_COUNT);
		} else if (view_scope.equals("global")) {
			return this.getTopTimed(view_scope, 0, BaseServlet.MAX_TIMED_RETURN_COUNT);
		} else {
			throw new InvalidParameter("view scope");
		}
	}
	
	public Map<String,Object> getPopular(int userid, String view_type,String view_scope) throws Exception{
		HashMap<String,Object> popular = new HashMap<String,Object>();
		
		if (view_type.equals("timers")) {
			popular.put("users", this.getTopTimers(userid,view_scope));
			return popular;
		} else if (view_type.equals("timed")) {
			popular.put("posts",this.getTopTimed(userid,view_scope));
			return popular; 
		} else {
			throw new InvalidParameter("view type");
		}
	}
	
	//Convert to transfer objects
	private PopularTimedTO timedObjToTimedTO(PopularTimedObject pto) throws ClassNotFoundException, SQLException{
		UserPostDAO postdao = new UserPostDAO();
		PostTO post = new PostTO(pto.getUserpost());
		post.setReposted(postdao.userReposted(pto.getUserpost().getIdUser(), pto.getUserpost().getIdUserPost()));
		return new PopularTimedTO(post);
	}
	
	private List<PopularTimedTO> timedListToPostList(List<PopularTimedObject> ptos) throws ClassNotFoundException, SQLException{
		List<PopularTimedTO> popularTimedTOs = new ArrayList<>();
		for(PopularTimedObject pto : ptos){
			PopularTimedTO popularTimedTO = timedObjToTimedTO(pto);
			popularTimedTO.setScore(pto.getScore());
			popularTimedTOs.add(popularTimedTO);
			
		}
		return popularTimedTOs;
	}
	
	private UserTO timerObjToPostTO(PopularTimerObject pto){
		return new UserTO(pto.getUser());
	}
	
	private List<UserTO> timerListToPostList(List<PopularTimerObject> ptos){
		List<UserTO> users = new ArrayList<>();
		for(PopularTimerObject pto : ptos){
			users.add(timerObjToPostTO(pto));
		}
		return users;
	}
}
