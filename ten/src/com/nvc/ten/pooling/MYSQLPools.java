package com.nvc.ten.pooling;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MYSQLPools {
	public static Map<String,MYSQLPool> pools = new HashMap<>();
	
	public static MYSQLPool getPool(String pool_name){
		return pools.get(pool_name);
	}
	
	public static Map<String, MYSQLPool> getPools() {
		return pools;
	}

	public static void setPools(Map<String, MYSQLPool> pools) {
		MYSQLPools.pools = pools;
	}

	public static void addPool(String pool_name,MYSQLPool pool){
		pools.put(pool_name,pool);
	}
}
