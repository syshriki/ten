package com.nvc.ten.pooling;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Vector;

import com.nvc.ten.servlets.BaseServlet;



public class MYSQLPool {
	private final String pingStatement = "/* ping */ SELECT 1";
	String databaseUrl = "jdbc:mysql://localhost:3306/myDatabase";
	String userName = "userName";
	String password = "userPass";
	int poolSize = 10;
			
	Vector connectionPool = new Vector();

	public MYSQLPool()
	{
		initialize();
	}

	public MYSQLPool(
		//String databaseName,
		String databaseUrl,
		String userName,
		String password,
		int poolSize
		)
	{
		this.databaseUrl = databaseUrl;
		this.userName = userName;
		this.password = password;
		this.poolSize = poolSize;
		initialize();
	}

	private void initialize()
	{
		//Here we can initialize all the information that we need
		initializeConnectionPool();
	}

	private void initializeConnectionPool()
	{
		while(!checkIfConnectionPoolIsFull())
		{
			System.out.println("Connection Pool is NOT full. Proceeding with adding new connections");
			//Adding new connection instance until the pool is full
			connectionPool.addElement(createNewConnectionForPool());
		}
		System.out.println("Connection Pool is full.");
	}

	private synchronized boolean checkIfConnectionPoolIsFull()
	{

		//Check if the pool size
		if(connectionPool.size() < poolSize)
		{
			return false;
		}

		return true;
	}

	//Creating a connection
	private Connection createNewConnectionForPool()
	{
		Connection connection = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(databaseUrl, userName, password);
			System.out.println("Connection: "+connection);
		}
		catch(SQLException sqle)
		{
			System.err.println("SQLException: "+sqle);
			return null;
		}
		catch(ClassNotFoundException cnfe)
		{
			System.err.println("ClassNotFoundException: "+cnfe);
			return null;
		}

		return connection;
	}

	public synchronized Connection getConnectionFromPool()
	{
		Connection connection = null;

		//Check if there is a connection available. There are times when all the connections in the pool may be used up
		if(connectionPool.size() > 0)
		{
			connection = (Connection) connectionPool.firstElement();
			connectionPool.removeElementAt(0);
		}
		/*else{ //Incase connection pool isnt big enough
			connection = createNewConnectionForPool();
		}*/
		//Giving away the connection from the connection pool
		return connection;
	}

	public synchronized void returnConnectionToPool(Connection connection)
	{
		//Adding the connection from the client back to the connection pool
		if(connectionPool.size()<this.poolSize)
			connectionPool.addElement(connection);
		else{
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public synchronized void pingConnections(){
		Statement stmt = null;
		for(int i = 0; i < this.poolSize; i++){
			Connection conn = this.getConnectionFromPool();
			try {
				stmt = conn.createStatement();
				stmt.execute(pingStatement);
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.returnConnectionToPool(conn);
		}
	}

}
