package com.nvc.ten.pooling.timertask;

import java.util.TimerTask;

import com.nvc.ten.pooling.MYSQLPools;

public class PingConnectionsTask extends TimerTask {
	public void run() {
		System.out.println("Keeping connections alive");
    	keeConnectionsAlive();
    }

	private void keeConnectionsAlive() {
		MYSQLPools.getPools().values().forEach(k -> k.pingConnections());
	}
}
