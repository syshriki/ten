package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class NotFollowingPrivateException  extends Exception implements UserThrowable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7194636194732796930L;
	public NotFollowingPrivateException(){
		super();
	}
	
	public String getMessage(){
		return "User is private";
	}
}
