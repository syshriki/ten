package com.nvc.ten.throwable.service;

public class UserAlreadyExistsException extends Exception{
	public static String ERROR = "Username already exists.";
	/**
	 * 
	 */
	
	public String getInputString(){ return ERROR;}
	private static final long serialVersionUID = 4320960562529343956L;
	public UserAlreadyExistsException(){
		super();
	}
}
