package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class InvalidID extends Exception implements UserThrowable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8285706152700825298L;
	public InvalidID(){
		super();
	}
	
	public String getMessage(){
		return "Invalid user id";
	}

}
