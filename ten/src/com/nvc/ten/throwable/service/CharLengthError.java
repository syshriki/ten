package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class CharLengthError extends Exception implements UserThrowable{

		/**
		 * 
		 */
		private static final long serialVersionUID = -30605478479320209L;
		private String info = "";
		public CharLengthError(String info){
			super();
			this.info = info;
		}
		
		public String getMessage(){
			return "Invalid amount of chars: "+info;
		}
		
}
