package com.nvc.ten.throwable.service;

public class InvalidFileFormatException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 380938048980553235L;
	public final static String ERROR = "Invalid file format.";
	/**
	 * 
	 */
	
	public String getInputString(){ return ERROR;}
	
	public InvalidFileFormatException(){
		super();
	}
}
