package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class InvalidParameter extends Exception implements UserThrowable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7194636194732796930L;
	private String parameterName = "";
	public InvalidParameter(String parameterName){
		super();
		this.parameterName = parameterName;
	}
	
	public String getMessage(){
		return "Insert parameter on field "+parameterName;
	}
}
