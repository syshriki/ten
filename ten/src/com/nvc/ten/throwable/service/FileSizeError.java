package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class FileSizeError extends Exception implements UserThrowable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -30605478479320209L;
	private String info = "";
	public FileSizeError(String info){
		super();
		this.info = info;
	}
	
	public String getMessage(){
		return "Invalid file size "+info;
	}
}
