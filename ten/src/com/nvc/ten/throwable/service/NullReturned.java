package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class NullReturned extends Exception implements UserThrowable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -72832986555098005L;

	public NullReturned(){
		super();
	}
	
	public String getMessage(){
		return "Received null when expecting object";
	}

}
