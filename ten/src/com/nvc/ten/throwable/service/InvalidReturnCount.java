package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class InvalidReturnCount  extends Exception implements UserThrowable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7194636194732796930L;
	public InvalidReturnCount (){
		super();
	}
	
	public String getMessage(){
		return "The amount of results specified to be returned is invalid. Check max and min result.";
	}
}
