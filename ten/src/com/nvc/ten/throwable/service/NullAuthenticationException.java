package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class NullAuthenticationException extends Exception implements UserThrowable{
	public final static String ERROR = "Invalid credentials";
	/**
	 * 
	 */
	public String getMessage(){ return ERROR;}
	private static final long serialVersionUID = 4320960562529343956L;
	public NullAuthenticationException(){
		super();
	}
}
