package com.nvc.ten.throwable.service;

public class RegistrationErrorException extends Exception{
	public static final String ERROR = "User could not be registered, see attached.";
	/**
	 * 
	 */
	
	public String getInputString(){ return ERROR;}
	private static final long serialVersionUID = 4320960562529343956L;
	public RegistrationErrorException(){
		super();
	}
}
