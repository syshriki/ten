package com.nvc.ten.throwable.service;

public class SaltInsertException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7448165496436474355L;
	public final static String ERROR = "Salt could not be added to database for new user.";
	/**
	 * 
	 */
	
	public String getInputString(){ return ERROR;}
	public SaltInsertException(){
		super();
	}
}
