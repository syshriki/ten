package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class InsertFailed extends Exception implements UserThrowable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7115622678259603537L;
	private  String className = "";
	public InsertFailed(String className){
		super();
		this.className = className;
	}
	
	public String getMessage(){
		return "Insert failed via "+className;
	}
	
}
