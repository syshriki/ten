package com.nvc.ten.throwable.service;

import com.nvc.ten.throwable.UserThrowable;

public class SaltNotFoundException extends Exception implements UserThrowable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7448165496436474355L;
	public final static String ERROR = "No salt could be found for the current user";
	/**
	 * 
	 */
	
	public String getMessage(){ return ERROR;}
	public SaltNotFoundException(){
		super();
	}
}
