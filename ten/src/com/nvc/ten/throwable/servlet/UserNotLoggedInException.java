package com.nvc.ten.throwable.servlet;

import com.nvc.ten.throwable.UserThrowable;

public class UserNotLoggedInException extends Exception implements UserThrowable{

	public static String ERROR = "Must be logged in to perform this action.";
	/**
	 * 
	 */
	private static final long serialVersionUID = -2455459531598587993L;

	public String getMessage(){ return ERROR;}
	public UserNotLoggedInException(){
		super();
	}
}
