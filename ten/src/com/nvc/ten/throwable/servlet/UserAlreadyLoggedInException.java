package com.nvc.ten.throwable.servlet;

import com.nvc.ten.throwable.UserThrowable;

public class UserAlreadyLoggedInException extends Exception implements UserThrowable{
	public static String ERROR = "Please logout before logging in.";
	/**
	 * 
	 */
	
	public String getMessage(){ return ERROR;}
	private static final long serialVersionUID = 4320960562529343956L;
	public UserAlreadyLoggedInException(){
		super();
	}
}
