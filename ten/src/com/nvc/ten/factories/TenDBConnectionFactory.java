package com.nvc.ten.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.nvc.ten.pooling.MYSQLPool;
import com.nvc.ten.pooling.MYSQLPools;
import com.nvc.ten.servlets.BaseServlet;

public class TenDBConnectionFactory {
	private final static String connectionString = BaseServlet.TENDB_CONNECTION_STRING;
	private static String db_user = BaseServlet.TENDB_USERNAME;
	private final static String driver = "com.mysql.jdbc.Driver";
	private static String db_pass = BaseServlet.TENDB_PASSWORD;
	private String poolName = com.nvc.ten.Application.tenPoolName;
	private static Connection conn;

	/*public TenDBConnectionFactory(){
		try {
			loadProps(propertyFilename);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	/*
	public final Connection getConnection() throws SQLException,
			ClassNotFoundException {
		Class.forName(driver);
		conn = DriverManager
				.getConnection(connectionString, db_user, db_pass);
		return conn;
	}*/

	public final Connection getConnection() throws SQLException,
			ClassNotFoundException {
		Connection conn = null;
		while(conn==null){
			conn = MYSQLPools.getPool(poolName).getConnectionFromPool();
			if(conn!=null)
				break;
			System.out.println("No connections available");
			try {
				Thread.sleep(400);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		}
		return conn;
	}
	
	public void returnConnection(Connection connection){
		if(MYSQLPools.getPool(poolName)!=null){
			MYSQLPools.getPool(poolName).returnConnectionToPool(connection);
		}else{
			try {
				connection.close(); //Hopefully this never happens
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public TenDBConnectionFactory(){
		
	}

}
