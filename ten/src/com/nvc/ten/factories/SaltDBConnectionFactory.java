package com.nvc.ten.factories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.nvc.ten.pooling.MYSQLPool;
import com.nvc.ten.pooling.MYSQLPools;
import com.nvc.ten.servlets.BaseServlet;

public class SaltDBConnectionFactory {
	 
    private final static String connectionString = BaseServlet.TENSALT_CONNECTION_STRING;
    public final static String TEN_DB_NAME = "tensalt";
	private final static String userName = BaseServlet.TENSALT_USERNAME; 
	private final static String driver = "com.mysql.jdbc.Driver";
	private final static String password = BaseServlet.TENSALT_PASSWORD;
	private static Connection conn;
	private String poolName = com.nvc.ten.Application.saltPoolName;
	
	public final Connection getConnection() throws SQLException, ClassNotFoundException{
		Class.forName(driver);
		conn = DriverManager.getConnection(connectionString, userName, password);
		return conn;
	}
	
	public void returnConnection(Connection connection){
		if(MYSQLPools.getPool(poolName)!=null){
			MYSQLPools.getPool(poolName).returnConnectionToPool(connection);
		}else{
			try {
				connection.close(); //Hopefully this never happens
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public SaltDBConnectionFactory(){
		
	}
	
}
